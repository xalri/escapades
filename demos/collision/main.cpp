#define BACKWARD_HAS_BFD 1
#include <vector>
#include <backward.hpp>
#include <util/time.hpp>
#include <storage/storage.hpp>
#include <math/math.hpp>
#include <physics/body.hpp>
#include <physics/collision.hpp>
#include <window/window.hpp>
#include <render/debug.hpp>
#include <resources/resources.hpp>
#include <render/gl.hpp>

backward::SignalHandling sh;

int main(int, char*[]) {
    auto window = Window("collision_demo", 1280, 720);
    auto view_bounds = Bounds<float>(range(0.f, 1280.f), range(0.0, 720.f));
    window.set_vsync(1);

	Storage storage{};
	Resources res{ storage };
	DebugDraw debug{ res };

	BroadPhase broad_phase{};

    auto floor = storage.create(Body::immovable(
            storage.create(Poly::rect(600.f, 20.f)),
            storage.create(Material()),
            storage.create(Transform{ vec2(400.f, 450.f), 185.f })
    ));
    floor->collision_id = 0b0001;

    // Same for the 'hand'
    auto hand = storage.create(Body::immovable(
            storage.create(Poly::ellipse(18, 50, 50)),
            storage.create(Material()),
            storage.create(Transform{ vec2(500.f, 250.f) })
    ));
    hand->collision_id = 0b0001;
    hand->is_static = false;

    auto prev_hand_pos = hand->t->position;

    // Create the pellet material & poly, we'll use these in the main loop when
    // new pellets are spawned.
    auto pellet_p = storage.create(Poly::ellipse(10, 40, 20));
    auto pellet_m = storage.create(Material());
    pellet_m->separation = 0.3f;
    pellet_m->elasticity = 0.6f;

    // A collection to store the pellets.
    auto pellets = std::vector<Ref<Body>>();

    // The bounds of the world, we delete pellets which escape these bounds.
    auto world_bounds = view_bounds.expand(200.f);

    // Counters for spawning pellets
    auto rate = 20, counter = 0, count = 0;

    while (!window.should_close()) {

        // Poll for input
        window.poll_events();

        // Update the 'hand' based on input
        while (auto event = window.next_event()) {
            if (event->kind == WindowEvKind::MouseMove) {
                if (window.mouse_pressed[GLFW_MOUSE_BUTTON_MIDDLE]) {
                    view_bounds = view_bounds - vec2<float>((float)event->mouse.x_diff, (float)event->mouse.y_diff);
                }
                hand->t->position = view_bounds.lerp(window.mouse_view.cast<float>() / window.view_size.cast<float>());
            }
            if (event->kind == WindowEvKind::MouseWheelScroll) {
                rate -= (int) event->mouse_wheel.y_off;
            }
        }

        hand->velocity = hand->t->position - prev_hand_pos;
        prev_hand_pos = hand->t->position;

        // Maybe spawn a new pellet.
        if (counter++ > rate) {
            if (count++ > 10) count = 0;
            counter = 0;
            auto rel = (float)(count / 5.f * M_PI);
            auto s = storage.create(Body::dynamic(
                    pellet_p.clone(),
                    pellet_m.clone(),
                    storage.create(Transform{
                        vec2(300.f + std::sin(rel) * 60, -60.f),
                        -20.f + count * 5,
                        (std::sin(rel) * 0.3f) + 1.f,
                    })));
            s->velocity = vec2(-std::sin(rel) * 2.f, 2.f);
            s->collision_id = 0b0010;
            s->collision_mask = 0b0011;
            s->update_properties();
            pellets.push_back(std::move(s));
        }

        Box<Body>& bodies = storage.box<Body>();

        // Make gravity happen.
        for (auto& b : bodies) {
            if (b.inv_mass > 0.0) {
                b.velocity.y += 0.15f;
                b.integrate();
            }
        }

        // Iterative collision detection & response
        for(auto i = 0; i < 5; i++) {
            // Find collisions
            broad_phase.find_collisions(bodies);
            auto no_collisions = true;

            // Iterate over bodies with collisions
            for (auto &c : broad_phase.collision_iter(bodies)) {
                auto &body_a = bodies[c.first];
                if (!c.second.empty()) no_collisions = false;

                // For all collisions for this body..
                for (auto &collision : c.second) {
                    auto &body_b = bodies[collision.body];

                    // Resolve the collision.
                    auto res = collision.contact.resolve(body_a, body_b);
                    res.first.apply(body_a);
                    res.second.apply(body_b);
                }
            }
            if (no_collisions) break;
        }

        // Remove out of bounds objects.
        swap_filter(pellets, [&](auto& ref){ return ref->bounds.intersects(world_bounds); });

        // Draw everything
        window.bind();
        gl::Clear().Color().Depth();
        for (auto& b : pellets) debug.draw_body(view(view_bounds), b);
        debug.draw_body(view(view_bounds), hand);
        debug.draw_body(view(view_bounds), floor);


        auto ss = std::stringstream();
        ss << broad_phase.profile << std::endl;
        ss << "entities: " << pellets.size();
        debug.draw_string(view(view_bounds) * translate(180, 500), ss.str());

        window.display();
    }
}
