#define BACKWARD_HAS_BFD 1
#include <thread>
#include <storage/storage.hpp>
#include <util/cwd.hpp>
#include <math/math.hpp>
#include <math/poly.hpp>
#include <physics/body.hpp>
#include <window/window.hpp>
#include <render/debug.hpp>
#include <render/queue.hpp>
#include <physics/collision.hpp>
#include <resources/resources.hpp>
#include <stage/stage.hpp>
#include <stage/tmx.hpp>
#include <field/navigation.hpp>
#include <field/sobel.hpp>
#include <field/draw.hpp>
#include <field/distance.hpp>
#include <field/blur.hpp>
#include <field/region.hpp>

struct Particle {
    Ref<Body> body{};
    bool alive = true;
    int in_target = 0;
    std::shared_ptr<field::Field<float, 2>> flowfield;

    Particle(Particle&&) = default;
    Particle& operator=(Particle&&) = default;

    Particle(std::shared_ptr<field::Field<float, 2>> field, Vector2f pos, Storage& s):
            flowfield(std::move(field))
    {
        static Ref<Poly> poly;
        static Ref<Material> mat;
        if (!poly.valid()) poly = s.create(Poly::ellipse(8, 20, 20));
        if (!mat.valid()) mat = s.emplace<Material>();
        body = s.create(Body::dynamic(
                poly.clone(),
                s.emplace<Material>(),
                s.emplace<Transform>()
        ));
        body->t->position = pos;
    }

    void update(Storage& s) {
        auto dir = flowfield->query(body->t->position).xy().normalize();

        dir.y = -dir.y;
        if (dir == vec2(0.f, 0.f)) {
            if(++in_target > 2) alive = false;
        } else {
            in_target = 0;
            body->velocity *= 0.86;
            body->velocity += dir.normalize() * 3 * 0.14;
        }
        body->integrate();
    }
};

int main() {
    auto window = Window("nav_demo", 1600, 960);
    window.set_vsync(1);
    auto bounds = Bounds<float>(range(0.f, 1600.f), range(0.0, 960.f));

	DrawQueue draw_queue{};
	Storage storage{};
	Resources res{ storage };
    PolyRenderCache polys{};
	DebugDraw debug{ res };
    std::vector<Ref<Particle>> particles{};
    Ref<MapObject> target{};

    auto map = res.get<Stage>(cwd() + "/map.tmx");
    for(auto& obj : map->objects) if (obj->type == "target") target = obj.clone();


    auto coarseness = 8.f;

    // Gives the cost of moving across each point in a region.
    field::Field<float, 1> cost_map(res, bounds, coarseness);

    // Calculates the global distance from some region given a cost-map.
    field::PathDistanceField path_field(res, bounds, coarseness);

    // Create a multi-chanel float field to store the gradient of the distance field.
    auto grad_field = std::make_shared<field::Field<float, 2>>(res, bounds, coarseness / 2.f);

    while (!window.should_close()) {
        window.poll_events();
        while (auto event = window.next_event()) {
            if (event->kind == WindowEvKind::MouseMove) {
                if (window.mouse_pressed[GLFW_MOUSE_BUTTON_MIDDLE] || window.key_pressed[GLFW_KEY_SPACE]) {
                    bounds = bounds - vec2<float>(event->mouse.x_diff, event->mouse.y_diff) * 1.5;
                }
            }
        }
        target->body->t->position = window.mouse_view.cast<float>();

        // Recalculate the cost map
        cost_map.clear(1.f);
		gl::Enable(gl::kBlend);
		gl::BlendEquation(gl::kMax);
        for(auto& p : particles) field::draw_body_density(res, polys, cost_map, *p->body, 0.7f, 13.f);

		gl::Disable(gl::kBlend);
        for(auto& obj : map->objects) if (obj != target) {
            field::draw_body(res, polys, cost_map, *obj->body, vec4(100.f), 1.f);
        }

        // Clear the distance field
        path_field.clear();

        // Draw the target to the distance field and update it
        field::draw_body(res, polys, path_field.distance, *target->body, vec4(10.f));
        path_field.update(cost_map, 80);
		//path_field.avoid_edges();

        // Calculate the gradient of the path field -- gives the direction an entity at
        // any point should move to get to the target.
        field::sobel_filter(res, path_field.distance, *grad_field);

        // Download the gradient field to RAM to speed up queries.
        grad_field->download();

        if (particles.size() < 2000)
            particles.push_back(storage.create(Particle(grad_field, vec2(1500.f, 500.f), storage)));

        for(auto& p : particles) p->update(storage);
        swap_filter(particles, [&](auto& ref){ return ref->alive; });

        for (auto body : storage.box<Body>().refs()) {
            draw_queue.add(0.0, [&, body](){ debug.draw_body(view(bounds), body); });
        }

        window.bind();
        gl::DepthMask(true);
        gl::ClearColor(0, 0, 0, 0);
        gl::Clear().Color().Depth();

        field::draw(res, path_field.distance, view(bounds), 3000 / coarseness, true);

        draw_queue.run();

        window.display();
    }
}
