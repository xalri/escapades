#pragma once

#include "field.hpp"

namespace field {

auto dist_init_src = ProgramSource{
triangle_fill_vert,
R"""(
    #version 330 core
    uniform sampler2D mask;
    uniform float threshold;
    uniform bool invert;
    out vec2 nearest;

    void main() {
        bool b = (texelFetch(mask, ivec2(gl_FragCoord.xy), 0).r < threshold) ^^ invert;
        nearest = vec2(10e10, 10e10) * float(b);
    }
)"""
};

auto edge_filter_init = ProgramSource{
triangle_fill_vert,
R"""(
    #version 330 core
    uniform sampler2D region;
    out vec2 nearest;
    uniform ivec2 size;

    ivec2 pos = ivec2(gl_FragCoord.xy);
    ivec2 max = size - ivec2(1, 1);
    float get(int x, int y) {
        return texelFetch(region, clamp(pos + ivec2(x, y), ivec2(0), max), 0).r;
    }

    void main() {
        float r = get(0, 0);
        bool b = get(1, 0) == r && get(-1, 0) == r && get(0, 1) == r && get(0, -1) == r;

        nearest = vec2(10e10, 10e10) * float(b);
    }
)"""
};

auto dist_nearest_src = ProgramSource{
triangle_fill_vert,
R"""(
    #version 330 core
    uniform sampler2D prev;
    uniform int step;
    uniform ivec2 size;

    out vec2 nearest;

    ivec2 pos = ivec2(gl_FragCoord.xy);
    float dist = 0;

    float dist_sq(vec2 v) { return v.x * v.x + v.y * v.y; }

    void compare(int x, int y) {
        ivec2 off = ivec2(x, y);
        ivec2 _pos = pos + off;
        if (_pos.x < 0 || _pos.y < 0 || _pos.x >= size.x || _pos.y >= size.y) return;
        vec2 _nearest = vec2(off) + texelFetch(prev, _pos, 0).xy;
        float _dist = dist_sq(_nearest);
        if (_dist < dist) {
            dist = _dist;
            nearest = _nearest;
        }
    }

    void main() {
        nearest = texelFetch(prev, pos, 0).xy;
        dist = dist_sq(nearest);
        compare(-step, 0);
        compare(step, 0);
        compare(0, -step);
        compare(0, step);

        compare(-step, step);
        compare(step, step);
        compare(-step, -step);
        compare(step, -step);
    }
)"""
};

auto dist_final_src = ProgramSource{
triangle_fill_vert,
R"""(
    #version 330 core
    uniform sampler2D nearest;
    uniform float max;
    out float dist;

    void main() {
        ivec2 pos = ivec2(gl_FragCoord.xy);
        vec2 rel = texelFetch(nearest, pos, 0).xy;
        dist = min(max, sqrt(rel.x * rel.x + rel.y * rel.y));
    }
)"""
};

/// Calculates the euclidean distance from each point to some region.
/// Uses jump flooding (http://www.comp.nus.edu.sg/~tants/jfa.html) for O(nlog(n)) performance.
struct DistanceField {
    Field<float, 2> nearest;
    Field<float, 1> distance;
    Ref<gl::Program> init_program; // Constructs the initial state of the nearest map from a mask
    Ref<gl::Program> init_region_program; // Constructs the initial state of the nearest map to give the distance to a boundry
    Ref<gl::Program> nearest_program; // Iteratively updates the nearest map
    // TODO: for SDF could we combine _before_ calculating the distance field?
    Ref<gl::Program> distance_program; // Calculates the distance map from the nearest map

    DistanceField(Resources &res, Bounds<float> world_bounds, float coarseness = 1.f):
            nearest(res, world_bounds, coarseness),
            distance(res, world_bounds, coarseness),
            init_program(res.get<gl::Program>("dist_field/init", dist_init_src)),
            init_region_program(res.get<gl::Program>("dist_field/init_region", edge_filter_init)),
            nearest_program(res.get<gl::Program>("dist_field/nearest", dist_nearest_src)),
            distance_program(res.get<gl::Program>("dist_field/dist", dist_final_src))
    { }

    /// Initialise the distance field with the given mask. `threshold` gives the value
    /// above which a point is considered part of the target region. If 'invert' is true,
    /// the distance from points inside the region to the outside is calculated instead.
    void init(Field<float, 1> const& mask, float mask_threshold = 0.5f, bool invert = false) {
        // set all cells above the threshold to a relative nearest cell of (0,0),
        // all others are set to (+inf, +inf)
        nearest.bind();
        gl::Bind(nearest.vao);
        gl::Use(*init_program);
        gl::BindToTexUnit(mask.texture, 0);
        gl::UniformSampler(*init_program, "mask") = 0;
        gl::Uniform<float>(*init_program, "threshold") = mask_threshold;
        gl::Uniform<GLuint>(*init_program, "invert") = invert ? GL_TRUE : GL_FALSE;
        gl::DrawArrays(gl::kTriangles, 0, 3);
    }

    /// Initialise the distance field to give the distance to the boundry of a region
    void init_region(Field<float, 1> const& regions) {
        // set all cells above the threshold to a relative nearest cell of (0,0),
        // all others are set to (+inf, +inf)
        nearest.bind();
        gl::Bind(nearest.vao);
        gl::Use(*init_region_program);
        gl::BindToTexUnit(regions.texture, 0);
        gl::Uniform<Vector2i>(*init_region_program, "size") = regions.size;
        gl::UniformSampler(*init_region_program, "region") = 0;
        gl::DrawArrays(gl::kTriangles, 0, 3);
    }

    /// Propagate distances.
    void update(float max_distance = 10e10) {
        gl::Use(*nearest_program);
        gl::BindToTexUnit(nearest.texture, 0);
        gl::BindToTexUnit(nearest._texture, 1);

        auto size = std::max(((int) max_distance), std::max(nearest.size.x, nearest.size.y) / 2);
        auto step = 1;
        auto iters = 1;
        while(step < size) {
            iters++;
            step = step << 1;
        }
        for(auto i = 0; i < iters; i++) {
            nearest.swap_buffers();
            nearest.bind();
            gl::Uniform<GLint>(*nearest_program, "step") = step;
            gl::Uniform<Vector2i>(*nearest_program, "size") = distance.size;
            gl::UniformSampler(*nearest_program, "prev") = i % 2;
            gl::DrawArrays(gl::kTriangles, 0, 3);
            if (step > 1) step /= 2;
        }

        distance.bind();
        gl::Use(*distance_program);
        gl::UniformSampler(*distance_program, "nearest") = 0;
        gl::Uniform<float>(*distance_program, "max") = max_distance;
        gl::BindToTexUnit(nearest.texture, 0);
        gl::DrawArrays(gl::kTriangles, 0, 3);

        gl::UnbindFromTexUnit(gl::kTexture2D, 0);
        gl::UnbindFromTexUnit(gl::kTexture2D, 1);
        gl::Unbind(nearest.vao);
        nearest.unbind();
   }
};

}
