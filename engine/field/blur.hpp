#pragma once

#include "field.hpp"

namespace field {

auto blur_filter_src = ProgramSource{
triangle_fill_vert,
R"""(
    #version 330 core
    uniform sampler2D_T source;
    uniform ivec2 size;
    layout (location = 0) out vec4_T avr;

    vec4_T get(int x, int y) {
        ivec2 uv = clamp(ivec2(gl_FragCoord.xy) + ivec2(x,y), ivec2(0), size - 1);
        return texelFetch(source, uv, 0);
    }

    void main() {
        // TODO: gaussian blur or something, anything is better than this :/
        avr = (get(-1,0) + get(1,0) + get(0,0) + get(0,1) + get(0,-1)) / 5;
    }
)"""
};

/// Blur a field.
template<class T, unsigned int N> void blur(Field<T, N> &field, Resources &res) {
    auto prog = res.get<gl::Program>(shader_name<T,N>("blur_filter"), blur_filter_src, (T) 0);

    field.swap_buffers();
    gl::Disable(gl::kDepthTest);
    gl::Disable(gl::kBlend);

    field.bind();
    gl::Bind(field.vao);
    gl::Use(*prog);
    gl::UniformSampler(*prog, "source") = 0;
    gl::Uniform<Vector2i>(*prog, "size").set(field.size);
    gl::BindToTexUnit(field._texture, 0);
    gl::DrawArrays(gl::PrimitiveType::kTriangles, 0, 3);
    gl::UnbindFromTexUnit(gl::kTexture2D, 0);
    gl::Unbind(field.vao);
    gl::UnbindFromTexUnit(gl::kTexture2D, 0);
    field.unbind();
}

}
