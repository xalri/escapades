#pragma once

#include <math/math.hpp>
#include <math/poly.hpp>
#include <physics/body.hpp>
#include <render/poly.hpp>
#include "render/gl.hpp"
#include "render/shader.hpp"

namespace field {

/// Generate a unique name for a template field shader
template<class T, unsigned int N>
std::string shader_name(std::string const& name) {
    return "field/" + name + "/" + std::string(typeid(T).name()) + std::to_string(N);
}

/// Get the texture size for a field to cover the given bounds.
Vector2i tx_size(Bounds<float> world_bounds, float coarseness = 1.f) {
    auto size = world_bounds.size() / coarseness;
    size.x = std::ceil(size.x);
    size.y = std::ceil(size.y);
    return size.cast<int>();
}

/// A field of values mapped to world-space.
template<class T, unsigned int N>
struct Field {
    Bounds<float> world_bounds;
    float coarseness;
    Vector2i size;
    Ref<gl::Program> clear_program;
    gl::Texture2D texture{};
    gl::Framebuffer fbo{};
    // TODO: only create second buffer when it's used.
    gl::Texture2D _texture{};
    gl::Framebuffer _fbo{};
    gl::VertexArray vao{};
    gl::ArrayBuffer vbo{};
    std::vector<T> _result{};
    Matrix3f transform; // world space -> field space

    /// Create an empty float field with given world-space bounds. `coarseness` gives
    /// the size of each pixel in the field. A lower coarseness value gives more
    /// detail but takes more video RAM and decreases the performance of field
    /// draws.
    Field(Resources &res, Bounds<float> world_bounds, float coarseness = 1.f) :
            world_bounds(world_bounds),
            coarseness(coarseness),
            size(tx_size(world_bounds, coarseness)),
            clear_program(res.get<gl::Program>("field/clear/" + std::string(typeid(T).name()), fill_screen_src, (T) 0)),
            texture(empty_tx<T, N>(size)),
            _texture(empty_tx<T, N>(size)),
            transform(scale(size.cast<float>() / 2) * translate(1, 1) * view(world_bounds)) {

        gl::Bind(fbo);
        fbo.attachTexture(gl::kColorAttachment0, texture);
        gl::DrawBuffer(gl::kColorAttachment0);
        gl::ReadBuffer(gl::kColorAttachment0);
        gl::Bind(_fbo);
        _fbo.attachTexture(gl::kColorAttachment0, _texture);
        gl::DrawBuffer(gl::kColorAttachment0);
        gl::ReadBuffer(gl::kColorAttachment0);

        auto min = world_bounds.min();
        auto max = world_bounds.max();
        gl::Bind(vao);
        gl::Bind(vbo);
        vbo.data(std::vector<float> {
                min.x, min.y, 0, 1, max.x, min.y, 1, 1, max.x, max.y, 1, 0,
                min.x, min.y, 0, 1, max.x, max.y, 1, 0, min.x, max.y, 0, 0
        });
        gl::VertexAttrib(0).pointer(2, gl::DataType::kFloat, false, 16, nullptr).enable();
        gl::VertexAttrib(1).pointer(2, gl::DataType::kFloat, false, 16, (void *) 8).enable();
        gl::Unbind(vbo);
        gl::Unbind(vao);
    }

    /// Bind the field's FBO and set the viewport.
    void bind() {
        gl::Bind(fbo);
        gl::Viewport(0, 0, size.x, size.y);
    }

    /// Get the field's value at the given point in world space.
    Vector4<T> query(Vector2f pos) {
        pos = world_bounds.clamp(pos);
        auto fb_pos = (transform * pos).template cast<int>();
        auto result = vec4<T>();
        if (!_result.empty()) {
            auto idx = (fb_pos.x + fb_pos.y * size.x) * N;
            result.r = _result[idx];
            if (N > 1) result.g = _result[idx + 1];
            if (N > 2) result.b = _result[idx + 2];
            if (N > 3) result.a = _result[idx + 3];
            return result;
        } else {
            bind();
            OGLWRAP_CHECKED_GLFUNCTION(glClampColor(GL_CLAMP_READ_COLOR, GL_FALSE));
            gl::ReadPixels(fb_pos.x, fb_pos.y, 1, 1, gl_format<N, T>(), gl_type<T>(), &result);
            unbind();
            return result;
        }
    }

    /// Unbind the field's FBO.
    void unbind() { gl::Unbind(fbo); }

    /// Swap the buffers.
    void swap_buffers() {
        auto tmp_fbo = std::move(_fbo);
        _fbo = std::move(fbo);
        fbo = std::move(tmp_fbo);
        auto tmp_tx = std::move(_texture);
        _texture = std::move(texture);
        texture = std::move(tmp_tx);
    }

    /// Clear the field to a given value.
    void clear(Vector4<T> value) {
        gl::Disable(gl::kDepthTest);
        gl::Disable(gl::kBlend);
        gl::Use(*clear_program);
        gl::Uniform<Vector4<T>>(*clear_program, "col").set(value);
        gl::Bind(vao);

        bind();
        // TODO: what breaks glClearBuffer?..
        //gl_clear<T>(value);
        gl::DrawArrays(gl::PrimitiveType::kTriangles, 0, 3);
        unbind();

        gl::Bind(_fbo);
        //gl_clear<T>(value);
        gl::DrawArrays(gl::PrimitiveType::kTriangles, 0, 3);
        gl::Unbind(_fbo);

        gl::Unbind(vao);

        reset_cache();
        //gl::Finish();
    }

    /// Download the texture to RAM for faster queries.
    void download() {
        bind();
        _result.clear();
        _result.resize(size.x * size.y * N);
        OGLWRAP_CHECKED_GLFUNCTION(glClampColor(GL_CLAMP_READ_COLOR, GL_FALSE));
        gl::ReadPixels(0, 0, size.x, size.y, gl_format<N, T>(), gl_type<T>(), _result.data());
        unbind();
    }

    /// Clear the cache, should be called whenever the field's texture is modified.
    void reset_cache() {
        _result.clear();
    }
};

}
