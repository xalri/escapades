#pragma once

#include "render/poly.hpp"
#include "field/field.hpp"

namespace field {

/// Draws a region of constant value to a field. Each point in the field which
/// is inside some polygon is set to a given value.
///
/// `res` is a reference to the resource manager, for getting the shader program.
/// `polys` is a cache of VAOs for polygons
/// `field` is the field to draw to
/// `region` is a polygon which defines the region to draw
/// `transform` is a transformation matrix from the polygon to *world space*
/// `value` is the value to write to the field for every point inside the region
/// `expand`, if set, expands the region along the polygon's vertex normals by some (world space) distance
template<class T, unsigned int N>
void draw_region(Resources& res, PolyRenderCache& polys, Field<T, N>& field, Ref<Poly> &region, Matrix3f transform, Vector4<T> value, float expand = 0.f) {
    field.bind();
    transform = view(field.world_bounds) * transform;

    auto program = res.get<gl::Program>(shader_name<T,N>("draw_region"), solid_shape_shader, (T) 0);
    gl::Use(*program);
    gl::Uniform<Matrix3f>(*program, "transform").set(transform);
    gl::Uniform<float>(*program, "expand").set(expand);
    gl::Uniform<Vector4<T>>(*program, "colour").set(value);

    auto& verts = polys.filled(region);
    gl::Bind(verts.vao);
    gl::DrawArrays(gl::PrimitiveType::kTriangles, 0, (GLsizei) verts.n);
    gl::Unbind(verts.vao);

    field.unbind();
}

/// Draw a body's hull to a field with the given value.
template<class T, unsigned int N>
void draw_body(Resources &res, PolyRenderCache& polys, Field<T,N>& field, Body &body, Vector4<T> value, float expand = 0.f) {
    draw_region(res, polys, field, body.hull, body.t->matrix(), value, expand);
}

const ProgramSource shape_density_src = ProgramSource {
R"""(
    #version 330 core
    in vec2 pos;
    in vec2 norm;
    uniform mat3 transform;
    uniform float expand;
    void main() {
        gl_Position = vec4(transform * vec3(pos + norm * expand, 1.0), 1.0);
        //gl_Position = vec4(transform * vec3(pos, 1.0), 1.0);
    }
)""",
R"""(
    #version 330 core
    uniform ivec2 center;
    uniform float radius;
    uniform float expand;
    uniform float mult;
    out vec4_T fragColor;
    void main() {
        ivec2 off = ivec2(gl_FragCoord.xy) - center;
        float dist = sqrt(off.x*off.x + off.y*off.y);
        fragColor = vec4(max(0, radius - dist)) * mult;
    }
)"""
};

/// Add a body to a field giving the inverse of the distance from the body's center.
void draw_body_density(Resources &res, PolyRenderCache &polys, Field<float, 1> &dist, Body &body, float mult, float expand = 0.f) {
    auto transform = view(dist.world_bounds) * body.t->matrix();
    auto center = (dist.transform * body.t->position).cast<int>();
    auto radius = (body.hull->radius() * body.t->scale + expand) / dist.coarseness;

    dist.bind();

    auto program = res.get<gl::Program>("field/shape_density", shape_density_src, (float) 0);
    gl::Use(*program);
    gl::Uniform<Matrix3f>(*program, "transform").set(transform);
    gl::Uniform<float>(*program, "expand").set(expand);
    gl::Uniform<float>(*program, "mult").set(mult);
    gl::Uniform<float>(*program, "radius").set(radius);
    gl::Uniform<Vector2i>(*program, "center").set(center);

    auto& verts = polys.filled(body.hull);
    gl::Bind(verts.vao);
    gl::DrawArrays(gl::PrimitiveType::kTriangles, 0, (GLsizei) verts.n);
    gl::Unbind(verts.vao);

    dist.unbind();
}

}
