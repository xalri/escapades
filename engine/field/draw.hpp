#pragma once

#include "field.hpp"

namespace field {

auto debug_draw_src = ProgramSource{
R"""(
    #version 330 core
    in vec2 pos;
    in vec2 uv;
    out vec2 frag_uv;
    uniform mat3 transform;
    void main() {
        frag_uv = uv;
        gl_Position = vec4(transform * vec3(pos, 1.0), 1.0);
    }
)""",
R"""(
    #version 330 core
    in vec2 frag_uv;
    out vec4 colour;
    uniform float min;
    uniform float max;
    uniform sampler2D_T field;
    uniform bool contour = false;

    void main() {
        colour = vec4(0, 0, 0, 0.5);
        vec4 value = (vec4(texture(field, frag_uv)) + min) / (max - min);

        // Draw bright magenta for nan/inf, to indicate an error..
        if (isinf(value.r) || isnan(value.r) || isinf(value.g) || isnan(value.g)) {
            colour = vec4(1, 0, 1, 1);
            return;
        }

        float bright = 1.0;
        if (contour && mod(value.r, 0.004) > 0.0035) { bright = 0.5; }

        colour.r = (1 - value.r) * bright;
        colour.g = value.g * bright;
        colour.b = (value.r < 1.0) ? 0.3 + (value.r * bright * 0.3) : 0.3;
    }
)"""
};

/// Draw a field.
/// `view` is a transformation from world space to clip space.
/// `range` gives the expected range of values in the field
template<class T, unsigned N>
void draw(Resources &res, Field <T, N> const &field, Matrix3f view, Range<float> range, bool contour = false) {
    auto program = res.get<gl::Program>(shader_name<T,N>("draw"), debug_draw_src, (T) 0);
    gl::Disable(gl::kDepthTest);
    gl::Use(*program);
    gl::Bind(field.vao);
    gl::Uniform<Matrix3f>(*program, "transform").set(view);
    gl::Uniform<GLfloat>(*program, "min").set(range.min);
    gl::Uniform<GLfloat>(*program, "max").set(range.max);
    gl::Uniform<GLint>(*program, "contour").set(contour ? GL_TRUE : GL_FALSE);
    gl::UniformSampler(*program, "field") = 0;
    gl::BindToTexUnit(field.texture, 0);
    gl::DrawArrays(gl::PrimitiveType::kTriangles, 0, 6);
    gl::UnbindFromTexUnit(gl::kTexture2D, 0);
    gl::Unbind(field.vao);
}

}
