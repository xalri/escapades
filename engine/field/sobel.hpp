#pragma once

#include "field.hpp"

namespace field {

/// A sobel filter. Approximates the gradient of a texture.
auto sobel_filter_src = ProgramSource{
triangle_fill_vert,
R"""(
    #version 330 core
    uniform sampler2D source;
    uniform ivec2 size;
    layout (location = 0) out vec2 gradient;

    ivec2 pos = ivec2(gl_FragCoord.xy);
    float get(int x, int y) {
        vec2 uv = clamp((vec2(pos) + vec2(x - 1, y - 1)) / size, vec2(0), vec2(1));
        return texture2D(source, uv, 0).r;
    }

    void main() {
        gradient = vec2(
            (get(0,0) + 2*get(0,1) + get(0,2)) - (get(2,0) + 2*get(2,1) + get(2,2)),
            (get(0,0) + 2*get(1,0) + get(2,0)) - (get(0,2) + 2*get(1,2) + get(2,2))
        );
    }
)"""
};

/// Approximate the gradient of a field with a sobel filter, writes the x and y gradient to
/// the red and green channels of the destination field
void sobel_filter(Resources &res, Field<float, 1> const &source, Field<float, 2> &dest) {
    gl::Disable(gl::Capability::kDepthTest);
    gl::Disable(gl::Capability::kBlend);

    dest.clear(0);
    dest.bind();

    auto sobel_prog = res.get<gl::Program>("sobel_program", sobel_filter_src);
    gl::Use(*sobel_prog);
    gl::UniformSampler(*sobel_prog, "source") = 0;
    gl::Uniform<Vector2i>(*sobel_prog, "size").set(dest.size);
    gl::BindToTexUnit(source.texture, 0);

    gl::Bind(source.vao);
    gl::DrawArrays(gl::PrimitiveType::kTriangles, 0, 3);
    gl::Unbind(source.vao);

    gl::UnbindFromTexUnit(gl::kTexture2D, 0);
    dest.unbind();
}

}
