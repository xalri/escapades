// GPU Pathfinding with global distance fields.
//
// sources:
//  - http://www2.cs.uregina.ca/~anima/408/Notes/Crowds/HybridVectorFieldPathfinding.htm
//  - https://en.wikipedia.org/wiki/Eikonal_equation#Numerical_approximation
//  - https://en.wikipedia.org/wiki/Fast_marching_method
//  - Jump Flooding: http://www.comp.nus.edu.sg/~tants/jfa/i3d06.pdf
//  - http://thomasdiewald.com/blog/?p=3294
//  - http://nullprogram.com/blog/2014/06/22/
//  - https://en.wikipedia.org/wiki/Sobel_operator
//  - https://howtorts.github.io/2014/01/04/basic-flow-fields.html

#pragma once

#include <render/gl.hpp>
#include <render/shader.hpp>
#include <field/field.hpp>
#include <field/distance.hpp>
#include <storage/storage.hpp>
#include <math/math.hpp>
#include <physics/body.hpp>
#include <resources/resources.hpp>

namespace field {

/// Jump flooding with an approximation of the eikonal equation
auto flood_shader_src = ProgramSource{
triangle_fill_vert,
R"""(
    #version 330 core

    // The current best-known distance from each point to the target
    uniform sampler2D distance;

    // The cost of traversing each point
    uniform sampler2D cost;

    // The distance to the closest point with a different cost
    // (the distance to the edge of the cost region)
    uniform sampler2D edge_dist;

    // The number of pixels to jump for each distance test.
    uniform int step;

    // The size of the distance texture
    uniform ivec2 size;

    // The updated shortest distance
    out float new_dist;

    // Get the clamped texel position for a given offset from the current fragment.
    ivec2 uv(int x, int y) {
        return clamp(ivec2(gl_FragCoord.xy) + ivec2(x, y), ivec2(0), size - 1);
    }

    float get_cost(int x, int y) { return texelFetch(cost, uv(x,y), 0).r; }
    float get_dist(int x, int y) { return texelFetch(distance, uv(x,y), 0).r; }

    void main() {
        float w = get_cost(0, 0);
        float d = get_dist(0, 0);

        int s = step;
        if (step > 1) s = min(int(texelFetch(edge_dist, ivec2(gl_FragCoord).xy, 0).r) + 1, s);

        // Get the distance to the target for neighbouring nodes.
        float dl = get_dist(-s, 0);
        float dr = get_dist(s, 0);
        float du = get_dist(0, s);
        float dd = get_dist(0, -s);

        // If none are smaller than the current node, return.
        if (dl >= d && dr >= d && du >= d && dd >= d) {
            new_dist = d;
            return;
        }

        // Get the cost of traveling to each neighbour from this node.
        float edge_w = w * 2 * (float(s) - 0.5); // cost to get to the edge of the neighbour
        float cl = get_cost(-s, 0) + edge_w;
        float cr = get_cost(s, 0) + edge_w;
        float cu = get_cost(0, s) + edge_w;
        float cd = get_cost(0, -s) + edge_w;

        // Get the smallest distance on each axis
        float dx; float dy; float cx; float cy;
        if (dl + cl < dr + cr) { dx = dl; cx = cl; } else { dx = dr; cx = cr; }
        if (du + cu < dd + cd) { dy = du; cy = cu; } else { dy = dd; cy = cd; }

        // ((d - dx)/cx)^2 + ((d - dy)/cy)^2 = 1
        // Solve for `d` to give the new distance from this node to the target.
        // If either axis is missing a value we just remove it's terms.
        // source: https://en.wikipedia.org/wiki/Eikonal_equation#Numerical_approximation

        if (dx < d && dy < d) {
            float f =  (dx+dy)*(dx+dy) - 2 * (dx*dx + dy*dy - cx*cy/4) ;
            float _d = (dx+dy) / 2 + sqrt(abs(f)) / 2;
            d = min(d, _d);
        } else {
            d = min(d, min(dx + cx/2, dy + cy/2));
        }

        new_dist = d;
    }
)"""
};

auto finalize_shader_src = ProgramSource{
triangle_fill_vert,
R"""(
    #version 330 core

    uniform sampler2D dist;
    uniform sampler2D edge_dist;
    out float new_dist;

    void main() {
        new_dist = texelFetch(dist, ivec2(gl_FragCoord), 0).r +
                   clamp(4.0 - texelFetch(edge_dist, ivec2(gl_FragCoord), 0).r, 0, 4.0) / 2.0;
    }
)"""
};

/// A field of distances from some region or point, computed on the GPU.
struct PathDistanceField {
    Field<float, 1> distance;
    DistanceField edge_dist; // Gives the distance to the edge of regions of different cost.
    Ref<gl::Program> flood_program{};
    Ref<gl::Program> final_program{};
    gl::VertexArray vao{};

    void clear() {
        distance.clear(10e30f);
    }

    PathDistanceField(Resources &res, Bounds<float> world_bounds, float coarseness):
            distance(res, world_bounds, coarseness),
            edge_dist(res, world_bounds, coarseness),
            flood_program(res.get<gl::Program>("path_dist/flood_shader", flood_shader_src)),
            final_program(res.get<gl::Program>("path_dist/final_shader", finalize_shader_src))
    {
        clear();
    }

    /// Update the field using the given cost map. The cost map should cover the same region
    /// in world-space as this distance field.
    void update(Field<float, 1> const& cost, unsigned iters) {
        gl::Disable(gl::Capability::kDepthTest);
        gl::Disable(gl::Capability::kBlend);

        // Find the distance from each point to the edge of it's region
        // TODO: would be better to have a distance for each direction, might also be faster to compute
        // (just loop in the fragment shader I think -- each texel would be slow, but it only requires one iteration)
        edge_dist.init_region(cost);
        edge_dist.update();

        gl::BindToTexUnit(distance._texture, 0);
        gl::BindToTexUnit(distance.texture, 1);
        gl::BindToTexUnit(cost.texture, 2);
        gl::BindToTexUnit(edge_dist.distance.texture, 3);

        gl::Bind(vao);
        gl::Use(*flood_program);
        gl::UniformSampler(*flood_program, "cost") = 2;
        gl::UniformSampler(*flood_program, "edge_dist") = 3;
        gl::Uniform<Vector2i>(*flood_program, "size").set(distance.size);
        auto dist_u = gl::UniformSampler(*flood_program, "distance");
        auto step_u = gl::UniformSampler(*flood_program, "step");

        // Propagate distances with jump flooding:

        // Find the largest power of two smaller than the field's dimensions.
        auto max_distance = std::max(distance.size.x, distance.size.y);
        auto n = 0;
        while((1 << (n + 1)) < max_distance) n++;

        // Iteratively run the flooding program
        for (auto i = 0u; i < iters * n; i++) {
            distance.swap_buffers();
            distance.bind();
            step_u = 1 << (i % n); // The number of texels to jump, cycles through 2^n, 2^(n-1) ..., 1.
            dist_u = i % 2; // The texture to read from -- swaps between the front & back buffers.
            gl::DrawArrays(gl::PrimitiveType::kTriangles, 0, 3);
        }

        // Clean everything up.
        distance.unbind();
        gl::UnbindFromTexUnit(gl::kTexture2D, 0);
        gl::UnbindFromTexUnit(gl::kTexture2D, 1);
        gl::UnbindFromTexUnit(gl::kTexture2D, 2);
        gl::UnbindFromTexUnit(gl::kTexture2D, 3);
        gl::Unbind(vao);
    }

    void avoid_edges() {
        gl::Bind(vao);
        gl::Use(*final_program);
        gl::BindToTexUnit(distance._texture, 0); // read from the distance back buffer
        gl::BindToTexUnit(edge_dist.distance.texture, 1);

        gl::UniformSampler(*final_program, "dist") = 0;
        gl::UniformSampler(*final_program, "edge_dist") = 1;

        distance.bind();
        gl::DrawArrays(gl::PrimitiveType::kTriangles, 0, 3);
        distance.unbind();

        gl::UnbindFromTexUnit(gl::kTexture2D, 0);
        gl::UnbindFromTexUnit(gl::kTexture2D, 1);
        gl::Unbind(vao);
    }
};
}

