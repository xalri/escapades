#pragma once

#include <math/math.hpp>
#include <physics/body.hpp>
#include <resources/resources.hpp>
#include "gl.hpp"
#include "text.hpp"
#include "poly.hpp"

const bool DEBUG_DRAW_BOUNDS = true;

struct DebugDraw {
    Ref<gl::Program> body_prog{};
    Ref<gl::Program> tile_prog{};
    std::vector<float> data{};
    Font font;
    PolyRenderCache polys;
    Ref<Poly> circle;

    explicit DebugDraw(Resources& res);

    /// Draw the outline and bounds of a body for debugging. Takes a Ref for vertex buffer caching
    void draw_body(Matrix3f view, Ref<Body> const &body, Vector4f col = vec4(0.5f, 0.9f, 0.6f, 0.6f), float interp = 0.0);

    /// Draw a string with the embedded debug font.
    void draw_string(Matrix3f transform, std::string const& str);
};


