#include <lodepng.h>
#include <stdexcept>
#include "image.hpp"

Image Image::load_png(const std::string &path) {
    unsigned error;
    std::vector<unsigned char> data;
    unsigned width, height;

    error = lodepng::decode(data, width, height, path, LCT_RGBA, 8);
    if(error) throw std::runtime_error(lodepng_error_text(error));

    return Image(width, height, std::move(data));
}
