#pragma once

#include <glad/glad.h>
#include <math/math.hpp>
#include <physics/body.hpp>
#include <storage/storage.hpp>
#include "texture.hpp"
#include "gl.hpp"

/// A drawable image with sprite sheet animation.
class Sprite {
private:
    Ref<gl::Program> program{};
    std::shared_ptr<gl::VertexArray> vao = std::make_shared<gl::VertexArray>();
    std::shared_ptr<gl::ArrayBuffer> buffer = std::make_shared<gl::ArrayBuffer >();
    unsigned long verts = 6;

    Sprite() = default;

public:
    // Movable, non-copyable.
    Sprite(Sprite&) = delete;
    Sprite& operator=(Sprite const&) = delete;
    Sprite(Sprite&&) = default;
    Sprite& operator=(Sprite&&) = default;

    /// The sprite's texture.
    Ref<Texture> tx{};
    /// The transformation from the uv coordinates of the sprite's geometry to texels of
    /// the texture, can be changed over time for sprite sheet animation.
    Matrix3f tx_transform{};
    /// The sprite's position & orientation, often shared with a Body.
    Ref<Transform> t{};
    /// The offset from the main transform, used to render the sprite some distance from
    /// the body.
    Transform offset{};
    /// The depth of the sprite used for draw ordering. (0 to 1)
    float depth = 0.5;
    /// The depth to use for parallax scrolling
    float parallax_depth = 1.0;
    Vector4f tint = vec4<float>(1.0, 1.0, 1.0, 1.0);
    bool smooth = false;
    bool visible = true;

    /// Animation data. If there are no animation frames calls to update_anim are ignored.
    struct Anim {
        /// The texture for each frame. Either this or tx_transform (or both) should be populated
        /// along with delay to enable animation.
        std::vector<Ref<Texture>> tx{};
        /// The texture transform of each frame
        std::vector<Matrix3f> tx_transform{};
        /// The delay in milliseconds before each frame will end
        std::vector<float> delay{};
        /// The time in milliseconds the current frame has been displayed
        float timer = 0.0f;
        /// The index of the current frame
        int current_frame = 0;
        // TODO: convenience functions for constructing anim for simple sprite sheets?

    } anim{};

    /// Create a sprite.
    // TODO: clipped sprites.
    Sprite(Resources& res,
           Ref<Texture> tx,
           Ref<Transform> t,
           Vector2f size,
           Matrix3f tx_transform = id(),
           Vector2f origin = vec2(0.f, 0.f));

    /// Create a new instance of the sprite
    Sprite instance(Ref<Transform> transform) const;

    /// Advance the animation by the given time in milliseconds. Has no effect
    /// if the sprite isn't animated.
    void update_anim(float msec);

    /// Move to a new frame.
    void set_frame(unsigned frame);

    /// Draw the sprite ~~with interpolation~~.
    // TODO: interpolation ._.
    void draw(Matrix3f view) const;
};
