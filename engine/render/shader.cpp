#include "shader.hpp"
#include <optional>

auto shape_vert =
R"""(
    #version 330 core
    in vec2 pos;
    in vec2 norm;
    out vec2 frag_pos;
    uniform mat3 transform;
    uniform float expand;
    void main() {
        frag_pos = pos + norm * expand;
        gl_Position = vec4(transform * vec3(frag_pos, 1.0), 1.0);
    }
)""";

ProgramSource solid_shape_shader = ProgramSource {
shape_vert,
R"""(
    #version 330 core
    uniform vec4_T colour;
    out vec4_T fragColor;
    void main() { fragColor = colour; }
)"""
};

const char* triangle_fill_vert = R"""(
    #version 330 core
    void main() {
        float x = -1.0 + float((gl_VertexID & 1) << 2);
        float y = -1.0 + float((gl_VertexID & 2) << 1);
        gl_Position = vec4(x, y, 0.0, 1.0);
    }
)""";

/// A shader program which sets every fragment of a buffer to a uniform colour.
ProgramSource fill_screen_src = ProgramSource{
triangle_fill_vert,
R"""(
    #version 330 core
    uniform vec4_T col;
    layout (location = 0) out vec4_T col_out;
    void main() { col_out = col; }
)"""
};
