#pragma once

#include <vector>
#include <sstream>
#include <storage/storage.hpp>
#include <resources/resources.hpp>
#include "tiles.hpp"

/// A fixed-width bitmap font.
class Font {
public:
    Ref<Texture> tx;
    Vector2i char_size;
    int char_offset;

    Chunk text(Ref<gl::Program>& tile_program, std::string const& str) {
        auto ss = std::stringstream(str);
        auto lines = std::vector<std::string>();
        auto line = std::string();
        auto pitch = 0ul;

        while(std::getline(ss, line, '\n')) {
            if (line.length() > pitch) pitch = line.length();
            lines.push_back(line);
        }

        auto data = std::vector<Chunk::Tile>();

        for(auto i = 0u; i < lines.size(); i++) {
            auto j = 0;
            for(auto c : lines[i]) {
                auto tile = (int)c - char_offset;
                data.push_back(Chunk::Tile{ (uint16_t) tile, (uint16_t)(j + i * pitch) });
                j++;
            }
        }

        return Chunk(tile_program, tx, char_size, data, char_size, 0.0, pitch);
    }
};
