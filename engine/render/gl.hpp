#pragma once

#include <math/math.hpp>
#include <glad/glad.h>
#include <oglwrap.h>
#include <shader.h>

// Extends gl::UniformObject for vector & matrix types.
namespace gl {
    template<> inline void UniformObject<Matrix3f>::set(const Matrix3f& mat, unsigned count) {
        glUniformMatrix3fv(location_, count, GL_FALSE, &mat.m00);
    }

    template<> inline Matrix3f UniformObject<Matrix3f>::get() const {
        auto value = Matrix3f();
        glGetUniformfv(program_.expose(), location_, &value.m00);
        return value;
    }

    template<> inline void UniformObject<Vector2f>::set(const Vector2f& vec, unsigned count) {
        glUniform2fv(location_, count, &vec.x);
    }

    template<> inline Vector2f UniformObject<Vector2f>::get() const {
        auto value = Vector2f();
        glGetUniformfv(program_.expose(), location_, &value.x);
        return value;
    }

    template<> inline void UniformObject<Vector2i>::set(const Vector2i& vec, unsigned count) {
        glUniform2iv(location_, count, &vec.x);
    }

    template<> inline Vector2i UniformObject<Vector2i>::get() const {
        auto value = Vector2i();
        glGetUniformiv(program_.expose(), location_, &value.x);
        return value;
    }

    template<> inline void UniformObject<Vector4f>::set(const Vector4f& vec, unsigned count) {
        glUniform4fv(location_, count, &vec.x);
    }

    template<> inline Vector4f UniformObject<Vector4f>::get() const {
        auto value = Vector4f();
        glGetUniformfv(program_.expose(), location_, &value.x);
        return value;
    }
}

/// Clear the current draw buffer to the given value.
template<typename T> void gl_clear(Vector4<T> value);

template<> inline void gl_clear<float>(Vector4<float> value) {
    OGLWRAP_CHECKED_GLFUNCTION(glClearBufferfv(GL_COLOR, 0, &value.r));
}

template<> inline void gl_clear<uint32_t>(Vector4<uint32_t> value) {
    OGLWRAP_CHECKED_GLFUNCTION(glClearBufferuiv(GL_COLOR, 0, &value.r));
}

template<> inline void gl_clear<int32_t>(Vector4<int32_t> value) {
    OGLWRAP_CHECKED_GLFUNCTION(glClearBufferiv(GL_COLOR, 0, &value.r));
}

/// Get the pixel data format with a given number of channels.
/// intended for use in generic functions, call as gl_format<N>()
template<unsigned int N, typename T> gl::PixelDataFormat gl_format();

template<> inline gl::PixelDataFormat gl_format<1, float>() { return gl::kRed; }
template<> inline gl::PixelDataFormat gl_format<2, float>() { return gl::kRg; }
template<> inline gl::PixelDataFormat gl_format<3, float>() { return gl::kRgb; }
template<> inline gl::PixelDataFormat gl_format<4, float>() { return gl::kRgba; }

template<> inline gl::PixelDataFormat gl_format<1, int32_t>() { return gl::kRedInteger; }
template<> inline gl::PixelDataFormat gl_format<2, int32_t>() { return gl::kRgInteger; }
template<> inline gl::PixelDataFormat gl_format<3, int32_t>() { return gl::kRgbInteger; }
template<> inline gl::PixelDataFormat gl_format<4, int32_t>() { return gl::kRgbaInteger; }

template<> inline gl::PixelDataFormat gl_format<1, uint32_t>() { return gl::kRedInteger; }
template<> inline gl::PixelDataFormat gl_format<2, uint32_t>() { return gl::kRgInteger; }
template<> inline gl::PixelDataFormat gl_format<3, uint32_t>() { return gl::kRgbInteger; }
template<> inline gl::PixelDataFormat gl_format<4, uint32_t>() { return gl::kRgbaInteger; }

template<> inline gl::PixelDataFormat gl_format<1, int16_t>() { return gl::kRedInteger; }
template<> inline gl::PixelDataFormat gl_format<2, int16_t>() { return gl::kRgInteger; }
template<> inline gl::PixelDataFormat gl_format<3, int16_t>() { return gl::kRgbInteger; }
template<> inline gl::PixelDataFormat gl_format<4, int16_t>() { return gl::kRgbaInteger; }

template<> inline gl::PixelDataFormat gl_format<1, uint16_t>() { return gl::kRedInteger; }
template<> inline gl::PixelDataFormat gl_format<2, uint16_t>() { return gl::kRgInteger; }
template<> inline gl::PixelDataFormat gl_format<3, uint16_t>() { return gl::kRgbInteger; }
template<> inline gl::PixelDataFormat gl_format<4, uint16_t>() { return gl::kRgbaInteger; }

template<> inline gl::PixelDataFormat gl_format<1, int8_t>() { return gl::kRedInteger; }
template<> inline gl::PixelDataFormat gl_format<2, int8_t>() { return gl::kRgInteger; }
template<> inline gl::PixelDataFormat gl_format<3, int8_t>() { return gl::kRgbInteger; }
template<> inline gl::PixelDataFormat gl_format<4, int8_t>() { return gl::kRgbaInteger; }

template<> inline gl::PixelDataFormat gl_format<1, uint8_t>() { return gl::kRedInteger; }
template<> inline gl::PixelDataFormat gl_format<2, uint8_t>() { return gl::kRgInteger; }
template<> inline gl::PixelDataFormat gl_format<3, uint8_t>() { return gl::kRgbInteger; }
template<> inline gl::PixelDataFormat gl_format<4, uint8_t>() { return gl::kRgbaInteger; }

/// Get the GL pixel data type for a c++ primitive type.
/// intended for use in generic functions, call as gl_type<T>()
template<class T> gl::PixelDataType gl_type();

template<> inline gl::PixelDataType gl_type<float>() { return gl::kFloat; }
template<> inline gl::PixelDataType gl_type<int32_t>() { return gl::kInt; }
template<> inline gl::PixelDataType gl_type<int16_t>() { return gl::kShort; }
template<> inline gl::PixelDataType gl_type<int8_t>() { return gl::kByte; }
template<> inline gl::PixelDataType gl_type<uint32_t>() { return gl::kUnsignedInt; }
template<> inline gl::PixelDataType gl_type<uint16_t>() { return gl::kUnsignedShort; }
template<> inline gl::PixelDataType gl_type<uint8_t>() { return gl::kUnsignedByte; }

/// Get the internal data format for a given type and number of channels.
/// For use in generic functions. Call as gl_internal_format<T, N>()
template<class T, unsigned int N> gl::PixelDataInternalFormat gl_internal_format();

template<> inline gl::PixelDataInternalFormat gl_internal_format<float, 1>() { return gl::kR32F; }
template<> inline gl::PixelDataInternalFormat gl_internal_format<float, 2>() { return gl::kRg32F; }
template<> inline gl::PixelDataInternalFormat gl_internal_format<float, 3>() { return gl::kRgb32F; }
template<> inline gl::PixelDataInternalFormat gl_internal_format<float, 4>() { return gl::kRgba32F; }

template<> inline gl::PixelDataInternalFormat gl_internal_format<int32_t, 1>() { return gl::kR32I; }
template<> inline gl::PixelDataInternalFormat gl_internal_format<int32_t, 2>() { return gl::kRg32I; }
template<> inline gl::PixelDataInternalFormat gl_internal_format<int32_t, 3>() { return gl::kRgb32I; }
template<> inline gl::PixelDataInternalFormat gl_internal_format<int32_t, 4>() { return gl::kRgba32I; }

template<> inline gl::PixelDataInternalFormat gl_internal_format<uint32_t, 1>() { return gl::kR32Ui; }
template<> inline gl::PixelDataInternalFormat gl_internal_format<uint32_t, 2>() { return gl::kRg32Ui; }
template<> inline gl::PixelDataInternalFormat gl_internal_format<uint32_t, 3>() { return gl::kRgb32Ui; }
template<> inline gl::PixelDataInternalFormat gl_internal_format<uint32_t, 4>() { return gl::kRgba32Ui; }


template<> inline gl::PixelDataInternalFormat gl_internal_format<int16_t, 1>() { return gl::kR16I; }
template<> inline gl::PixelDataInternalFormat gl_internal_format<int16_t, 2>() { return gl::kRg16I; }
template<> inline gl::PixelDataInternalFormat gl_internal_format<int16_t, 3>() { return gl::kRgb16I; }
template<> inline gl::PixelDataInternalFormat gl_internal_format<int16_t, 4>() { return gl::kRgba16I; }

template<> inline gl::PixelDataInternalFormat gl_internal_format<uint16_t, 1>() { return gl::kR16Ui; }
template<> inline gl::PixelDataInternalFormat gl_internal_format<uint16_t, 2>() { return gl::kRg16Ui; }
template<> inline gl::PixelDataInternalFormat gl_internal_format<uint16_t, 3>() { return gl::kRgb16Ui; }
template<> inline gl::PixelDataInternalFormat gl_internal_format<uint16_t, 4>() { return gl::kRgba16Ui; }


template<> inline gl::PixelDataInternalFormat gl_internal_format<int8_t, 1>() { return gl::kR8I; }
template<> inline gl::PixelDataInternalFormat gl_internal_format<int8_t, 2>() { return gl::kRg8I; }
template<> inline gl::PixelDataInternalFormat gl_internal_format<int8_t, 3>() { return gl::kRgb8I; }
template<> inline gl::PixelDataInternalFormat gl_internal_format<int8_t, 4>() { return gl::kRgba8I; }

template<> inline gl::PixelDataInternalFormat gl_internal_format<uint8_t, 1>() { return gl::kR8Ui; }
template<> inline gl::PixelDataInternalFormat gl_internal_format<uint8_t, 2>() { return gl::kRg8Ui; }
template<> inline gl::PixelDataInternalFormat gl_internal_format<uint8_t, 3>() { return gl::kRgb8Ui; }
template<> inline gl::PixelDataInternalFormat gl_internal_format<uint8_t, 4>() { return gl::kRgba8Ui; }

/// Create a 2D texture of the given size.
template<class T, unsigned int N> gl::Texture2D empty_tx(Vector2i size) {
    gl::Texture2D tx{};
    gl::Bind(tx);
    tx.minFilter(gl::kLinear);
    tx.magFilter(gl::kLinear);
    //tx.minFilter(gl::kNearest);
    //tx.magFilter(gl::kNearest);
    tx.wrapS(gl::kClampToBorder);
    tx.wrapT(gl::kClampToBorder);
    tx.upload(gl_internal_format<T, N>(), size.x, size.y, gl_format<N, T>(), gl_type<T>(), nullptr);
    gl::Unbind(tx);
    return tx;
}
