#pragma once

#include <resources/resources.hpp>
#include "image.hpp"
#include "gl.hpp"

struct Texture {
    gl::Texture2D tx;
    unsigned width, height;

    Texture(gl::Texture2D tx, unsigned w, unsigned h): tx(std::move(tx)), width(w), height(h) {}

    /// Create a texture from the image data.
    static Texture from_image(Image const& image) {
        auto tx = gl::Texture2D();
        gl::Bind(tx);
        tx.upload(gl::kRgba, image.width, image.height, gl::kRgba, gl::kUnsignedByte, image.data.data());
        tx.wrapS(gl::kClampToEdge);
        tx.wrapT(gl::kClampToEdge);
        //tx.minFilter(gl::kLinear);
        //tx.magFilter(gl::kLinear);
        tx.minFilter(gl::kNearest);
        tx.magFilter(gl::kNearest);
        return Texture(std::move(tx), image.width, image.height);
    }
};

overload(Texture) load_resource(Resources &res, std::string const& id) {
    return Texture::from_image(Image::load_png(id));
}

overload(int) load_resource(Resources &res, std::string const& id) {
    return 42;
}
