#include "poly.hpp"
#include "shader.hpp"

auto fill_shader_src = ProgramSource{
R"""(
    #version 330 core

    in vec2 in_pos;
    in vec2 in_uv;
    uniform mat3 view;
    uniform mat3 model;
    uniform float depth;
    uniform float parallax;

    void main() {
        float inv_p = 1.0 / parallax;
        mat3 transform = mat3(inv_p, 0, 0,  0, inv_p, 0, 0, 0, inv_p) * view * model;
        gl_Position = vec4((transform * vec3(in_pos, 1.0)).xy, depth, 1.0);
    }
)""",
R"""(
    #version 330 core
    out vec4 fragColor;
    uniform vec4 col;
    void main() { fragColor = col; }
)"""
};

FilledPoly::FilledPoly(Resources &res, Ref<Poly> poly, Ref<Transform> t) {
    this->program = res.get<gl::Program>("fill_program", fill_shader_src);
    this->t = std::move(t);
    auto poly_cache = res.get<PolyRenderCache>("poly_cache");
    verts = poly_cache->filled(poly);
}

void FilledPoly::draw(Matrix3f view) const {
    if (!visible) return;
    auto model = t->matrix();

    gl::DepthMask(false);
    gl::Enable(gl::Capability::kDepthTest);
    gl::Enable(gl::Capability::kBlend);
    gl::BlendEquation(gl::BlendEquation::kFuncAdd);
    gl::BlendFunc(gl::BlendFunction::kSrcAlpha, gl::BlendFunction::kOneMinusSrcAlpha);

    gl::Use(*program);
    gl::Uniform<Matrix3f>(*program, "view").set(view);
    gl::Uniform<Matrix3f>(*program, "model").set(model);
    gl::Uniform<float>(*program, "depth").set(depth);
    gl::Uniform<float>(*program, "parallax").set(parallax_depth);
    gl::Uniform<Vector4f>(*program, "col").set(col);

    gl::Bind(*verts.vao);
    gl::DrawArrays(gl::PrimitiveType::kTriangles, 0, (int) verts.n);
    gl::Unbind(*verts.vao);
    gl::UnbindFromTexUnit(gl::kTexture2D, 0);
}
