#pragma once

#include <math/poly.hpp>
#include <storage/storage.hpp>
#include <unordered_map>
#include <memory>
#include "gl.hpp"
#include <resources/resources.hpp>

/// Constructs VBOs for Polys
struct PolyRenderCache {
    struct Verts {
        std::shared_ptr<gl::VertexArray> vao{};
        std::shared_ptr<gl::ArrayBuffer> vbo{};
        unsigned long n;
    };
    std::unordered_map<RefData, Verts> map_filled{};
    std::unordered_map<RefData, Verts> map_outline{};

    Verts const& filled(Ref<Poly> const& ref) {
        auto it = map_filled.find(ref.data);
        if (it == map_filled.end()) it = map_filled.emplace(ref.data, gen(*ref)).first;
        return it->second;
    }

    Verts const& outline(Ref<Poly> const& ref) {
        auto it = map_outline.find(ref.data);
        if (it == map_outline.end()) it = map_outline.emplace(ref.data, gen_outline(*ref)).first;
        return it->second;
    }

    Verts gen(Poly const& region){
        thread_local auto data = std::vector<Vector2f>();

        auto vao = std::make_shared<gl::VertexArray>();
        auto vbo = std::make_shared<gl::ArrayBuffer>();
        gl::Bind(*vao);
        gl::Bind(*vbo);
        gl::VertexAttrib(0).pointer(2, gl::kFloat, false, 16, nullptr).enable();
        gl::VertexAttrib(1).pointer(2, gl::kFloat, false, 16, (void*) 8).enable();
        for (auto& poly : region.convex()) {
            auto first = data.size();
            for(auto i = 0; i < poly->size(); i++) {
                if (i > 2) {
                    auto last = data.size() - 2;
                    data.push_back(data[first]);
                    data.push_back(data[first+1]);
                    data.push_back(data[last]);
                    data.push_back(data[last+1]);
                }
                auto n = ((*poly)[i-1] - (*poly)[i]) + ((*poly)[i+1] - (*poly)[i]);
                data.push_back((*poly)[i]);
                data.push_back(-n.normalize());
            }
        }
        vbo->data(data);
        auto n = data.size() / 2;
        data.clear();
        gl::Unbind(*vbo);
        gl::Unbind(*vao);

        return Verts{ std::move(vao), std::move(vbo), n };
    }

    Verts gen_outline(Poly const& region){
        thread_local auto data = std::vector<Vector2f>();

        auto vao = std::make_shared<gl::VertexArray>();
        auto vbo = std::make_shared<gl::ArrayBuffer>();
        gl::Bind(*vao);
        gl::Bind(*vbo);
        gl::VertexAttrib(0).pointer(2).enable();
        for (auto& p : region) data.push_back(p);
        vbo->data(data);
        auto n = data.size();
        data.clear();
        gl::Unbind(*vbo);
        gl::Unbind(*vao);

        return Verts{ std::move(vao), std::move(vbo), n };
    }
};

overload(PolyRenderCache) load_resource(Resources &res, std::string const& id) {
    return PolyRenderCache{};
}

class FilledPoly {
private:
    Ref<gl::Program> program{};
    PolyRenderCache::Verts verts{};

    FilledPoly() = default;

public:
    Ref<Transform> t{};
    float depth = 0.5;
    float parallax_depth = 1.0;
    Vector4f col = vec4<float>(1.0, 1.0, 1.0, 1.0);
    bool visible = true;

    FilledPoly(Resources& res, Ref<Poly> poly, Ref<Transform> t);

    void draw(Matrix3f view) const;
};
