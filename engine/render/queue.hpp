#pragma once

#include <vector>
#include <functional>
#include <algorithm>

/// A list of lambdas that draw stuff. They're sorted by depth and called
/// back-to-front to make transparency happen.
class DrawQueue {
    std::vector<std::pair<float, std::function<void()>>> queue;

public:
    /// Create a draw queue.
    /// Reserves some space to avoid allocations while the queue's being populated.
    DrawQueue() { queue.reserve(1028); }

    /// Add a thing to draw
    void add(float depth, std::function<void()>&& fn) {
        queue.emplace_back(depth, std::move(fn));
    }

    /// Draw everything and clear the queue
    void run() {
        // Sort by descending depth -- the fragment shaders are simple (so overdrawing
        // isn't a problem) and we don't know what's transparent.
        std::sort(queue.begin(), queue.end(), [](auto const& a, auto const& b) {
            return a.first > b.first;
        });
        for(auto& pair : queue) pair.second();
        queue.clear();
    }
};