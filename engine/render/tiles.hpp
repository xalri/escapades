//! GPU tile map rendering

#pragma once

#include <vector>
#include <memory>
#include <map>
#include <ostream>
#include <math/math.hpp>
#include <storage/storage.hpp>
#include <resources/resources.hpp>
#include "texture.hpp"
#include "gl.hpp"
#include "shader.hpp"

/// A chunk of tiles stored in video memory
class Chunk {
private:
    Ref<gl::Program> program{};
    Ref<Texture> diffuse{};
    Vector2i tx_tile_size = vec2<int>(0, 0);
    gl::VertexArray vao{};
    gl::ArrayBuffer buffer{};
    Vector2i tile_size{};
    unsigned tile_count;
    int pitch;
    bool smooth = false;

    /// Tiles per row in the tileset's texture.
    int tx_pitch() const { return (int)(diffuse->width / tx_tile_size.x); }

public:
    /// The value to be written to the depth buffer.
    float depth;
    /// Depth for a parallax effect
    float parallax_depth = 1.0;

    Chunk(Chunk&) = delete;
    Chunk(Chunk&&) = default;
    Chunk& operator=(Chunk&) = delete;
    Chunk& operator=(Chunk&&) = default;

    /// Data for a single static tile
    struct Tile {
        /// The tile ID.
        uint16_t tile_id = 0;
        /// The position of the tile in the chunk.
        uint16_t pos = 0;
        /// Flags for flipping the tile.
        uint8_t flags = 0;

        const uint8_t FLIPPED_HORIZONTALLY = 0x8;
        const uint8_t FLIPPED_VERTICALLY = 0x4;
        const uint8_t FLIPPED_DIAGONALLY = 0x2;
    };

    static Ref<gl::Program> get_program(Resources& res);

    Chunk(Ref<gl::Program> program,
          Ref<Texture> diffuse,
          Vector2i tx_tile_size,
          std::vector<Tile> const& tiles,
          Vector2i tile_size,
          float depth = 0.5f,
          int pitch = 16);

    /// Draw the tiles
    void draw(Matrix3f transform) const;
};
