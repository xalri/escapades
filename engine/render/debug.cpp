#include "gl.hpp"
#include "shader.hpp"
#include "debug_font.hpp"
#include "debug.hpp"
#include "tiles.hpp"

Texture load_font_tx() {
    auto tx = gl::Texture2D();
    gl::Bind(tx);
    tx.upload(gl::kRgba8, font_small.width, font_small.height, gl::kRgba, gl::kUnsignedByte, font_small.pixel_data);
    tx.minFilter(gl::kLinear);
    tx.magFilter(gl::kLinear);
    gl::Unbind(tx);
    return Texture { std::move(tx), font_small.width, font_small.height };
}

void DebugDraw::draw_string(Matrix3f transform, std::string const& str) {
    font.text(tile_prog, str).draw(transform);
}

void DebugDraw::draw_body(Matrix3f view, Ref<Body> const &body, Vector4f col, float interp) {
    gl::DepthMask(false);
    gl::Disable(gl::Capability::kDepthTest);
    gl::Enable(gl::Capability::kBlend);
    gl::BlendEquation(gl::BlendEquation::kFuncAdd);
    gl::BlendFunc(gl::BlendFunction::kSrcAlpha, gl::BlendFunction::kOneMinusSrcAlpha);

    gl::Use(*body_prog);
    body.test_valid();
    body->t.test_valid();
    auto t = view * translate(body->velocity * interp) * body->t->matrix();
    gl::Uniform<Matrix3f>(*body_prog, std::string("transform")).set(t);
    gl::Uniform<Vector4f>(*body_prog, std::string("colour")).set(col);

    auto& v = polys.outline(body->hull);
    gl::Bind(*v.vao);
    gl::DrawArrays(gl::PrimitiveType::kLineLoop, 0, v.n);
    gl::Unbind(*v.vao);

    gl::Uniform<Vector4f>(*body_prog, std::string("colour")).set(vec4(0.1f, 0.4f, 1.f, 1.f));
    auto& _v = polys.outline(circle);
    gl::Bind(*_v.vao);
    gl::DrawArrays(gl::PrimitiveType::kLineLoop, 0, _v.n);
    gl::Unbind(*_v.vao);
}

DebugDraw::DebugDraw(Resources &res) {
    circle = res.storage.create(Poly::ellipse(6, 3, 3));
    tile_prog = Chunk::get_program(res);
    body_prog = res.get<gl::Program>("solid_shape_program", solid_shape_shader, (float) 0);
    font = Font {
            res.get_with<Texture>("debug_font", [](){ return load_font_tx(); }),
            vec2(7, 12),
            32
    };
}
