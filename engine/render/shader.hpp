/// GLSL shaders with psudo-generics using string substitution.
/// To create generic shaders, use the type names:
///     scalar_T, vec2_T, vec3_T, vec4_T, and sampler2D_T
/// and load with, e.g.:
///     res.get<gl::Program>("some_name", my_shader_source, (int) 0);
/// to replace the generic types with the signed integer types.
/// For shaders without generic types, the last parameter can be omitted.

#pragma once

#include <map>
#include <memory>
#include <resources/resources.hpp>
#include "gl.hpp"

struct ProgramSource {
    const char* vert = nullptr;
    const char* frag = nullptr;
    const char* geom = nullptr;
};

inline void replace_all(std::string& in, std::string const& find, std::string const& replace) {
    auto start = 0ul;
    while((start = in.find(find, start)) != std::string::npos) {
        in.replace(start, find.length(), replace);
        start += replace.length();
    }
}

inline void substitute(std::string& in, void*) { }

inline void substitute(std::string& in, int32_t) {
    replace_all(in, "scalar_T", "int");
    replace_all(in, "vec2_T", "ivec2");
    replace_all(in, "vec3_T", "ivec3");
    replace_all(in, "vec4_T", "ivec4");
    replace_all(in, "sampler2D_T", "isampler2D");
}

inline void substitute(std::string& in, uint32_t) {
    replace_all(in, "scalar_T", "uint");
    replace_all(in, "vec2_T", "uvec2");
    replace_all(in, "vec3_T", "uvec3");
    replace_all(in, "vec4_T", "uvec4");
    replace_all(in, "sampler2D_T", "usampler2D");
}

inline void substitute(std::string& in, float) {
    replace_all(in, "scalar_T", "float");
    replace_all(in, "vec2_T", "vec2");
    replace_all(in, "vec3_T", "vec3");
    replace_all(in, "vec4_T", "vec4");
    replace_all(in, "sampler2D_T", "sampler2D");
}

inline void substitute(std::string& in, int16_t) { substitute(in, (int32_t)0); }
inline void substitute(std::string& in, int8_t) { substitute(in, (int32_t)0); }
inline void substitute(std::string& in, uint16_t) { substitute(in, (int32_t)0); }
inline void substitute(std::string& in, uint8_t) { substitute(in, (int32_t)0); }

template<class T> gl::Program load_program(ProgramSource source, T t) {
    auto vs = std::optional<gl::Shader>();
    auto gs = std::optional<gl::Shader>();
    auto fs = std::optional<gl::Shader>();

    if (source.vert) {
        auto src = std::string(source.vert);
        substitute(src, t);
        gl::ShaderSource vs_source;
        vs_source.set_source(src);
        vs.emplace(gl::kVertexShader, vs_source);
    }

    if (source.geom) {
        auto src = std::string(source.geom);
        substitute(src, t);
        gl::ShaderSource gs_source;
        gs_source.set_source(src);
        gs.emplace(gl::kGeometryShader, gs_source);
    }

    if (source.frag) {
        auto src = std::string(source.frag);
        substitute(src, t);
        gl::ShaderSource fs_source;
        fs_source.set_source(src);
        fs.emplace(gl::kFragmentShader, fs_source);
    }

    auto program = gl::Program();
    if(vs.has_value()) program.attachShader(*vs);
    if(gs.has_value()) program.attachShader(*gs);
    if(fs.has_value()) program.attachShader(*fs);
    program.link();

    return program;
}

inline gl::Program load_program(ProgramSource source) {
    return load_program(source, (void*)0);
}

overload(gl::Program) load_resource(Resources&, std::string const&, ProgramSource program) {
    return load_program(program);
}

template<class T, class R>
inline typename std::enable_if<std::is_same<T, gl::Program>::value, gl::Program>::type
load_resource(Resources&, std::string const&, ProgramSource program, R r) {
    return load_program<R>(program, r);
}

extern ProgramSource solid_shape_shader;

/// A vertex shader which using an empty VBO generates a screen-filling triangle.
extern const char* triangle_fill_vert;

/// A shader program which sets every fragment of a buffer to a uniform colour.
extern ProgramSource fill_screen_src;
