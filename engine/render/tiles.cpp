#include "tiles.hpp"

auto tiles_shader_source = ProgramSource{
R"""(
    #version 330 core

    in int in_tile;
    in int in_pos;
    in int in_flags;

    out int tile;
    out int flags;
    out vec2 pos;

    uniform int tile_w;
    uniform int tile_h;
    uniform int pitch;

    void main() {
        tile = in_tile;
        flags = in_flags;
        pos = vec2((in_pos % pitch) * tile_w, (in_pos / pitch) * tile_h);
    }
)""",
R"""(
    #version 330 core
    in vec2 uv;
    out vec4 fragColor;
    uniform sampler2D tx;
    uniform int _smooth;

    void main() {
        if (_smooth != 0) {
            ivec2 size = textureSize(tx, 0);
            fragColor = texture(tx, uv / vec2(size));
        } else {
            fragColor = texelFetch(tx, ivec2(uv), 0);
        }
		//fragColor = vec4(1, 0, 1, 1);
    }
)""",
R"""(
    #version 330 core

    layout (points) in;
    layout (triangle_strip, max_vertices = 4) out;

    in int tile[];
    in int flags[];
    in vec2 pos[];

    out vec2 uv;

    uniform mat3 transform;
    uniform int tile_w;
    uniform int tile_h;
    uniform int pitch;
    uniform int t_tile_w;
    uniform int t_tile_h;
    uniform int t_pitch;
    uniform float depth;

    void main() {
        int n = gl_PrimitiveIDIn;
		// TODO: what the fuck?:
		n = 0;
        vec2 pos = pos[n];
        int tile = tile[n];
        int flags = flags[n];

        int tile_u = (tile % t_pitch) * t_tile_w;
        int tile_v = (tile / t_pitch) * t_tile_h;

        bool flipped_horizontally = (flags & 0x8) != 0;
        bool flipped_vertically = (flags & 0x4) != 0;
        bool flipped_diagonally = (flags & 0x2) != 0;

        int uv_left = tile_u;
        int uv_right = tile_u + t_tile_w;
        int uv_top = tile_v;
        int uv_bottom = tile_v + t_tile_h;

        if (flipped_diagonally) {
            uv_top = tile_u;
            uv_bottom = tile_u + t_tile_w;
            uv_left = tile_v;
            uv_right = tile_v + t_tile_h;
        }
        if (flipped_vertically) {
            int tmp = uv_top;
            uv_top = uv_bottom;
            uv_bottom = tmp;
        }
        if (flipped_horizontally) {
            int tmp = uv_left;
            uv_left = uv_right;
            uv_right = tmp;
        }

		gl_PrimitiveID = n;
        gl_Position = vec4((transform * vec3(pos.x, pos.y, 1.0)).xy, depth, 1.0);
        uv = vec2(uv_left, uv_top);
        EmitVertex();

		gl_PrimitiveID = n;
        gl_Position = vec4((transform * vec3(pos.x + tile_w, pos.y, 1.0)).xy, depth, 1.0);
        uv = vec2(uv_right, uv_top);
        EmitVertex();

		gl_PrimitiveID = n;
        gl_Position = vec4((transform * vec3(pos.x, pos.y + tile_h, 1.0)).xy, depth, 1.0);
        uv = vec2(uv_left, uv_bottom);
        EmitVertex();

		gl_PrimitiveID = n;
        gl_Position = vec4((transform * vec3(pos.x + tile_w, pos.y + tile_h, 1.0)).xy, depth, 1.0);
        uv = vec2(uv_right, uv_bottom);
        EmitVertex();
    }
)"""
};

Ref<gl::Program> Chunk::get_program(Resources &res) {
    return res.get<gl::Program>("tiles_program", tiles_shader_source);
}

Chunk::Chunk(
        Ref<gl::Program> program,
        Ref<Texture> diffuse,
        Vector2i tx_tile_size,
        std::vector<Tile> const &tiles,
        Vector2i tile_size,
        float depth,
        int pitch) :
        program(program),
        diffuse(std::move(diffuse)),
        tx_tile_size(tx_tile_size),
        tile_size(tile_size),
        pitch(pitch),
        depth(depth)
{
    gl::Bind(vao);
    gl::Bind(buffer);
    gl::VertexAttrib(*program, "in_tile").ipointer(1, gl::WholeDataType::kUnsignedShort, 5, nullptr).enable();
    gl::VertexAttrib(*program, "in_pos").ipointer(1, gl::WholeDataType::kUnsignedShort, 5, (void *) 2).enable();
    gl::VertexAttrib(*program, "in_flags").ipointer(1, gl::WholeDataType::kUnsignedByte, 5, (void *) 4).enable();

    auto data = std::vector<uint8_t>();
    for (auto const &t : tiles) {
        data.push_back((uint8_t) t.tile_id);
        data.push_back((uint8_t) (t.tile_id >> 8));
        data.push_back((uint8_t) t.pos);
        data.push_back((uint8_t) (t.pos >> 8));
        data.push_back(t.flags);
    }

    tile_count = static_cast<unsigned>(data.size());
    buffer.data(data);

    gl::Unbind(vao);
    gl::Unbind(buffer);
}

void Chunk::draw(Matrix3f transform) const {
    gl::DepthMask(true);
    gl::Enable(gl::Capability::kDepthTest);
    gl::Enable(gl::Capability::kBlend);
    gl::BlendEquation(gl::BlendEquation::kFuncAdd);
    gl::BlendFunc(gl::BlendFunction::kSrcAlpha, gl::BlendFunction::kOneMinusSrcAlpha);

    gl::Use(*program);

    auto inv_p = 1.0f / parallax_depth;
    transform = scale(inv_p) * transform;

    gl::Uniform<Matrix3f>(*program, "transform").set(transform);
    gl::Uniform<float>(*program, "depth").set(depth);
    gl::Uniform<int32_t>(*program, "tile_w").set(tile_size.x);
    gl::Uniform<int32_t>(*program, "tile_h").set(tile_size.y);
    gl::Uniform<int32_t>(*program, "t_tile_w").set(tx_tile_size.x);
    gl::Uniform<int32_t>(*program, "t_tile_h").set(tx_tile_size.y);
    gl::Uniform<int32_t>(*program, "t_pitch").set(tx_pitch());
    gl::Uniform<int32_t>(*program, "pitch").set(pitch);
    gl::Uniform<int32_t>(*program, "_smooth").set(smooth?1:0);

    gl::BindToTexUnit(diffuse->tx, 0);

    gl::Bind(vao);
    gl::DrawArrays(gl::PrimitiveType::kPoints, 0, tile_count);
    gl::Unbind(vao);
    gl::UnbindFromTexUnit(gl::kTexture2D, 0);
}
