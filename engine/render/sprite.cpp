#include "sprite.hpp"

#include <utility>
#include "gl.hpp"
#include "shader.hpp"
#include "poly.hpp"

auto sprite_shader_source = ProgramSource{
R"""(
    #version 330 core

    in vec2 in_pos;
    in vec2 in_uv;
    out vec2 uv;
    uniform mat3 view;
    uniform mat3 model;
    uniform mat3 tx_transform;
    uniform float depth;
    uniform float parallax;

    void main() {
        uv = (tx_transform * vec3(in_uv, 1.0)).xy;
        float inv_p = 1.0 / parallax;
        mat3 transform = mat3(inv_p, 0, 0,  0, inv_p, 0, 0, 0, inv_p) * view * model;
        gl_Position = vec4((transform * vec3(in_pos, 1.0)).xy, depth, 1.0);
    }
)""",
R"""(
    #version 330 core
    in vec2 uv;
    out vec4 fragColor;
    uniform vec4 tint;
    uniform sampler2D tx;
    uniform int _smooth;

    void main() {
        if (_smooth != 0) {
            ivec2 size = textureSize(tx, 0);
            fragColor = texture(tx, uv / vec2(size)) * tint;
        } else {
            fragColor = texelFetch(tx, ivec2(uv), 0) * tint;
        }
    }
)"""
};


Sprite::Sprite(
        Resources& res,
        Ref<Texture> tx,
        Ref<Transform> t,
        Vector2f size,
        Matrix3f tx_transform,
        Vector2f origin)
{
    this->program = res.get<gl::Program>("sprite_program", sprite_shader_source);
    this->t = std::move(t);
    this->tx = std::move(tx);
    this->tx_transform = tx_transform;

    auto data = std::vector<Vector2f> {
            vec2(0.f,    0.f) - origin,    vec2(0.f, 0.f),
            vec2(0.f,    size.y) - origin, vec2(0.f, 1.f),
            vec2(size.x, size.y) - origin, vec2(1.f, 1.f),
            vec2(size.x, size.y) - origin, vec2(1.f, 1.f),
            vec2(size.x, 0.f) - origin,    vec2(1.f, 0.f),
            vec2(0.f,    0.f) - origin,    vec2(0.f, 0.f),
    };

    gl::Bind(*vao);
    gl::Bind(*buffer);
    gl::VertexAttrib(*program, "in_pos").pointer(2, gl::DataType::kFloat, false, 16, nullptr).enable();
    gl::VertexAttrib(*program, "in_uv").pointer(2, gl::DataType::kFloat, false, 16, (void*)8).enable();
    this->verts = data.size();
    this->buffer->data(data);
    gl::Unbind(*vao);
    gl::Unbind(*buffer);
}

void Sprite::draw(Matrix3f view) const {
    if (!visible) return;
    auto t = *this->t + offset;
    auto model = t.matrix();

    gl::DepthMask(false);
    gl::Enable(gl::Capability::kDepthTest);
    gl::Enable(gl::Capability::kBlend);
    gl::BlendEquation(gl::BlendEquation::kFuncAdd);
    gl::BlendFunc(gl::BlendFunction::kSrcAlpha, gl::BlendFunction::kOneMinusSrcAlpha);

    gl::Use(*program);
    gl::Uniform<Matrix3f>(*program, "view").set(view);
    gl::Uniform<Matrix3f>(*program, "model").set(model);
    gl::Uniform<Matrix3f>(*program, "tx_transform").set(tx_transform);
    gl::Uniform<float>(*program, "depth").set(depth);
    gl::Uniform<float>(*program, "parallax").set(parallax_depth);
    gl::Uniform<Vector4f>(*program, "tint").set(tint);
    gl::Uniform<int>(*program, "_smooth").set((int)smooth);
    gl::UniformSampler(*program, "tx") = 0;
    gl::BindToTexUnit(tx->tx, 0);

    gl::Bind(*vao);
    gl::DrawArrays(gl::PrimitiveType::kTriangles, 0, (int) verts);
    gl::Unbind(*vao);
    gl::UnbindFromTexUnit(gl::kTexture2D, 0);
}

void Sprite::set_frame(unsigned frame) {
    anim.current_frame = wrap_max(frame, (unsigned) anim.tx_transform.size());
    anim.timer = 0.0;
    if(anim.tx_transform.size() > frame)
        tx_transform = anim.tx_transform[frame];
    if(anim.tx.size() > frame)
        tx = anim.tx[frame].clone();
}

void Sprite::update_anim(float msec) {
    if (anim.tx_transform.empty() && anim.tx.empty()) return;
    if (anim.delay[anim.current_frame] == 0) return;
    anim.timer += msec;
    if (anim.timer >= anim.delay[anim.current_frame]) {
        anim.timer -= anim.delay[anim.current_frame];

        auto frame = wrap_max(anim.current_frame + 1, (int) anim.tx_transform.size());
        anim.current_frame = frame;
        if(anim.tx_transform.size() > (unsigned) frame)
            tx_transform = anim.tx_transform[frame];
        if(anim.tx.size() > (unsigned) frame)
            tx = anim.tx[frame].clone();
    }
}

Sprite Sprite::instance(Ref<Transform> transform) const {
    auto sprite = Sprite();
    sprite.anim = anim;
    sprite.tx = tx.clone();
    sprite.tx_transform = tx_transform;
    sprite.t = std::move(transform);
    sprite.offset = offset;
    sprite.depth = depth;
    sprite.parallax_depth = parallax_depth;
    sprite.vao = vao;
    sprite.buffer = buffer;
    sprite.verts = verts;
    sprite.program = program.clone();
    sprite.tint = tint;
    return sprite;
}
