#pragma once

class Image {
public:
    unsigned width, height;
    std::vector<unsigned char> data{};

    Image(unsigned w, unsigned h, std::vector<unsigned char>&& data): width(w), height(h), data(data) {}
    ~Image() = default;

    /// Load a PNG image from a file
    static Image load_png(const std::string& path);
};
