#pragma once

#include "tileset.hpp"
#include "property.hpp"
#include "stage.hpp"
#include <tmxlite/Tileset.hpp>
#include <tmxlite/Property.hpp>
#include <tmxlite/Map.hpp>
#include <tmxlite/TileLayer.hpp>
#include <util/util.hpp>
#include <resources/resources.hpp>

Stage load_map(Resources& res, std::string const& path, Storage& storage);

Tileset load_tileset(Resources& res, std::string const& path, Storage& storage);

/// Load a map from the TMX format.
Stage map_from_tmx(Resources& res, tmx::Map& tmx, Storage& storage);

/// Load a tileset in the TMX format.
Tileset tileset_from_tmx(Resources& res, tmx::Tileset const &tmx, Storage& storage);

/// Convert a TMX property.
Property property_from_tmx(tmx::Property const& property);

/// Convert a set of TMX properties.
Properties properties_from_tmx(std::vector<tmx::Property> const& properties);

/// Create a Poly from a tmx object.
Poly poly_from_tmx(tmx::Object const& obj);

overload(Stage) load_resource(Resources& res, std::string const& id) {
    return load_map(res, id, res.storage);
}

overload(Tileset) load_resource(Resources& res, std::string const& id) {
    return load_tileset(res, id, res.storage);
}
