#pragma once

#include <physics/body.hpp>
#include <optional>

struct Colour { uint8_t r = 0, g = 0, b = 0, a = 255; };

/// A generic property of an object.
struct Property {
    enum class Kind { Float, Int, Bool, String, Color };

    /// The property's type
    Kind kind;
    union {
        float float_value;
        int int_value;
        bool bool_value;
        Colour color_value{};
    };
    std::string string_value;
};

/// A collection of properties
struct Properties {
    /// Map of names to property values
    std::map<std::string, Property> map{};

    /// Get a property given it's name
    std::optional<Property> get(std::string const& name) const {
        try {
            auto param = map.at(name);
            return {map.at(name)};
        }
        catch(std::exception&) {
            return {};
        }
    }

    /// Get the material specified by the properties
    Material get_material() const {
        auto material = Material();
        for (auto &property : map) {
            auto& value = property.second.float_value;
            if (property.first == "density") material.density = value;
            if (property.first == "static_friction") material.static_friction = value;
            if (property.first == "kinetic_friction") material.kinetic_friction = value;
            if (property.first == "elasticity") material.elasticity = value;
            if (property.first == "separation") material.separation = value;
        }
        return material;
    }
};
