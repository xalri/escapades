#pragma once

#include <fstream>
#include <json/json.h>
#include <resources/resources.hpp>
#include <physics/body.hpp>
#include <util/cwd.hpp>
#include <render/sprite.hpp>
#include "tileset.hpp"

/// An Object prototype.
struct Prototype {
    Body body;
    Sprite sprite;
};

inline Vector2f json_vec(Json::Value v) {
    return vec2(v[0].asFloat(), v[1].asFloat());
}

overload(Prototype) load_resource(Resources& res, std::string const& id) {
    auto root = Json::Value();
    auto file = std::ifstream();
    file.open(id);
	if (!file.is_open()) throw std::runtime_error("Failed to open file " + id);
    file >> root;

    if(root["sprite_idx"].isNull()) throw std::runtime_error("prototype JSON missing sprite_idx");
    if(root["spriteset"].isNull()) throw std::runtime_error("prototype JSON missing spriteset");

    auto sprites = res.get<Tileset>(cwd() + root["spriteset"].asString());
    auto sprite_idx = root["sprite_idx"].asInt();
    auto origin = json_vec(root["origin"]);
    auto size = json_vec(root["size"]);
    auto orientation = root["orientation"].asFloat();

    auto& s = res.storage;

    auto sprite = sprites->get_sprite(res, sprite_idx, size, s.emplace<Transform>(), origin);
    sprite.offset.orientation = orientation;
    sprite.depth = root.get("depth", 0.0).asFloat();
    sprite.smooth = root.get("smooth", true).asBool();

    auto m = Material();
    if (!root["material"].isNull()) {
        auto json = root["material"];
        m.density = json.get("density", m.density).asFloat();
        m.elasticity = json.get("elasticity", m.elasticity).asFloat();
        m.kinetic_friction = json.get("kinetic_friction", m.kinetic_friction).asFloat();
        m.static_friction = json.get("static_friction", m.static_friction).asFloat();
        m.separation = json.get("separation", m.separation).asFloat();
    }

    auto body = Body::dynamic(
            s.create(sprites->get_poly(sprite_idx, size, origin, s.box<Poly>())),
            s.create<Material>(m),
            s.emplace<Transform>());
    body.hull->transform(rotate(orientation));

    if (root.get("static", false).asBool())  {
        body.inv_inertia = 0.0;
        body.inv_mass = 0.0;
        body.is_static = true;
    }

    return Prototype{ std::move(body), std::move(sprite) };
}

/// An object in the game world. Combines a sprite with a collidable body.
class Object {
public:
    Ref<Transform> transform{};
    Ref<Body> body{};
    Ref<Sprite> sprite{};

    /// Load the object from a prototype.
    Object(Storage& s, Prototype const& proto):
            transform(s.emplace<Transform>()),
            body(s.create(proto.body.instance(transform.clone()))),
            sprite(s.create(proto.sprite.instance(transform.clone()))) { }

    /// Load the object from a prototype given it's resource path.
    Object(Resources& res, std::string const& id): Object(res.storage, *res.get<Prototype>(id)) { }

    Object() = default;

    Object instance(Storage &s) {
        Object o{};
        auto t = s.emplace<Transform>(body.valid() ? *body->t : *sprite->t);
        if (body.valid()) {
            o.body = s.create<Body>(body->instance(t));
        }
        if (sprite.valid()) o.sprite = s.create<Sprite>(sprite->instance(t));
        return o;
    }

};
