#pragma once

#include <vector>
#include <memory>
#include <storage/storage.hpp>
#include <math/math.hpp>
#include <math/poly.hpp>
#include <physics/body.hpp>
#include <render/sprite.hpp>
#include <render/texture.hpp>
#include "property.hpp"

/// Identifies a tile in a collection of tilesets
struct TileID { int32_t tileset_id, tile_id, flags = 0; };

struct TileGeometry {
    std::string name;
    std::string type;
    Ref<Poly> poly{};
    Properties properties{};
    Ref<Material> material{};
};

/// A set of tiles, either made of a single texture with many tiles or one
/// texture per tile. Tiles can have associated geometry, properties, and
/// animation.
struct Tileset {
    struct Tile {
        /// One frame of an animation
        struct Frame {
            float delay;
            uint32_t tile_id;
        };

        Properties properties{};
        std::string type;
        std::vector<Frame> anim{};
        std::vector<TileGeometry> geometry{};
        Ref<Texture> diffuse{};
        Vector2i size;
    };

    Tileset() = default;
    Tileset(Tileset&) = delete;
    Tileset(Tileset&&) = default;
    Tileset& operator=(Tileset&) = delete;
    Tileset& operator=(Tileset&&) = default;

    std::string name;
    std::vector<Tile> tiles{};
    bool is_split = false;
    bool smooth = true;
    Ref<Texture> diffuse{};
    Vector2i tile_size{};
    uint32_t first_gid;

    /// Get a tile as a sprite.
    Sprite get_sprite(Resources& res, uint32_t local_id, Vector2f size, Ref<Transform> transform, Vector2f origin = vec2(0.f, 0.f), uint32_t flip_flags = 0) const;

    Poly get_poly(uint32_t local_id, Vector2f size, Vector2f origin, Box<Poly> const& polys) const;

    /// Get the position in tiles in a single-image tileset for a given tile ID.
    Vector2i pos(uint32_t local_id) const {
        auto pitch = (int)(diffuse->width / tile_size.x);
        return vec2<int>(local_id % pitch, local_id / pitch);
    }
};
