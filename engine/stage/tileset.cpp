#include "tileset.hpp"

Sprite Tileset::get_sprite(Resources& res, uint32_t local_id, Vector2f size, Ref<Transform> transform, Vector2f origin, uint32_t flip_flags) const {
    if (local_id > tiles.size()) {
        throw std::invalid_argument("Tileset::get_sprite invalid tile ID");
    }
    auto tile_size = tiles[local_id].size.cast<float>();

    bool flip_horizontally = (flip_flags & 0x8) != 0;
    bool flip_vertically = (flip_flags & 0x4) != 0;
    //bool flip_diagonally = (flip_flags & 0x2) != 0;

    // Make a transformation from UV to texels for the given tile.
    auto make_transform = [&](uint32_t local_id) -> Matrix3f {
        auto t = is_split ?
            scale(tile_size) :
            scale(tile_size) * translate(pos(local_id).cast<float>());
        if (flip_horizontally) t = t * translate(0.5f, 0.f) * flip_x() * translate(-0.5f, 0.f);
        if (flip_vertically) t = t * translate(0.f, 0.5f) * flip_y() * translate(0.f, -0.5f);
        return t;
    };

    auto tx = is_split ? tiles[local_id].diffuse : this->diffuse;
    auto sprite = Sprite(res, tx, std::move(transform), size, make_transform(local_id), origin);

    sprite.smooth = this->smooth;
    if (auto smooth = tiles[local_id].properties.get("smooth")) sprite.smooth = smooth->bool_value;

    if(!tiles[local_id].anim.empty()) {
        for(auto frame : tiles[local_id].anim) {
            sprite.anim.delay.push_back(frame.delay);
            sprite.anim.tx_transform.push_back(make_transform(frame.tile_id));
            if (is_split) sprite.anim.tx.push_back(tiles[frame.tile_id].diffuse);
        }
        sprite.set_frame(0);
    }

    return sprite;
}

Poly Tileset::get_poly(uint32_t local_id, Vector2f size, Vector2f origin, Box<Poly> const& polys) const {
    auto sprite_size = tiles[local_id].size.cast<float>();
    auto scale = size / sprite_size;
    auto poly = polys[tiles[local_id].geometry[0].poly].clone();
    poly.transform(::translate(-origin) * ::scale(scale));
    return poly;
}
