#pragma once

#include <vector>
#include <functional>
#include <optional>
#include <render/tiles.hpp>
#include <resources/resources.hpp>
#include "tileset.hpp"

/// A static map layer made up of chunks of tiles. Each chunk is a 16x16 block
/// of tiles stored on the GPU. Best used with as many tiles as possible in a
/// single texture.
class TileLayer {
private:
    Vector2i layer_size, tile_size;
    uint32_t x_chunks = 0, y_chunks = 0;
    std::vector<std::vector<std::vector<Chunk>>> chunks;

public:
    float depth;

    /// The width & height of a chunk in tiles.
    static const unsigned CHUNK_SIZE = 16;

    /// Create a tile layer. Splits the data into chunks and uploads to the GPU.
    TileLayer(
            Resources& res,
            const std::function<std::optional<TileID>(int, int)> &tile,
            const std::function<Tileset const& (int)> &tileset,
            Vector2i layer_size,
            Vector2i tile_size,
            float depth = 0.5,
            float parallax = 1.0);

    /// Draw the layer, culls to the given world-space bounds.
    void draw(Matrix3f transform, Bounds<float> bounds) const;
};
