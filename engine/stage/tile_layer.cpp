#include "tile_layer.hpp"

TileLayer::TileLayer(
        Resources& res,
        const std::function<std::optional<TileID>(int, int)> &tile,
        const std::function<Tileset const& (int)> &tileset,
        Vector2i layer_size,
        Vector2i tile_size,
        float depth,
        float parallax)
{
    this->layer_size = layer_size;
    this->tile_size = tile_size;
    this->depth = depth;
    auto chunk_size = CHUNK_SIZE;

    // Temporary storage for chunk data, one per tileset since each chunk
    // can only hold tiles from a single set.
    auto data = std::vector<std::vector<Chunk::Tile>>();

    x_chunks = layer_size.x / chunk_size + 1;
    y_chunks = layer_size.y / chunk_size + 1;
    chunks.reserve(x_chunks);

    for(unsigned chunk_x = 0; chunk_x < x_chunks; chunk_x++) {
        chunks.emplace_back();
        chunks.back().reserve(y_chunks);

        for(unsigned chunk_y = 0; chunk_y < y_chunks; chunk_y++) {
            chunks[chunk_x].emplace_back();

            for (auto &i : data) i.clear();

            // Get the range of tiles to be included in this chunk
            auto map_x = Range<int32_t>(chunk_x * chunk_size, (chunk_x + 1) * chunk_size - 1);
            auto map_y = Range<int32_t>(chunk_y * chunk_size, (chunk_y + 1) * chunk_size - 1);

            // Wrap to the size of the map
            if (map_x.max >= layer_size.x) map_x.max = layer_size.x - 1;
            if (map_y.max >= layer_size.y) map_y.max = layer_size.y - 1;

            // Iterate over the tiles in this chunk
            for(auto x : map_x) {
                for(auto y : map_y) {
                    if (auto t = tile(x, y)) {
                        auto chunk_idx = x - map_x.min + (y - map_y.min) * chunk_size;

                        while ((int)data.size() < t->tileset_id + 1) {
                            data.emplace_back();
                            data.back().reserve(chunk_size * chunk_size);
                        }

                        data[t->tileset_id].push_back(Chunk::Tile {
                                static_cast<uint16_t>(t->tile_id),
                                static_cast<uint8_t>(chunk_idx),
                                static_cast<uint8_t>(t->flags)
                        });
                    }
                }
            }

            for (unsigned i = 0; i < data.size(); i++) {
                if (data[i].empty()) continue;
                auto& _tileset = tileset(i);
                chunks[chunk_x][chunk_y].emplace_back(
                        Chunk::get_program(res), _tileset.diffuse, _tileset.tile_size, data[i], tile_size, depth);
                chunks[chunk_x][chunk_y].back().parallax_depth = parallax;
            }
        }
    }
}

void TileLayer::draw(Matrix3f transform, Bounds<float> bounds) const {
    auto chunk_w = tile_size.x * CHUNK_SIZE;
    auto chunk_h = tile_size.y * CHUNK_SIZE;
    auto x_range = Range<int>(
            clamp((int)(bounds.x.min / chunk_w),     0,  (int)x_chunks - 1),
            clamp((int)(bounds.x.max / chunk_w) + 1, 0,  (int)x_chunks - 1));

    auto y_range = Range<int>(
            clamp((int)(bounds.y.min / chunk_h),     0,  (int)y_chunks - 1),
            clamp((int)(bounds.y.max / chunk_h) + 1, 0,  (int)y_chunks - 1));

    for(auto x : x_range) {
        for(auto y : y_range) {
            auto t = transform * translate((float)x * (float)chunk_w, (float)y * (float)chunk_h);
            for (auto& chunk : chunks[x][y]) chunk.draw(t);
        }
    }
}
