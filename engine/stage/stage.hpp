#pragma once

#include <vector>
#include <physics/body.hpp>
#include <storage/storage.hpp>
#include <render/sprite.hpp>
#include <render/poly.hpp>
#include "tileset.hpp"
#include "tile_layer.hpp"
#include "property.hpp"

struct MapObject {
    Ref<Body> body{};
    Ref<Sprite> sprite{};
    std::string name;
    std::string type;
    Properties properties{};
    bool alive = true;

    MapObject instance(Storage &s) {
        MapObject o{};
        o.name = name;
        o.type = type;
        o.properties = properties;

        auto t = s.emplace<Transform>(body.valid() ? *body->t : *sprite->t);
        if (body.valid()) {
            o.body = s.create<Body>(body->instance(t));
        }
        if (sprite.valid()) o.sprite = s.create<Sprite>(sprite->instance(t));
        return o;
    }

    void init_component(Weak<MapObject> self) {
        if (body.valid()) body->parent = self.any();
    }
};

struct Stage {
    // Static things:
    /// Tilesets
    std::shared_ptr<std::vector<Tileset>> tilesets = std::make_shared<std::vector<Tileset>>();
    /// Tile layers.
    std::shared_ptr<std::vector<TileLayer>> tile_layers = std::make_shared<std::vector<TileLayer>>();
    /// Visible non-textured polys
    std::shared_ptr<std::vector<FilledPoly>> filled_polys = std::make_shared<std::vector<FilledPoly>>();
    Colour bg_colour{};

    // Things which can change between instances:
    /// Sprites, including animated tiles
    std::vector<Ref<Sprite>> sprites{};
    /// Collidable objects.
    std::vector<Ref<MapObject>> objects{};

    /// Get the tileset & local tile ID for a global tile ID.
    TileID get_tile(unsigned global_id) const {
        auto tileset_id = (unsigned)(tilesets->size() - 1);
        while(global_id < (*tilesets)[tileset_id].first_gid) tileset_id--;
        auto local_tile_id = global_id - (*tilesets)[tileset_id].first_gid;
        return TileID { (int) tileset_id, (int) local_tile_id };
    }

    /// Create an instance of the stage in the given storage.
    Stage instance(Storage &s) {
        Stage stage{};
        stage.bg_colour = bg_colour;
        stage.tilesets = tilesets;
        stage.tile_layers = tile_layers;
        stage.filled_polys = filled_polys;
        for (auto& o : objects) stage.objects.push_back(s.create<MapObject>(o->instance(s)));
        for (auto& x : sprites) {
            auto t = s.emplace<Transform>(*x->t);
            stage.sprites.push_back(s.create<Sprite>(x->instance(t)));
        }
        return stage;
    }
};

