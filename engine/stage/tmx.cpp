#include <pugixml.hpp>
#include "tmx.hpp"
#include "math/math.hpp"

Tileset tileset_from_tmx(Resources& res, tmx::Tileset const &t, Storage& storage) {
    auto tileset = Tileset();
    tileset.first_gid = t.getFirstGID();
    tileset.tile_size = vec2<int>(t.getTileSize().x, t.getTileSize().y);
    tileset.is_split = t.getImagePath().empty();
    tileset.name = t.getName();

    // Load tileset texture
    if (!tileset.is_split) {
        tileset.diffuse = res.get<Texture>(t.getImagePath());
    }

    // Get the number of tiles.
    auto tile_count = t.getTileCount();
    for (auto tile : t.getTiles()) if (tile.ID >= tile_count) tile_count = tile.ID + 1;
    tileset.tiles.resize(tile_count);

    for (auto& tile : tileset.tiles) {
        tile.size = tileset.tile_size;
    }

    auto default_material = storage.emplace<Material>();
    auto default_poly = storage.create(Poly::rect((float)tileset.tile_size.x, (float)tileset.tile_size.y, false));

    // Get tile geometry, textures & properties from the tmx data
    for (auto& tmx_tile : t.getTiles()) {
        auto& tile = tileset.tiles[tmx_tile.ID];

        tile.properties = properties_from_tmx(tmx_tile.properties);
        tile.type = tmx_tile.type;

        if (tileset.is_split) {
            tile.diffuse = res.get<Texture>(tmx_tile.imagePath);
            tile.size = vec2<int>(tile.diffuse->width, tile.diffuse->height);
        }

        for (auto& frame : tmx_tile.animation.frames) {
            tile.anim.push_back({ (float)frame.duration, frame.tileID });
        }

        for (auto& obj : tmx_tile.objectGroup.getObjects()) {
            auto poly = poly_from_tmx(obj);
            auto properties = properties_from_tmx(obj.getProperties());
            // Update the poly such that it's origin is the top-right corner of the tile
            auto offset = Transform {
                    vec2(obj.getPosition().x, obj.getPosition().y),
                    obj.getRotation()
            };
            poly.transform(offset.matrix());
            auto _material = properties_from_tmx(obj.getProperties()).get_material();
            auto material = (_material == Material()) ? default_material.clone() : storage.create(std::move(_material));

            tile.geometry.push_back({
                  obj.getName(), obj.getType(), storage.create(std::move(poly)), properties, std::move(material)
            });
        }

        // TODO: should this exist? the semantics aren't obvious.
        if (tile.geometry.empty()) {
            tile.geometry.push_back({
                    "", "", default_poly.clone(), Properties(), default_material.clone()
            });
        }
    }

    return tileset;
}


Property property_from_tmx(tmx::Property const& property) {
    auto p = Property();
    if (property.getType() == tmx::Property::Type::Boolean) {
        p.kind = Property::Kind::Bool;
        p.bool_value = property.getBoolValue();
    } else if (property.getType() == tmx::Property::Type::Float) {
        p.kind = Property::Kind::Float;
        p.float_value = property.getFloatValue();
    } else if (property.getType() == tmx::Property::Type::Int) {
        p.kind = Property::Kind::Int;
        p.int_value = property.getIntValue();
    } else if (property.getType() == tmx::Property::Type::String || property.getType() == tmx::Property::Type::File) {
        p.kind = Property::Kind::String;
        p.string_value = property.getStringValue();
    } else if (property.getType() == tmx::Property::Type::Colour) {
        p.color_value = reinterpret_cast<Colour const&>(property.getColourValue());
    } else {
        throw unimplemented();
    }
    return p;
}

Properties properties_from_tmx(std::vector<tmx::Property> const &_properties) {
    auto properties = Properties();
    for(auto& p : _properties) {
        properties.map[p.getName()] = property_from_tmx(p);
    }
    return properties;
}

Poly poly_from_tmx(tmx::Object const &obj) {
    auto points = std::vector<Vector2f>();

    if (obj.getShape() == tmx::Object::Shape::Polygon || obj.getShape() == tmx::Object::Shape::Polyline) {
        for (auto &point : obj.getPoints()) {
            points.push_back(vec2<float>(point.x, point.y));
        }
    } else if (obj.getShape() == tmx::Object::Shape::Rectangle) {
        auto box = obj.getAABB();
        return Poly::rect(box.width, box.height, false);
    } else if (obj.getShape() == tmx::Object::Shape::Ellipse) {
        auto box = obj.getAABB();
        auto ellipse = Poly::ellipse(20, box.width, box.height);
        ellipse.transform(translate(box.width / 2.f, box.height / 2.f));
        return ellipse;
    } else if (obj.getShape() == tmx::Object::Shape::Text) {
        throw unimplemented();
    }

    return Poly(points);
}

/// Parameters for reading map layers
struct LayerParams {
    Resources& res;
    Storage& storage;
    Vector2i map_size;
    Vector2i tile_size;
    float depth = 0.5;
    float parallax = 1.0;
    bool collidable = false;

    void update(Properties const& p) {
        if(auto value = p.get("parallax")) parallax = value->float_value;
        if(auto value = p.get("collidable")) collidable = value->bool_value;
    }
};

void read_object_group(Stage &map, tmx::ObjectGroup *group, LayerParams params) {
    auto depth = params.depth;
    group->sortObjects();
    for (auto& obj : group->getObjects()) {
        depth += 0.00001;
        auto properties = properties_from_tmx(obj.getProperties());
        uint32_t collision_id = 0;
        uint32_t collision_mask = 0;
        if (auto id = properties.get("collision_id")) collision_id = (uint32_t) id->int_value;
        if (auto id = properties.get("collision_mask")) collision_mask = (uint32_t) id->int_value;

        if (obj.getTileID() != 0) {
            auto size = vec2<float>(obj.getAABB().width, obj.getAABB().height);
            auto gid = obj.getTileID();
            static const std::uint32_t mask = 0xf0000000;
            auto flip_flags = ((gid & mask) >> 28);
            bool flip_horizontally = (flip_flags & 0x8) != 0;
            bool flip_vertically = (flip_flags & 0x4) != 0;
            auto id = gid & ~mask;
            auto tile = map.get_tile(id);
            auto& tileset = (*map.tilesets)[tile.tileset_id];
            auto& tile_data = tileset.tiles[tile.tile_id];

            auto origin = size / 2;
            origin.y = -origin.y;
            auto transform = params.storage.create(Transform {
                    (::rotate(obj.getRotation()) * origin) + vec2(obj.getPosition().x, obj.getPosition().y),
                    obj.getRotation()
            });

            auto sprite = tileset.get_sprite(params.res, tile.tile_id, size, transform.clone(), origin + vec2(0.0f, size.y), flip_flags);
            sprite.depth = depth;
            sprite.parallax_depth = params.parallax;

            if (auto smooth = properties.get("smooth")) sprite.smooth = smooth->bool_value;

            map.objects.push_back(params.storage.create(MapObject {
                    Ref<Body>{},
                    params.storage.create(std::move(sprite)),
                    obj.getName(),
                    obj.getType(),
                    properties,
            }));

            if (params.collidable && tile_data.geometry.size() > 0) {
                auto &geom = tile_data.geometry[0];

                auto poly = geom.poly->clone();
                auto w = (float) tile_data.size.x;
                auto h = (float) tile_data.size.y;
                auto t = ::translate(-origin) *
                         ::scale(size / tile_data.size.cast<float>()) *
                         ::translate(0.f, -h);
                if (flip_horizontally) t = t * translate(w / 2.f, 0.f) * flip_x() * translate(-w / 2.f, 0.f);
                if (flip_vertically) t = t * translate(0.f, h / 2.f) * flip_y() * translate(0.f, -h / 2.f);

                poly.transform(t);

                auto _material = properties.get_material();
                auto material = (_material == Material() || _material == *geom.material) ?
                        geom.material.clone() : params.storage.create(std::move(_material));
                auto dynamic = false;
                if (auto x = properties.get("dynamic")) dynamic = x->bool_value;

                auto body = dynamic?
                    Body::dynamic(
                        params.storage.create(std::move(poly)),
                        material,
                        transform.clone()
                    ) :
                    Body::immovable(
                        params.storage.create(std::move(poly)),
                        material,
                        transform.clone()
                    );

                body.collision_id = collision_id;
                body.collision_mask = collision_mask;
                auto _body = params.storage.create(std::move(body));
                _body->parent = map.objects.back().weak().any();
                map.objects.back()->body = _body;
            }
        } else {
            auto poly = poly_from_tmx(obj);
            auto origin = poly.centroid();
            poly.transform(::translate(-origin));

            // TODO: avoid duplicating default material
            auto material = properties.get_material();
            auto transform = params.storage.create(Transform {
                    (::rotate(obj.getRotation()) * origin) + vec2(obj.getPosition().x, obj.getPosition().y),
                    obj.getRotation()
            });

            auto dynamic = false;
            if (auto x = properties.get("dynamic")) dynamic = x->bool_value;

            auto poly_ref = params.storage.create(std::move(poly));

            if (auto x = properties.get("visible")) if (x->bool_value) {
                auto p = FilledPoly(params.res, poly_ref.clone(), transform.clone());
                p.depth = depth;
                p.parallax_depth = params.parallax;
                if (auto _c = properties.get("colour")) {
                    auto c = _c->color_value;
                    p.col = vec4(c.r / 255.f, c.g / 255.f, c.b / 255.f, c.a / 255.f);
                }
                map.filled_polys->push_back(p);
            }

            auto body = dynamic?
                Body::dynamic(
                    poly_ref,
                    params.storage.create<Material>(material),
                    transform.clone()
                ) :
                Body::immovable(
                    poly_ref,
                    params.storage.create<Material>(material),
                    transform.clone()
                );

            body.collision_id = collision_id;
            body.collision_mask = collision_mask;
            auto _body = params.storage.create(std::move(body));
            map.objects.push_back(params.storage.create(MapObject {
                    _body.clone(),
                    Ref<Sprite>{},
                    obj.getName(),
                    obj.getType(),
                    properties,
            }));
            _body->parent = map.objects.back().weak().any();
        }
    }
}

void read_tile_layer(Stage &map, tmx::TileLayer *layer, LayerParams params) {
    auto get_tile = [&](int x, int y) -> std::optional<TileID> {
        auto _tile = layer->getTiles()[x + y * params.map_size.x];
        if (_tile.ID == 0) return {};

        auto t = map.get_tile(_tile.ID);
        t.flags = _tile.flipFlags;
        auto& tileset = (*map.tilesets)[t.tileset_id];
        auto& tile_data = tileset.tiles[t.tile_id];

        auto pos = (vec2(x, y + 1) * params.tile_size).cast<float>();

        auto transform = params.storage.create(Transform{ pos });
        transform->position.y -= (float)tileset.tile_size.y;

        if (params.collidable) {
            uint32_t collision_id = 0;
            if (auto id = tile_data.properties.get("collision_id")) collision_id = (uint32_t) id->int_value;
            // TODO: overwriting material

            for(auto& obj : tile_data.geometry) {
                auto body = Body::immovable(obj.poly.clone(), obj.material.clone(), transform.clone());
                body.collision_id = collision_id;
                auto _body = params.storage.create(std::move(body));
                map.objects.push_back(params.storage.create(MapObject {
                        _body.clone(),
                        Ref<Sprite>{},
                        "",
                        tile_data.type,
                        tile_data.properties,
                }));
                _body->parent = map.objects.back().weak().any();
            }
        }

        if (tileset.is_split || !tile_data.anim.empty()) {
            // Animated tiles and tiles with a separate texture are added as sprites
            auto sprite = tileset.get_sprite(params.res, t.tile_id, tile_data.size.cast<float>(), transform.clone());
            sprite.depth = params.depth;
            sprite.parallax_depth = params.parallax;

            map.sprites.push_back(params.storage.create(std::move(sprite)));
            return {};
        } else {
            return {t};
        }
    };

    auto get_tileset = [&](int tileset_id) -> Tileset const& {
        return (*map.tilesets)[tileset_id];
    };

    map.tile_layers->emplace_back(
            params.res,
            get_tile,
            get_tileset,
            params.map_size,
            params.tile_size,
            params.depth,
            params.parallax
    );
}

Stage map_from_tmx(Resources& res, tmx::Map &tmx, Storage& storage) {
    auto map = Stage();
    map.bg_colour = reinterpret_cast<Colour const&>(tmx.getBackgroundColour());

    // Load tilesets.
    for(auto& t : tmx.getTilesets()) map.tilesets->push_back(tileset_from_tmx(res, t, storage));

    auto map_size = vec2<int>(tmx.getTileCount().x, tmx.getTileCount().y);
    auto tile_size = vec2<int>(tmx.getTileSize().x, tmx.getTileSize().y);

    // Use layer depths between 0.25 and 0.75
    auto depth = 0.75f;
    auto depth_step = 0.5f / ((float)tmx.getLayers().size() - 1);

    for(auto& tmx_layer : tmx.getLayers()) {
        auto layer_conf = LayerParams{ res, storage, map_size, tile_size, depth };

        if (tmx_layer->getType() == tmx::Layer::Type::Object) {
            auto obj_group = dynamic_cast<tmx::ObjectGroup*>(tmx_layer.get());
            // TODO: getProperties on the tmx_layer is empty for object groups. bug in tmxlite?
            auto properties = properties_from_tmx(obj_group->getProperties());
            layer_conf.update(properties);
            read_object_group(map, obj_group, layer_conf);
        }
        if (tmx_layer->getType() == tmx::Layer::Type::Tile) {
            auto tile_layer = dynamic_cast<tmx::TileLayer*>(tmx_layer.get());
            auto properties = properties_from_tmx(tile_layer->getProperties());
            layer_conf.update(properties);
            read_tile_layer(map, tile_layer, layer_conf);
        }
        depth -= depth_step;
    }

    return map;
}

Stage load_map(Resources& res, std::string const &path, Storage& storage) {
    auto tmx = tmx::Map();
    tmx.load(path);
    return map_from_tmx(res, tmx, storage);
}

Tileset load_tileset(Resources &res, std::string const &path, Storage& storage) {
    pugi::xml_document doc;
    auto result = doc.load_file(path.c_str());
    if (!result) throw std::runtime_error(result.description());

    std::string dir;
    std::size_t i = path.rfind('/', path.length());
    if (i != std::string::npos) dir = path.substr(0, i);

    auto tmx = tmx::Tileset(dir);
    auto node = doc.root().child("tileset");
    node.append_attribute("firstgid").set_value(1);
    tmx.parse(node);
    return tileset_from_tmx(res, tmx, storage);
}
