#include <optional>
#include "collision.hpp"

// ================================================================= //
//                       Intersection test
// ================================================================= //

std::optional<Intersection> Intersection::test(const ConvexBody &body_a, const ConvexBody &body_b) {
    auto oa = body_a.orientation;
    auto ob = body_b.orientation;
    auto ob_ = ob.transpose();

    // parameters of body_a in the reference frame of body_b
    auto orientation = oa * ob_;
    auto offset = ob_ * (body_a.pos - body_b.pos);
    auto velocity = ob_ * (body_a.vel - body_b.vel);

    // Track the minimum overlap and latest future collision.
    auto collision = Intersection{ vec2<float>(0, 0), -std::numeric_limits<float>::min() };
    auto overlap = Intersection{ vec2<float>(0, 0), -std::numeric_limits<float>::max() };

    // Test for collision against a given axis, updates the minimum collision
    // and overlap, and returns true if there's an intersection.
    auto test = [&](Vector2f normal) {
        auto i = Intersection::axis_intersect(
                normal, *body_a.hull, *body_b.hull, orientation, offset, velocity, 1.0,
                body_a.scale, body_b.scale);
        if (!i) return false;

        if (i->t > collision.t) collision = *i;
        else if (i->t > overlap.t) overlap = *i;
        return true;
    };

    // Check possible axes, returns `std::nullopt` if the bodies are separated along any axis.
    for (auto s : body_a.hull->segments()) {
        auto vec = orientation * vec2<float>(s.a.y - s.b.y, s.b.x - s.a.x);
        if (!test(vec)) return {};
    }

    for (auto s : body_b.hull->segments()) {
        auto vec = vec2<float>(s.a.y - s.b.y, s.b.x - s.a.x);
        if (!test(vec)) return {};
    }

    auto min = (collision.t >= 0) ? collision : overlap;
    min.normal = ob * min.normal.normalize();
    auto center_a = (oa * body_a.hull->centroid()) + body_a.pos;
    auto center_b = (ob * body_b.hull->centroid()) + body_b.pos;
    if (min.normal.dot(center_a - center_b) < 0.0) {
        min.normal = -min.normal;
    }

    return { min };
}


std::optional<float> smallest_positive(float a, float b) {
    if (a >= 0.0 && b >= 0.0) { return {std::min(a, b)}; }
    else if (a >= 0.0) { return {a}; }
    else if (b >= 0.0) { return {b}; }
    else { return {}; }
}

std::optional<Intersection>
Intersection::axis_intersect(Vector2f axis, const Poly &hull_a, const Poly &hull_b, Matrix3f orientation,
                             Vector2f offset, Vector2f velocity, float max_time, float scale_a, float scale_b) {
    // Project each hull onto the axis.
    auto projection_a = hull_a.project(orientation.transpose() * axis) * scale_a;
    auto projection_b = hull_b.project(axis) * scale_b;

    // Adjust by the relative offset
    projection_a = projection_a + offset.dot(axis);

    auto d0 = projection_a.min - projection_b.max;
    auto d1 = projection_b.min - projection_a.max;

    if (d0 >= 0.0 || d1 >= 0.0) {
        // Calculate the time in frames to the collision
        auto v = velocity.dot(axis);
        if (v == 0) return {};
        if (auto time = smallest_positive(-d0/v, d1/v)) {
            if (*time < max_time) { return {Intersection{ axis, *time }}; }
        }
        return {};
    } else {
        auto len = axis.length();
        auto t = std::max(d0, d1) / len;
        axis /= len;
        return {Intersection{ axis, t }};
    }
}

bool Intersection::operator==(const Intersection &rhs) const {
    return normal == rhs.normal && t == rhs.t;
}
bool Intersection::operator!=(const Intersection &rhs) const {
    return !(rhs == *this);
}

std::ostream &operator<<(std::ostream &os, const Intersection &intersection) {
    os << "normal: " << intersection.normal << " t: " << intersection.t;
    return os;
}


// ================================================================= //
//                        Support points
// ================================================================= //

Support::Support(ConvexBody const &body, Intersection intersection) {
    auto normal = body.orientation.transpose() * intersection.normal;
    auto tangent = intersection.normal.perpendicular();
    auto pos = body.pos + body.vel * std::max(0.0f, intersection.t);

    auto closest = std::numeric_limits<float>::max();
    for(auto p : *body.hull) {
        auto dist = (p * body.scale).dot(normal);
        if (dist < closest) closest = dist;
    }

    auto candidates = 0;

    for(auto point : *body.hull) {
        auto p = point * body.scale;
        if (p.dot(normal) < closest + SUPPORT_EDGE_THRESHOLD) {
            candidates += 1;

            auto world_space = pos + body.orientation * p;
            auto perp_dist = world_space.dot(tangent);

            if (perp_dist < min_dist) {
                min = world_space;
                min_dist = perp_dist;
            }
            if (perp_dist > max_dist) {
                max = world_space;
                max_dist = perp_dist;
            }
        }
    }

    is_point = candidates == 1;
}

std::ostream &operator<<(std::ostream &os, const Support &support) {
    os << "a: " << support.min << " b: " << support.max << " a_dist: " << support.min_dist << " b_dist: " << support.max_dist
       << " is_point: " << support.is_point;
    return os;
}

Support::Support(const Vector2f &min, const Vector2f &max, float min_dist, float max_dist, bool is_point) :
        min(min), max(max), min_dist( min_dist), max_dist( max_dist), is_point(is_point) {}


// ================================================================= //
//                    Body collision detection
// ================================================================= //

std::optional<Contact> Contact::collide(Body const &body_a, Body const &body_b) {

    if(body_a.t->scale == 0 || body_b.t->scale == 0) return {};

    Intersection intersection;
    ConvexBody convex_a;
    ConvexBody convex_b;
    bool collision = false;

    // Find the earliest collision between the two bodies.
    for (auto& a : body_a.convex) {
        if (!a.bounds.intersects(body_b.bounds)) continue;
        for (auto& b : body_b.convex) {
            if (!b.bounds.intersects(a.bounds)) continue;
            if (a.hull == nullptr) std::abort();
            if (b.hull == nullptr) std::abort();

            if (auto c = Intersection::test(a, b)) {
                if (!collision || c->t < intersection.t) {
                    collision = true;
                    intersection = *c;
                    convex_a = a;
                    convex_b = b;
                }
            }
        }
    }

    if (!collision) return {};

    if (convex_a.hull == nullptr) std::abort();
    if (convex_b.hull == nullptr) std::abort();

    // Calculate support points
    auto support_a = Support(convex_a, intersection);
    auto support_b = Support(convex_b, intersection.invert_normal()).inv();

    // Calculate contact points from supports.
    if (support_a.is_point && support_b.is_point) {
        return { Contact{ intersection, false, support_a.min, support_b.min } };
    } else {
        auto range_a = support_a.range();
        auto range_b = support_b.range();
        if (!range_a.intersects(range_b)) return {};

        Vector2f ca, cb;

        if (range_a.min > range_b.min) {
            ca = support_a.min;
            cb = project_on_segment(support_a.min, support_b.min, support_b.max);
        } else {
            ca = project_on_segment(support_b.min, support_a.min, support_a.max);
            cb = support_b.min;
        }

        if (range_a.max < range_b.max) {
            ca += support_a.max;
            cb += project_on_segment(support_a.max, support_b.min, support_b.max);
        } else {
            ca += project_on_segment(support_b.max, support_a.min, support_a.max);
            cb += support_b.max;
        }
        ca /= 2.0;
        cb /= 2.0;

        auto flush = !support_a.is_point && !support_b.is_point;
        return { Contact{ intersection, flush, ca, cb } };
    }
}

std::ostream &operator<<(std::ostream &os, const Contact &contact) {
    os << "intersection: " << contact.intersection << " flush: " << contact.flush << " contact_a: " << contact.contact_a
       << " contact_b: " << contact.contact_b;
    return os;
}

bool Contact::operator==(const Contact &rhs) const {
    return intersection == rhs.intersection &&
           flush == rhs.flush &&
           contact_a == rhs.contact_a &&
           contact_b == rhs.contact_b;
}

bool Contact::operator!=(const Contact &rhs) const {
    return !(rhs == *this);
}


// ================================================================= //
//                       Collision resolution
// ================================================================= //

std::pair<Resolution, Resolution> Contact::resolve(Body const& body_a, Body const& body_b) const {
    auto time = intersection.t;
    auto normal = intersection.normal;

    auto& material_a = *body_a.material;
    auto& material_b = *body_b.material;

    auto elasticity = material_b.elasticity;
    auto static_friction = clamp((material_a.static_friction + material_b.static_friction) / 2.f, 0.f, 1.f);
    auto kinetic_friction = clamp((material_a.kinetic_friction + material_b.kinetic_friction) / 2.f, 0.f, 1.f);

    auto pa = body_a.t->position;
    auto pb = body_b.t->position;
    auto va = body_a.velocity;
    auto vb = body_b.velocity;
    auto wa = body_a.angular_vel;
    auto wb = body_b.angular_vel;
    auto ma = body_a.inv_mass;
    auto mb = body_b.inv_mass;
    auto ia = body_a.inv_inertia;
    auto ib = body_b.inv_inertia;

    // If there's an existing overlap adjust the positions to fix it.
    if (time < 0.0) {
        auto d = contact_b - contact_a;
        auto separation = std::max(material_a.separation, material_b.separation);
        auto multiplier = separation / (ma + mb);

        if (ma > 0.0) pa += d * ma * multiplier;
        if (mb > 0.0) pb += -d * mb * multiplier;

        time = 0;
    }

    // Contact point at time of impact, relative to the center of mass
    auto ra = contact_a - (pa + (va * time));
    auto rb = contact_b - (pb + (vb * time));

    // Velocity at contact point
    auto vpa = va + (ra.perpendicular() * deg2rad(wa));
    auto vpb = vb + (rb.perpendicular() * deg2rad(wb));

    // Impact velocity
    auto vc = vpa - vpb;
    auto v = vc.dot(normal);
    if (v > 0.0f) {
        // Bodies are already separating along the collision normal, no need to resolve.
        return std::make_pair(Resolution{pa, va, wa}, Resolution{pb, vb, wb});
    }

    // Remove elasticity when the velocity along the normal is small, or the
    // surfaces will never meet and there will be no kinetic friction.
    if (-v < 0.2f) elasticity = 0.0f;
    if (-v > 5.f) elasticity /= (-v / 5.f);

    auto vt = vc - (normal * v); // Transverse velocity
    if (vt.length_sq() > 0.00001) vt = vt.normalize(); // (can be zero, so check before normalising)

    // Inertia at the contact point.
    auto _ia = ia * std::pow(ra.perp_dot(normal), 2);
    auto _ib = ib * std::pow(rb.perp_dot(normal), 2);

    // Impulse
    auto denom = ma + mb + _ia + _ib;
    auto jn = normal * -(1.0f + elasticity) * v / denom; // impulse along the normal
    auto jt = vt * kinetic_friction * v / denom; // transverse impulse

    auto j = jn + jt; // combined

    auto solve_angular = [&](Vector2f r, float i, float w) {
        if (i == 0.0) { return w; }
        auto w_ = w + rad2deg(r.perp_dot(j) * i);
        // Apply damping when the direction changes
        if (w_ * w < 0.0) {
            w_ *= 0.85f;
            if (flush && std::abs(w_) < 1.5f) { w_ = 0.0; }
        }
        return w_;
    };

    auto solve_linear = [&](Vector2f v, Vector2f dv, float m) {
        if (m == 0.0) { return v; }
        v = v + (jn * m);
        auto _vt = vt * dv.dot(vt); // relative transverse velocity
        auto friction = jt * m;
        if (friction.dot(_vt) < 0.0) { // friction can only oppose transverse motion
            auto friction_magnitude = friction.length() * std::max(1.f, static_friction / kinetic_friction);
            if (friction_magnitude > _vt.length()) {
                // Friction exceeds transverse momentum, remove transverse velocity.
                //v -= _vt;
            } else {
                v += friction;
            }
        }
        return v;
    };

    return std::make_pair(
            Resolution{ pa, solve_linear(va, va - vb,  ma), solve_angular( ra, ia, wa) },
            Resolution{ pb, solve_linear(vb, vb - va, -mb), solve_angular( -rb, ib, wb) }
     );
}

// ================================================================= //
//                          Broad Phase
// ================================================================= //

void BroadPhase::find_collisions(Box<Body> &box) {
    auto n = box.meta->data.size();
    {
        auto block = profile.start_block("update body list");
        tree_ids.resize(n, -1);
        resize_contacts(box);

        // Remove dead pointers from the tree
        for (unsigned i = 0; i < n; i++) {
            if (tree_ids[i] == -1) continue;
            if (box.meta->refs[i] == 0) {
                dynamic_tree.DestroyProxy(tree_ids[i]);
                tree_ids[i] = -1;
            }
        }

        // Add new bodies to the tree
        for (unsigned idx = 0; idx < n; idx++) {
            if (tree_ids[idx] == -1 && box.meta->refs[idx] > 0 && (box.meta->data)[idx].collision_id != 0) {
                (box.meta->data)[idx].update_bounds();
                (box.meta->data)[idx].update_convex();
                auto bounds = (box.meta->data)[idx].bounds;
                tree_ids[idx] = dynamic_tree.CreateProxy(bounds, idx);
            }
        }
    }

    // empty contacts from last tick
    for(auto& collisions : contacts) collisions.clear();

    // Thread local contact storage, to avoid locking a mutex for each contact.
    static thread_local std::vector<Collision> thread_contacts{};

    // Tests for collisions between two bodies, adding contacts to the map
    auto test = [&](auto a, auto b) {
        auto& body_a = (box.meta->data)[a];
        auto& body_b = (box.meta->data)[b];

        if ((body_a.collision_mask & body_b.collision_id) != 0) {
            auto contact = Contact::collide(body_a, body_b);
            if (contact) thread_contacts.push_back(Collision{box.make_weak(b), *contact});
        }
    };

    {
        auto block = profile.start_block("calculate bounds");
        for(auto a = 0u; a < n; a++) {
            if (box.meta->refs[a] == 0) continue;
            if (!(box.meta->data)[a].is_static) {
                (box.meta->data)[a].update_bounds();
                (box.meta->data)[a].update_convex();
            }
        }
    }

    {
        auto block = profile.start_block("update dynamic tree");
        for (unsigned a = 0; a < n; a++) {
            if (box.meta->refs[a] == 0) continue;
            if (!(box.meta->data)[a].is_static) {
                if (tree_ids[a] != -1) {
                    dynamic_tree.MoveProxy(tree_ids[a], (box.meta->data)[a].bounds, (box.meta->data)[a].velocity);
                }
            }
        }
    }

    // Distribute narrow-phase tests among worker threads
    auto block = profile.start_block("narrow phase total");
    auto query_profile = profile.profile("query"); // avoid writes to `profile` in parallel loop..
    par::workers.split((int) n, [&](int a) {
        if (box.meta->refs[a] == 0) return;
        if ((box.meta->data)[a].collision_mask == 0) return;
        auto block = query_profile.start_block();

        auto callback = [&](auto _b) {
            auto b = dynamic_tree.GetUserData(_b);
            if (a != b) test(a, b);
            return true;
        };
        dynamic_tree.Query(callback, (box.meta->data)[a].bounds);

        if (!thread_contacts.empty()) {
            contacts_mutex.lock();
            contacts[a].reserve(thread_contacts.size());
			for (const auto &c : thread_contacts) contacts[a].push_back(c);
            contacts_mutex.unlock();
            thread_contacts.clear();
        }
    });
}

std::vector<Collision> const& BroadPhase::collisions(Weak<Body> const &ref) const {
    if(!ref.is_alive()) throw std::invalid_argument("BroadPhase::collisions dead weak ref");
    if(ref.data.index >= contacts.size()) std::abort();
    return contacts[ref.data.index];
}

std::vector<Collision> const& BroadPhase::collisions(Ref<Body> const &ref) const {
    if(ref.data.index >= contacts.size()) std::abort();
    return contacts[ref.data.index];
}

CollisionIter BroadPhase::collision_iter(Box<Body> &box) const {
    return CollisionIter(*this, box);
}
