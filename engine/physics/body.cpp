
#include <math/math.hpp>
#include "body.hpp"

Body Body::dynamic(Ref<Poly> hull, Ref<Material> material, Ref<Transform> t) {
    auto body = Body();
    body.t = std::move(t);
    body.hull = std::move(hull);
    body.material = std::move(material);
    body.inv_mass = 1.0;
    body.inv_inertia = 1.0;
    body.update_properties();
    return body;
}

Body Body::immovable(Ref<Poly> hull, Ref<Material> material, Ref<Transform> t) {
    auto body = Body();
    body.t = std::move(t);
    body.is_static = true;
    body.hull = std::move(hull);
    body.material = std::move(material);
    return body;
}

void Body::integrate() {
    t->position += velocity;
    t->orientation += angular_vel;
    t->orientation = wrap_max(t->orientation, 360.0f);
}

void Body::update_convex() {
    convex.clear();
    auto m = ::rotate(t->orientation);
    for(auto p : hull->convex()) {
        auto bounds = (hull->convex().size() == 1) ?
                this->bounds : p->swept_bounds(t->position, t->orientation, velocity, t->scale);
        convex.push_back(ConvexBody{ p, t->position, velocity, m, t->scale, bounds });
    }
}

void Body::update_bounds() {
    bounds = hull->swept_bounds(t->position, t->orientation, velocity, t->scale);
}

void Body::update_properties() {
    auto mass = hull->area() * material->density;
    auto inertia = mass * hull->inertia();
    if (inv_mass > 0.f) inv_mass = 1.f / mass;
    if (inv_inertia > 0.f) inv_inertia = 1.f / inertia;
}

Body Body::instance(Ref<Transform> transform) const {
    auto body = Body();
    body.hull = hull.clone();
    body.material = material.clone();
    body.t = std::move(transform);
    body.velocity = velocity;
    body.angular_vel = angular_vel;
    body.collision_id = collision_id;
    body.collision_mask = collision_mask;
    body.inv_mass = inv_mass;
    body.inv_inertia = inv_inertia;
    body.parent = parent;
    return body;
}

bool Material::operator==(const Material &rhs) const {
    return density == rhs.density &&
           static_friction == rhs.static_friction &&
           kinetic_friction == rhs.kinetic_friction &&
           elasticity == rhs.elasticity &&
           separation == rhs.separation;
}

bool Material::operator!=(const Material &rhs) const {
    return !(rhs == *this);
}
