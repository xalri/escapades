// Collision detection & impulse based resolution for concave bodies.
//
// Collision detection & contact points:
// First we find the earliest collision between the two bodies with the separating
// axis theorem, projecting the body's geometry against an the axes and adjusting
// based on their velocities, giving us either a time-to-impact for a collision in
// the next frame or a distance if the bodies already overlap as well as the
// collision normal.
// From the collision normal we find the vertices/segments involved in the
// collision, then the contact points for each body in world-space.
//
// Collision resolution:
// To resolve a collision, first we resolve any existing overlap by moving
// the bodies along the collision normal relative to their weights.
// We then calculate an impulse which is used to independently update the
// linear and angular velocity for each body, taking into account the
// elasticity and static & dynamic friction of the collision.
// It's assumed that each body's center of mass is it's centroid.
//
// Broad phase:
// To reduce the number of collision tests `BroadPhase` implements
// a broad phase using Box2D's dynamic AABB tree.
//
// sources:
// - http://www.euclideanspace.com/physics/dynamics/collision/
// - https://en.wikipedia.org/wiki/Collision_response#Impulse-based_contact_model
// - https://gafferongames.com/post/collision_response_and_coulomb_friction/
// - http://www.randygaul.net/2013/03/27/game-physics-engine-part-1-impulse-resolution/
// - http://chrishecker.com/images/e/e7/Gdmphys3.pdf
// - http://www.cs.cmu.edu/~baraff/sigcourse/notesd2.pdf
// - https://www.gamedev.net/forums/topic/346956-adding-vector-from-another-moving-object/
// - https://volgogradetzzz.blogspot.co.uk/2011/05/swept-sat.html

#pragma once

#include <algorithm>
#include <optional>
#include <unordered_map>
#include <vector>
#include <ostream>
#include <storage/storage.hpp>
#include <math/math.hpp>
#include <math/poly.hpp>
#include <util/time.hpp>
#include "body.hpp"
#include "b2DynamicTree.h"

static const float SUPPORT_EDGE_THRESHOLD = 0.2f;

/// An intersection between two bodies, used to calculate support points.
struct Intersection {
    /// The intersection normal
    Vector2f normal;

    /// The time to collision in ticks. A negative value represents the distance
    /// of an existing overlap.
    float t;

    Intersection() = default;

    /// Test for an intersection between two convex bodies.
    static std::optional<Intersection> test(ConvexBody const& a, ConvexBody const & b);

    /// Calculate the intersection of two polygons along one axis, parameters are
    /// in the reference frame of hull_b.
    static std::optional<Intersection> axis_intersect(Vector2f axis, const Poly &hull_a, const Poly &hull_b, Matrix3f orientation,
                                                          Vector2f offset, Vector2f velocity, float max_time, float scale_a, float scale_b);

    /// Invert the intersection normal
    inline Intersection invert_normal() const {
        return Intersection{ -normal, t };
    }

    friend std::ostream &operator<<(std::ostream &os, const Intersection &intersection);

    bool operator==(const Intersection &rhs) const;

    bool operator!=(const Intersection &rhs) const;
};

/// A supporting point or edge, used to calculate contact points.
struct Support {
    Vector2f min, max;
    // Perpendicular distance from the collision tangent.
    float min_dist = std::numeric_limits<float>::max();
    float max_dist = -std::numeric_limits<float>::max();
    bool is_point;

    Support(const Vector2f &min, const Vector2f &max, float min_dist, float max_dist, bool is_point);

    /// Find the world-space support points for a collided body
    explicit Support(ConvexBody const& body, Intersection intersection);

    /// Get the range along the collision tangent.
    inline Range<float> range() {
        return Range<float>(min_dist, max_dist);
    }

    /// Invert the distances
    inline Support inv() {
        return Support(max, min, -max_dist, -min_dist, is_point);
    }

    friend std::ostream &operator<<(std::ostream &os, const Support &support);
};

/// An update to a body to solve a collision
struct Resolution {
    Vector2f pos;
    Vector2f velocity;
    float angular_vel;
    Resolution() = delete;

    void apply(Body& body) const {
        body.t->position = pos;
        body.velocity = velocity;
        body.angular_vel = angular_vel;
    }
};

/// Information about a collision between two bodies
struct Contact {
    Intersection intersection;
    bool flush;
    Vector2f contact_a;
    Vector2f contact_b;

    Contact() = delete;

    /// Calculate contact points for two bodies.
    static std::optional<Contact> collide(Body const& body_a, Body const& body_b);

    /// Resolve the collision
    std::pair<Resolution, Resolution> resolve(Body const& body_a, Body const& body_b) const;

    friend std::ostream &operator<<(std::ostream &os, const Contact &contact);

    bool operator==(const Contact &rhs) const;

    bool operator!=(const Contact &rhs) const;
};

struct Collision {
    Weak<Body> body;
    Contact contact;
};

/// An iterator over the contacts for all bodies in a storage.
struct CollisionIter;

/// Collision broad-phase
class BroadPhase {
private:
    friend struct CollisionIter;
    
    b2DynamicTree dynamic_tree{};
    std::vector<int32_t> tree_ids{};
    std::mutex contacts_mutex{};
    std::vector<std::vector<Collision>> contacts{};

    void resize_contacts(Box<Body> &box) {
        if (contacts.size() < box.meta->data.size()) {
            contacts.resize(box.meta->data.size());
            for(auto& c : contacts) c.reserve(16);
        }
    }

public:
    BroadPhase() = default;

    /// Checks for collisions, adding collision data to the contacts map.
    /// If, on the first call to find_collisions after a body is added, the body's
    /// collision ID is 0, it's not added to the broad phase and won't register
    /// any collisions.
    void find_collisions(Box<Body> &box);

    /// Get the number of collisions in the last find_collisions call.
    unsigned long num_collisions() {
        auto sum = 0ul;
        for(auto c : contacts) sum += c.size();
        return sum;
    }

    /// Get the set of contacts for a single body.
    std::vector<Collision> const& collisions(Weak<Body> const& ref) const;
    std::vector<Collision> const& collisions(Ref<Body> const& ref) const;

    /// Get an iterator over the contacts for every body.
    CollisionIter collision_iter(Box<Body> &box) const;

    SplitProfile profile{};
};

typedef std::pair<Weak<Body>, std::vector<Collision> const&> BodyContacts;

/// An iterator over collisions
struct CollisionIter {
    BroadPhase const& contacts;
    Box<Body> const& box;
    explicit CollisionIter(BroadPhase const& contacts, Box<Body> const& box): contacts(contacts), box(box) {}
    struct it_state {
        int pos;

        inline void next(const CollisionIter* ref) {
            pos++;
            while (pos < (int) ref->contacts.contacts.size() && ref->contacts.contacts[pos].empty()) pos++;
        }
        inline void begin(const CollisionIter* ref) { pos = 0; }
        inline void end(const CollisionIter* ref) { pos = static_cast<int>(ref->contacts.contacts.size()); }
        inline bool cmp(const it_state& s) const { return pos != s.pos; }
        inline BodyContacts get(const CollisionIter* ref) {
            return { ref->box.make_weak(pos), ref->contacts.contacts[pos] };
        }
    };
    SETUP_CONST_ITERATOR(CollisionIter, BodyContacts, it_state);
};

