#pragma once

#include <typeindex>
#include <math/math.hpp>
#include <math/poly.hpp>
#include <storage/storage.hpp>

/// The properties of a body's material, used for collision response.
struct Material {
    float density = 1.0f;
    float static_friction = 0.4f;
    float kinetic_friction = 0.3f;
    float elasticity = 0.25f;
    float separation = 0.4f;

    bool operator==(const Material &rhs) const;
    bool operator!=(const Material &rhs) const;

    Material() = default;
};

struct ConvexBody;

/// A rigid body.
class Body {
private:
    Body() = default;

public:
    Ref<Poly> hull{};
    Ref<Material> material{};
    Ref<Transform> t{};

    Vector2f velocity = vec2(0.0f, 0.0f);
    float angular_vel = 0;
    unsigned collision_id = 0;
    unsigned collision_mask = 0;
    /// Inverse of the relative mass, set to 0 to prevent movement.
    float inv_mass = 0;
    /// Inverse of the relative rotational inertia, set to 0 to prevent rotation.
    float inv_inertia = 0;
    bool is_static = false;
    Bounds<float> bounds = Bounds<float>(range(0.f,0.f), range(0.f, 0.f));
    std::vector<ConvexBody> convex{};
    WeakAny parent{};

    // Movable, non-copyable.
    Body(Body&) = delete;
    Body& operator=(Body const&) = delete;
    Body(Body&&) = default;
    Body& operator=(Body&&) = default;

    /// Create a new dynamic body
    static Body dynamic(Ref<Poly> hull, Ref<Material> material, Ref<Transform> transform);

    /// Create a new immovable body
    static Body immovable(Ref<Poly> hull, Ref<Material> material, Ref<Transform> transform);

    /// Create a new instance of the body with a different transform, linked to
    /// the same poly and material.
    Body instance(Ref<Transform> transform) const;

    /// Recalculate mass & rotational inertia, should be called whenever
    /// the hull, density, or scale is changed.
    void update_properties();

    /// Increment the position & angle by the velocity & angular velocity respectively.
    void integrate();

    /// Update the collision bounds
    void update_bounds();

    /// Update the convex components
    void update_convex();

    /// Apply a force to the body from some point in world-space
    void push(Vector2f pos, Vector2f force) {
        pos = t->position - pos;
        velocity += force * inv_mass;
        angular_vel += rad2deg(-pos.perp_dot(force)) * inv_inertia;
    }
};

struct ConvexBody {
    std::shared_ptr<Poly> hull = nullptr;
    Vector2f pos;
    Vector2f vel;
    Matrix3f orientation;
    float scale;
    Bounds<float> bounds;
};
