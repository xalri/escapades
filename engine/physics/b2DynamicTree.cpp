// Copyright (c) 2009 Erin Catto http://www.box2d.org
// Minor modifications by Lewis Hallam, 2018-02-09
//
// This software is provided 'as-is', without any express or implied
// warranty.  In no event will the authors be held liable for any damages
// arising from the use of this software.
// Permission is granted to anyone to use this software for any purpose,
// including commercial applications, and to alter it and redistribute it
// freely, subject to the following restrictions:
// 1. The origin of this software must not be misrepresented; you must not
// claim that you wrote the original software. If you use this software
// in a product, an acknowledgment in the product documentation would be
// appreciated but is not required.
// 2. Altered source versions must be plainly marked as such, and must not be
// misrepresented as being the original software.
// 3. This notice may not be removed or altered from any source distribution.

#include "b2DynamicTree.h"
#include <string.h>

b2DynamicTree::b2DynamicTree()
{
	m_root = b2_nullNode;
			
	m_nodes.resize(16);

	// Build a linked list for the free list.
	for (int32_t i = 0; i < 15; ++i)
	{
		m_nodes[i].next = i + 1;
		m_nodes[i].height = -1;
	}
	m_nodes[15].next = b2_nullNode;
	m_nodes[15].height = -1;
	m_freeList = 0;

	m_path = 0;

	m_insertionCount = 0;
}

// Allocate a node from the pool. Grow the pool if necessary.
int32_t b2DynamicTree::AllocateNode()
{
	// Expand the node pool as needed.
	if (m_freeList == b2_nullNode)
	{
		m_freeList = m_nodes.size();
        
		auto new_capacity = 2 * m_nodes.size();
		m_nodes.reserve(new_capacity);
        
		while(m_nodes.size() < new_capacity) {
			m_nodes.emplace_back();
			m_nodes.back().next = m_nodes.size();
			m_nodes.back().height = -1;
		}
        
		m_nodes.emplace_back();
		m_nodes.back().next = b2_nullNode;
		m_nodes.back().height = -1;
	}

	// Peel a node off the free list.
	int32_t nodeId = m_freeList;
	m_freeList = m_nodes[nodeId].next;
	m_nodes[nodeId].parent = b2_nullNode;
	m_nodes[nodeId].child1 = b2_nullNode;
	m_nodes[nodeId].child2 = b2_nullNode;
	m_nodes[nodeId].height = 0;
	m_nodes[nodeId].userData = -1;
	return nodeId;
}

// Return a node to the pool.
void b2DynamicTree::FreeNode(int32_t nodeId)
{
	m_nodes[nodeId].next = m_freeList;
	m_nodes[nodeId].height = -1;
	m_freeList = nodeId;
}

// Create a proxy in the tree as a leaf node. We return the index
// of the node instead of a pointer so that we can grow
// the node pool.
int32_t b2DynamicTree::CreateProxy(const Bounds<float>& aabb, int32_t userData)
{
	int32_t proxyId = AllocateNode();

	// Fatten the aabb.
	m_nodes[proxyId].aabb = Bounds<float>(
            range(aabb.x.min - b2_aabbExtension, aabb.x.max + b2_aabbExtension),
			range(aabb.y.min - b2_aabbExtension, aabb.y.max + b2_aabbExtension)
	);
	m_nodes[proxyId].userData = userData;
	m_nodes[proxyId].height = 0;

	InsertLeaf(proxyId);

	return proxyId;
}

void b2DynamicTree::DestroyProxy(int32_t proxyId)
{
	RemoveLeaf(proxyId);
	FreeNode(proxyId);
}

bool b2DynamicTree::MoveProxy(int32_t proxyId, const Bounds<float>& aabb, const Vector2f& vel)
{
	if (m_nodes[proxyId].aabb.contains(aabb))
	{
		return false;
	}

	RemoveLeaf(proxyId);

	// Extend AABB.
	auto b =  Bounds<float>(
			range(aabb.x.min - b2_aabbExtension, aabb.x.max + b2_aabbExtension),
			range(aabb.y.min - b2_aabbExtension, aabb.y.max + b2_aabbExtension)
	);
	if (vel.x < 0) b.x.min += 2 * vel.x;
	if (vel.x > 0) b.x.max += 2 * vel.x;
	if (vel.y < 0) b.y.min += 2 * vel.y;
	if (vel.y > 0) b.y.max += 2 * vel.y;

	m_nodes[proxyId].aabb = b;

	InsertLeaf(proxyId);
	return true;
}

void b2DynamicTree::InsertLeaf(int32_t leaf)
{
	++m_insertionCount;

	if (m_root == b2_nullNode)
	{
		m_root = leaf;
		m_nodes[m_root].parent = b2_nullNode;
		return;
	}

	// Find the best sibling for this node
	Bounds<float> leafAABB = m_nodes[leaf].aabb;
	int32_t index = m_root;
	while (!m_nodes[index].IsLeaf())
	{
		int32_t child1 = m_nodes[index].child1;
		int32_t child2 = m_nodes[index].child2;

		float area = m_nodes[index].aabb.perimeter();

		Bounds<float> combinedAABB = m_nodes[index].aabb.combine(leafAABB);
		float combinedArea = combinedAABB.perimeter();

		// Cost of creating a new parent for this node and the new leaf
		float cost = 2.0f * combinedArea;

		// Minimum cost of pushing the leaf further down the tree
		float inheritanceCost = 2.0f * (combinedArea - area);

		// Cost of descending into child1
		float cost1;
		if (m_nodes[child1].IsLeaf())
		{
			Bounds<float> aabb = leafAABB.combine(m_nodes[child1].aabb);
			cost1 = aabb.perimeter() + inheritanceCost;
		}
		else
		{
			Bounds<float> aabb = leafAABB.combine(m_nodes[child1].aabb);
			float oldArea = m_nodes[child1].aabb.perimeter();
			float newArea = aabb.perimeter();
			cost1 = (newArea - oldArea) + inheritanceCost;
		}

		// Cost of descending into child2
		float cost2;
		if (m_nodes[child2].IsLeaf())
		{
			Bounds<float> aabb = leafAABB.combine(m_nodes[child2].aabb);
			cost2 = aabb.perimeter() + inheritanceCost;
		}
		else
		{
			Bounds<float> aabb = leafAABB.combine(m_nodes[child2].aabb);
			float oldArea = m_nodes[child2].aabb.perimeter();
			float newArea = aabb.perimeter();
			cost2 = newArea - oldArea + inheritanceCost;
		}

		// Descend according to the minimum cost.
		if (cost < cost1 && cost < cost2)
		{
			break;
		}

		// Descend
		if (cost1 < cost2)
		{
			index = child1;
		}
		else
		{
			index = child2;
		}
	}

	int32_t sibling = index;

	// Create a new parent.
	int32_t oldParent = m_nodes[sibling].parent;
	int32_t newParent = AllocateNode();
	m_nodes[newParent].parent = oldParent;
	m_nodes[newParent].userData = -1;
	m_nodes[newParent].aabb = leafAABB.combine(m_nodes[sibling].aabb);
	m_nodes[newParent].height = m_nodes[sibling].height + 1;

	if (oldParent != b2_nullNode)
	{
		// The sibling was not the root.
		if (m_nodes[oldParent].child1 == sibling)
		{
			m_nodes[oldParent].child1 = newParent;
		}
		else
		{
			m_nodes[oldParent].child2 = newParent;
		}

		m_nodes[newParent].child1 = sibling;
		m_nodes[newParent].child2 = leaf;
		m_nodes[sibling].parent = newParent;
		m_nodes[leaf].parent = newParent;
	}
	else
	{
		// The sibling was the root.
		m_nodes[newParent].child1 = sibling;
		m_nodes[newParent].child2 = leaf;
		m_nodes[sibling].parent = newParent;
		m_nodes[leaf].parent = newParent;
		m_root = newParent;
	}

	// Walk back up the tree fixing heights and AABBs
	index = m_nodes[leaf].parent;
	while (index != b2_nullNode)
	{
		index = Balance(index);

		int32_t child1 = m_nodes[index].child1;
		int32_t child2 = m_nodes[index].child2;

		m_nodes[index].height = 1 + std::max(m_nodes[child1].height, m_nodes[child2].height);
		m_nodes[index].aabb = m_nodes[child1].aabb.combine(m_nodes[child2].aabb);

		index = m_nodes[index].parent;
	}

	//Validate();
}

void b2DynamicTree::RemoveLeaf(int32_t leaf)
{
	if (leaf == m_root)
	{
		m_root = b2_nullNode;
		return;
	}

	int32_t parent = m_nodes[leaf].parent;
	int32_t grandParent = m_nodes[parent].parent;
	int32_t sibling;
	if (m_nodes[parent].child1 == leaf)
	{
		sibling = m_nodes[parent].child2;
	}
	else
	{
		sibling = m_nodes[parent].child1;
	}

	if (grandParent != b2_nullNode)
	{
		// Destroy parent and connect sibling to grandParent.
		if (m_nodes[grandParent].child1 == parent)
		{
			m_nodes[grandParent].child1 = sibling;
		}
		else
		{
			m_nodes[grandParent].child2 = sibling;
		}
		m_nodes[sibling].parent = grandParent;
		FreeNode(parent);

		// Adjust ancestor bounds.
		int32_t index = grandParent;
		while (index != b2_nullNode)
		{
			index = Balance(index);

			int32_t child1 = m_nodes[index].child1;
			int32_t child2 = m_nodes[index].child2;

			m_nodes[index].aabb = m_nodes[child1].aabb.combine(m_nodes[child2].aabb);
			m_nodes[index].height = 1 + std::max(m_nodes[child1].height, m_nodes[child2].height);

			index = m_nodes[index].parent;
		}
	}
	else
	{
		m_root = sibling;
		m_nodes[sibling].parent = b2_nullNode;
		FreeNode(parent);
	}

	//Validate();
}

// Perform a left or right rotation if node A is imbalanced.
// Returns the new root index.
int32_t b2DynamicTree::Balance(int32_t iA)
{
	b2TreeNode& A = m_nodes[iA];
	if (A.IsLeaf() || A.height < 2)
	{
		return iA;
	}

	int32_t iB = A.child1;
	int32_t iC = A.child2;

	b2TreeNode& B = m_nodes[iB];
	b2TreeNode& C = m_nodes[iC];

	int32_t balance = C.height - B.height;

	// Rotate C up
	if (balance > 1)
	{
		int32_t iF = C.child1;
		int32_t iG = C.child2;
		b2TreeNode& F = m_nodes[iF];
		b2TreeNode& G = m_nodes[iG];

		// Swap A and C
		C.child1 = iA;
		C.parent = A.parent;
		A.parent = iC;

		// A's old parent should point to C
		if (C.parent != b2_nullNode)
		{
			if (m_nodes[C.parent].child1 == iA)
			{
				m_nodes[C.parent].child1 = iC;
			}
			else
			{
				m_nodes[C.parent].child2 = iC;
			}
		}
		else
		{
			m_root = iC;
		}

		// Rotate
		if (F.height > G.height)
		{
			C.child2 = iF;
			A.child2 = iG;
			G.parent = iA;
			A.aabb = B.aabb.combine(G.aabb);
			C.aabb = A.aabb.combine(F.aabb);

			A.height = 1 + std::max(B.height, G.height);
			C.height = 1 + std::max(A.height, F.height);
		}
		else
		{
			C.child2 = iG;
			A.child2 = iF;
			F.parent = iA;
			A.aabb = B.aabb.combine(F.aabb);
			C.aabb = A.aabb.combine(G.aabb);

			A.height = 1 + std::max(B.height, F.height);
			C.height = 1 + std::max(A.height, G.height);
		}

		return iC;
	}
	
	// Rotate B up
	if (balance < -1)
	{
		int32_t iD = B.child1;
		int32_t iE = B.child2;
		b2TreeNode& D = m_nodes[iD];
		b2TreeNode& E = m_nodes[iE];

		// Swap A and B
		B.child1 = iA;
		B.parent = A.parent;
		A.parent = iB;

		// A's old parent should point to B
		if (B.parent != b2_nullNode)
		{
			if (m_nodes[B.parent].child1 == iA)
			{
				m_nodes[B.parent].child1 = iB;
			}
			else
			{
				m_nodes[B.parent].child2 = iB;
			}
		}
		else
		{
			m_root = iB;
		}

		// Rotate
		if (D.height > E.height)
		{
			B.child2 = iD;
			A.child1 = iE;
			E.parent = iA;
			A.aabb = C.aabb.combine(E.aabb);
			B.aabb = A.aabb.combine(D.aabb);

			A.height = 1 + std::max(C.height, E.height);
			B.height = 1 + std::max(A.height, D.height);
		}
		else
		{
			B.child2 = iE;
			A.child1 = iD;
			D.parent = iA;
			A.aabb = C.aabb.combine(D.aabb);
			B.aabb = A.aabb.combine(E.aabb);

			A.height = 1 + std::max(C.height, D.height);
			B.height = 1 + std::max(A.height, E.height);
		}

		return iB;
	}

	return iA;
}

int32_t b2DynamicTree::GetHeight() const
{
	if (m_root == b2_nullNode)
	{
		return 0;
	}

	return m_nodes[m_root].height;
}

//
float b2DynamicTree::GetAreaRatio() const
{
	if (m_root == b2_nullNode)
	{
		return 0.0f;
	}

	const b2TreeNode& root = m_nodes[m_root];
	float rootArea = root.aabb.perimeter();

	float totalArea = 0.0f;
	for (uint32_t i = 0; i < m_nodes.size(); ++i)
	{
		const b2TreeNode& node = m_nodes[i];
		if (node.height < 0)
		{
			// Free node in pool
			continue;
		}

		totalArea += node.aabb.perimeter();
	}

	return totalArea / rootArea;
}

// Compute the height of a sub-tree.
int32_t b2DynamicTree::ComputeHeight(int32_t nodeId) const
{
	b2TreeNode const& node = m_nodes[nodeId];

	if (node.IsLeaf())
	{
		return 0;
	}

	int32_t height1 = ComputeHeight(node.child1);
	int32_t height2 = ComputeHeight(node.child2);
	return 1 + std::max(height1, height2);
}

int32_t b2DynamicTree::ComputeHeight() const
{
	int32_t height = ComputeHeight(m_root);
	return height;
}

/*
void b2DynamicTree::ValidateStructure(int32_t index) const
{
	if (index == b2_nullNode)
	{
		return;
	}

	const b2TreeNode& node = m_nodes[index];

	int32_t child1 = node.child1;
	int32_t child2 = node.child2;

	if (node.IsLeaf())
	{
		b2Assert(child1 == b2_nullNode);
		b2Assert(child2 == b2_nullNode);
		b2Assert(node.height == 0);
		return;
	}

	b2Assert(0 <= child1 && child1 < m_nodeCapacity);
	b2Assert(0 <= child2 && child2 < m_nodeCapacity);

	b2Assert(m_nodes[child1].parent == index);
	b2Assert(m_nodes[child2].parent == index);

	ValidateStructure(child1);
	ValidateStructure(child2);
}

void b2DynamicTree::ValidateMetrics(int32_t index) const
{
	if (index == b2_nullNode)
	{
		return;
	}

	const b2TreeNode* node = m_nodes + index;

	int32_t child1 = node->child1;
	int32_t child2 = node->child2;

	if (node->IsLeaf())
	{
		b2Assert(child1 == b2_nullNode);
		b2Assert(child2 == b2_nullNode);
		b2Assert(node->height == 0);
		return;
	}

	b2Assert(0 <= child1 && child1 < m_nodeCapacity);
	b2Assert(0 <= child2 && child2 < m_nodeCapacity);

	int32_t height1 = m_nodes[child1].height;
	int32_t height2 = m_nodes[child2].height;
	int32_t height;
	height = 1 + std::max(height1, height2);
	b2Assert(node->height == height);

	Bounds<float> aabb;
	aabb.combine(m_nodes[child1].aabb, m_nodes[child2].aabb);

	b2Assert(aabb.lowerBound == node->aabb.lowerBound);
	b2Assert(aabb.upperBound == node->aabb.upperBound);

	ValidateMetrics(child1);
	ValidateMetrics(child2);
}

void b2DynamicTree::Validate() const
{
#if defined(b2DEBUG)
	ValidateStructure(m_root);
	ValidateMetrics(m_root);

	int32_t freeCount = 0;
	int32_t freeIndex = m_freeList;
	while (freeIndex != b2_nullNode)
	{
		b2Assert(0 <= freeIndex && freeIndex < m_nodeCapacity);
		freeIndex = m_nodes[freeIndex].next;
		++freeCount;
	}

	b2Assert(GetHeight() == ComputeHeight());

	b2Assert(m_nodeCount + freeCount == m_nodeCapacity);
#endif
}
 */

int32_t b2DynamicTree::GetMaxBalance() const
{
	int32_t maxBalance = 0;
	for (uint32_t i = 0; i < m_nodes.size(); ++i)
	{
		const b2TreeNode& node = m_nodes[i];
		if (node.height <= 1)
		{
			continue;
		}

		int32_t child1 = node.child1;
		int32_t child2 = node.child2;
		int32_t balance = std::abs(m_nodes[child2].height - m_nodes[child1].height);
		maxBalance = std::max(maxBalance, balance);
	}

	return maxBalance;
}

/*
void b2DynamicTree::RebuildBottomUp()
{
	int32_t* _nodes = (int32_t*)b2Alloc(m_nodeCount * sizeof(int32_t));
	int32_t count = 0;

	// Build array of leaves. Free the rest.
	for (int32_t i = 0; i < m_nodeCapacity; ++i)
	{
		if (m_nodes[i].height < 0)
		{
			// free node in pool
			continue;
		}

		if (m_nodes[i].IsLeaf())
		{
			m_nodes[i].parent = b2_nullNode;
			_nodes[count] = i;
			++count;
		}
		else
		{
			FreeNode(i);
		}
	}

	while (count > 1)
	{
		float minCost = b2_maxFloat;
		int32_t iMin = -1, jMin = -1;
		for (int32_t i = 0; i < count; ++i)
		{
			Bounds<float> aabbi = m_nodes[_nodes[i]].aabb;

			for (int32_t j = i + 1; j < count; ++j)
			{
				Bounds<float> aabbj = m_nodes[_nodes[j]].aabb;
				Bounds<float> b;
				b.combine(aabbi, aabbj);
				float cost = b.perimeter();
				if (cost < minCost)
				{
					iMin = i;
					jMin = j;
					minCost = cost;
				}
			}
		}

		int32_t index1 = _nodes[iMin];
		int32_t index2 = _nodes[jMin];
		b2TreeNode* child1 = m_nodes + index1;
		b2TreeNode* child2 = m_nodes + index2;

		int32_t parentIndex = AllocateNode();
		b2TreeNode* parent = m_nodes + parentIndex;
		parent->child1 = index1;
		parent->child2 = index2;
		parent->height = 1 + std::max(child1->height, child2->height);
		parent->aabb.combine(child1->aabb, child2->aabb);
		parent->parent = b2_nullNode;

		child1->parent = parentIndex;
		child2->parent = parentIndex;

		_nodes[jMin] = _nodes[count-1];
		_nodes[iMin] = parentIndex;
		--count;
	}

	m_root = _nodes[0];
	b2Free(_nodes);
}
 */

void b2DynamicTree::ShiftOrigin(const Vector2f& newOrigin)
{
	// Build array of leaves. Free the rest.
	for (uint32_t i = 0; i < m_nodes.size(); ++i)
	{
		m_nodes[i].aabb.x.min -= newOrigin.x;
		m_nodes[i].aabb.x.max -= newOrigin.x;
		m_nodes[i].aabb.y.min -= newOrigin.y;
		m_nodes[i].aabb.y.max -= newOrigin.y;
	}
}
