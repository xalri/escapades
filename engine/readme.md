A 2D C++ game engine.

## Modules

 - **storage**: A _component graph system_ based on [froggy](https://github.com/kvark/froggy).
 - **math**: Functions, ranges & bounds, and geometric types & algorithms.
 - **physics**: Collision detection & impulse based response.
 - **field**: GPU flowfield pathfinding
 - **stage**: Reads tilesets, tile data, sprites, and collision geometry from TMX map files.
 - **render**: Draws things to the screen.
 - **resources**: Resource loading & management.
 - **window**: Wraps GLFW to add an event queue and key state tracking.
 - **util**: Various useful things which don't belong anywhere else, timing, parallel iteration,
             basic data structures, etc.
 - **audio**: Loading and playing sounds.


## Third party libraries

 - glad: An OpenGL extension loader.
 - glfw: GL Context creation & input.
 - oglwrap: an OpenGL wrapper: checks for errors, has objects, etc.
 - lodepng: PNG image loading.
 - tmxlite: Parsing TMX map files.
 - wave: A nice audio library based on openAL
 - iter: A template for adding stl iterators to custom containers
 - backward: catches signals & uncaught exceptions and pretty prints a stack trace,
      gives us somewhere to start when things fail outside of a debugger.
 - catch: A small unit testing framework.
