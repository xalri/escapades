#pragma once

#include <chrono>
#include <functional>
#include <utility>
#include <map>
#include <memory>
#include <mutex>
#include <iostream>
#include <string>
#include "buffer.hpp"
#include "stats.hpp"

/// Get the current time in nanoseconds, relative to some epoch consistent within the program
inline uint64_t now() {
    auto time = std::chrono::high_resolution_clock::now();
    auto ns = std::chrono::duration_cast<std::chrono::nanoseconds>(time.time_since_epoch());
    return static_cast<uint64_t>(ns.count());
}

constexpr double ns_to_ms(uint64_t ns) { return (double) ns / 1000000.0; }
constexpr uint64_t ms_to_ns(double ms) { return (uint64_t)(ms * 1000000.0); }
constexpr uint64_t seconds_to_ns(double ms) { return (uint64_t)(ms * 1000000000.0); }

/// An interval timer.
class StepTimer {
public:
    uint64_t step;
    uint64_t time = now();
    uint64_t rollover = 0ull;

    /// Create a timer with the given interval in nanoseconds
    explicit StepTimer(uint64_t step = 1000000000ull / 60ull): step(step) {}

    /// Run fn for each tick since the last call.
    void tick(std::function<void()> fn) {
        auto diff = now() - time + rollover;
        time = now();

        while (diff >= step) {
            diff -= step;
            fn();
        }
        rollover = diff;
    }
};

constexpr unsigned long BUFFER_SIZE = 1024;

/// Tracks the time taken to run multiple instances of a task
class Profile {
    std::shared_ptr<Buffer<double, BUFFER_SIZE>> buffer = std::make_shared<Buffer<double, BUFFER_SIZE>>();
    std::string name = "";
public:

    struct Block {
        std::shared_ptr<Buffer<double, BUFFER_SIZE>> buffer;
        uint64_t start;
        bool done = false;

        Block(const Block&) = delete;
        Block& operator=(const Block&) = delete;
        Block& operator=(Block&& other) {
            end();
            other.done = true;
            start = other.start;
            buffer = other.buffer;
            done = false;
            return *this;
        }
        Block(Block&&) noexcept = default;

        double elapsed() const { return ns_to_ms(now() - start); }

        void end() { if (!done) { done = true; buffer->add(elapsed()); } }

        ~Block() {
			if (buffer != nullptr) end();
		}
    };

    explicit Profile(std::string const& name): name(std::move(name)) {}

    /// Start a new block. Drop the returned value at the end of the task to
    /// finish timing.
    Block start_block() {
        return Block{buffer, now()};
    }

    /// Get the min, max, and average duration in milliseconds.
    Stats<double> stats() {
        auto stats = Stats<double>(buffer->begin(), buffer->end());
        return stats;
    }

    friend std::ostream &operator<<(std::ostream &os, Profile &profile) {
        os << profile.stats() << " (ms) | " << profile.name;
        return os;
    }
};

/// Track the time taken by multiple tasks identified by name.
struct SplitProfile {
    std::map<std::string, Profile> profiles{};

    Profile& profile(std::string const& name) {
        auto it = profiles.find(name);
        if (it == profiles.end()) it = profiles.emplace(name, Profile(name)).first;
        return it->second;
    }

    Profile::Block start_block(std::string const& name) {
        return profile(name).start_block();
    }

    friend std::ostream &operator<<(std::ostream &os, SplitProfile &split) {
        for(auto p : split.profiles) os << p.second << std::endl;
        return os;
    }
};
