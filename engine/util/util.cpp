#include "util.hpp"

std::mt19937& rng() {
    static thread_local std::mt19937* rng = nullptr;
    if (!rng) rng = new std::mt19937(now() + std::hash<std::thread::id>()(std::this_thread::get_id()));
    return *rng;
}
