#pragma once

#include "ctpl_stl.h"
#include "time.hpp"
#include <thread>
#include <chrono>
#include <functional>
#include <condition_variable>

#include <iostream>
#include <sstream>

namespace par {
    const int num_workers = 0;

    struct WorkerPool {
        std::vector<std::thread> threads{};
        std::atomic_int started = 0;
        std::atomic_int complete = 0;
        int n_shared = 0;
        int n_total = 0;
        std::atomic_bool stop = false;
        std::function<void(int)> task;
        std::condition_variable wake_cv;
        std::mutex wake_m;
        bool wake = false;
        std::condition_variable done_cv;
        std::mutex done_m;

        /// Run n iterations of a function on the pool. Calls fn with an integer from 0 to n-1.
        template<class Fn> void split(int num, Fn fn) {
			init();
            task = fn;
            started = 0;
            complete = 0;
            n_total = num;
            n_shared = n_total - 20;

            if (n_shared > 0) {
                wake_m.lock();
                wake = true;
                wake_cv.notify_all();
                wake_m.unlock();
            }

            int j;
            while((j = started++) < n_total) {
                task(j);
                complete++;
            }

            auto done_ul = std::unique_lock<std::mutex>(done_m);
            if (complete != n_total) done_cv.wait(done_ul, [this]() { return complete == n_total; });

            if (n_shared > 0) {
                wake_m.lock();
                wake = false;
                wake_m.unlock();
            }
        }

        void worker_thread() {
            while (true) {
                auto ul = std::unique_lock<std::mutex>(wake_m);
                wake_cv.wait(ul, [this]() { return (wake && started < n_shared) || stop; });
                if (stop) return;
                ul.unlock();

                int j;
                while((j = started++) < n_total) {
                    task(j);

                    auto now = complete++;
                    if (now + 1 == n_total) {
                        auto done_ul = std::unique_lock<std::mutex>(done_m);
                        done_cv.notify_all();
                    }
                    if (started >= n_shared) break;
                }
            }
        }

        WorkerPool() = default;

		void init() {
			if ((int) threads.size() == num_workers) return;
            for (auto i = 0; i < num_workers; i++) {
				std::cout << "Creating worker thread " << i << std::endl;
                threads.emplace_back([this]() { worker_thread(); });
            }
		}

        ~WorkerPool() {
            stop = true;
            wake_cv.notify_all();
            for (auto& t : threads) t.join();
        }
    };

    extern thread_local WorkerPool workers;

    /// Iterate over a vector in parallel.
    template<class Fn, class T> void for_each(std::vector<T> &vec, Fn fn) {
        workers.split((int) vec.size(), [&](int i) { fn(vec[i]); });
    }
}
