#pragma once
#include <string>
#include <algorithm>

#ifdef _WIN32
#include <direct.h>
    #define getcwd _getcwd
#else
#include <unistd.h>
#endif

std::string cwd() {
    char buffer[2048];
    char *result = getcwd(buffer, sizeof(buffer));
    auto str = std::string();
    if (result) str = result;
	std::replace(str.begin(), str.end(), '\\', '/');
    return str;
}
