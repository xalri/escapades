#pragma once

#include <ostream>
#include <limits>

/// Statistical measures for some data.
template<class T> struct Stats {
    bool empty = true;
    T min = std::numeric_limits<T>::max();
    T max = -std::numeric_limits<T>::max();
    T avr = 0;

    template <class iter> Stats(iter it, iter end) {
        auto n = 0;
        while (it != end) {
            n += 1;
            empty = false;
            auto value = *(it++);
            avr += value;
            if (value < min) min = value;
            if (value > max) max = value;
        }
        avr /= (T)n;
    }

    friend std::ostream &operator<<(std::ostream &os, const Stats &stats) {
        if (stats.empty) {
            os << "-";
        } else {
            os.precision(6);
            os << std::fixed;
            os << "min: " << stats.min << " max: " << stats.max << " avr: " << stats.avr;
        }
        return os;
    }
};