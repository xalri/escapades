#pragma once

#include <stdexcept>
#include <algorithm>
#include <random>
#include <thread>
#include <random>
#include "time.hpp"

// Thread local rng
extern std::mt19937& rng();

class unimplemented: public std::logic_error {
public:
    unimplemented(): logic_error("Not yet implemented") {}
};

template<class T> void drop(T) {}

/// Filter elements from a vector according to some predicate. Doesn't preserve order.
// TODO: there's probably something in std::algorithm for this.
template<class T, class P> void swap_filter(std::vector<T>& vec, P predicate) {
	/*
	auto len = vec.size();
    for (auto it = vec.end(); it != vec.begin();) {
        --it;
        if(!predicate(*it)) {
            if (it == vec.end() - 1) {
				len--;
            } else {
                *it = std::move(vec[len - 1]);
                len--;
            }
        }
    }
	vec.resize(len);
	*/

	vec.erase(std::remove_if(vec.begin(), vec.end(), [&](auto x) { return !predicate(x); }), vec.end());
}