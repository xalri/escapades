#pragma once

#include <vector>
#include <cstdint>

/// A growable stack
template<class T, int32_t N> class GrowableStack {
private:
    int32_t stack_size = 0;
    std::vector<T> vec;
    T stack[N];
public:
    /// Add an item to the stack
    void push(T item) {
        if (stack_size < N) stack[stack_size] = std::move(item);
        else vec.push_back(std::move(item));
        stack_size++;
    }

    /// Remove the item on the top of the stack
    T pop() {
        stack_size--;
        if (stack_size < N) return stack[stack_size];
        else {
            auto ret = vec.back();
            vec.pop_back();
            return ret;
        }
    }

    /// Get the size of the stack
    int32_t size() const {
        return stack_size;
    }
};