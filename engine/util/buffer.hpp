#pragma once

#include <cstdint>
#include <optional>
#include <vector>
#include <array>
#include <algorithm>
#include <atomic>
#include <iterator_tpl.h>

/// A fixed size circular buffer
template<class T, unsigned N> class Buffer {
    //std::atomic_uint64_t idx = 0;
	std::atomic_uint32_t idx = 0;
    std::array<T, N> data{};

public:

    /// Returns true if no items have been added to the buffer.
    bool empty() const { return data.empty(); }

    /// Returns the number of items in the buffer
    uint64_t size() const { return data.size(); }

    /// Add a value to the end of the buffer
    void add(T value) {
        auto idx = this->idx++;
        data[idx % N] = std::move(value);
    }

    /// Get a value from the buffer. Returns `nullopt` if the value is out of range.
    std::optional<T&> get(uint64_t index) const {
        auto idx = this->idx.load();
        auto overflow = (idx > N) ? idx - N : 0;
        if (index > overflow + data.size() - 1) return {};
        if (index < overflow) return {};
        return { data[index % N] };
    }

    struct it_state {
        unsigned long pos;
        inline void next(const Buffer* ref) { pos++; }
        inline void begin(const Buffer* ref) { pos = 0ul; }
        inline void end(const Buffer* ref) { pos = std::min<unsigned long>(ref->idx.load(), ref->data.size()); }
        inline bool cmp(const it_state& s) const { return pos != s.pos; }
        inline T const& get(const Buffer* ref) const { return ref->data[pos]; }
    };
    SETUP_CONST_ITERATOR(Buffer, T, it_state);

    /*
   /// Get an iterator over the buffer's elements, from oldest to newest.
   pub fn iter<'a>(&'a self) -> impl DoubleEndedIterator<Item=&'a T> {
      let old = if self.data.len() < self.capacity as usize {
         [].iter()
      } else {
         self.data[self.pos as usize..self.capacity as usize].iter()
      };
      let new = self.data[0..self.pos as usize].iter();
      old.chain(new)
   }
     */
};