#pragma once

#include <memory>
#include <functional>
#include <storage/storage.hpp>

/// A source of resources, e.g. a directory, archive, web server, ...
class Source {
    // TODO: ...
};

class DirSource : Source {
    std::string root;
    // TODO: ...
};

class Resources;

/// Load a resource.
/// To add a new resource type implement this function with concrete types, see
/// render/texture.hpp for an example.
template<class T, class... Args> T load_resource(Resources& res, std::string const& id, Args... args);

/// load_resources functions often only differ in return type, use overload(TypeParam, Return) as
/// the return type make this work ._.
#define if_t(t, r) template<class T> inline typename std::enable_if<std::is_same<T, t>::value, r>::type
#define overload(t) if_t(t, t)

/// A store of resources.
// TODO: generic over source? then some resources could be specific to source types
class Resources {
    std::map<std::string, RefAny> map{};

public:
    Storage& storage;
    Source source{};

    explicit Resources(Storage& s, Source source = Source()): storage(s), source(source) {}

    // Movable & non-copyable.
    Resources& operator=(Resources&&) noexcept = default;
    Resources(Resources&&) = default;
    Resources& operator=(const Resources&) = delete;
    Resources(const Resources& other) = delete;

    /// Get a resource. If the resource already exists returns a reference to it, otherwise
    /// loads it with the given arguments.
    template<class T, class... Args> Ref<T> get(std::string const& id, Args... args) {
        auto it = map.find(id);
        if (it != map.end()) {
            if (!it->second.is<T>())
                throw std::runtime_error("Attempt to access existing resource with different type");
        }
        if (it == map.end()) {
            std::cout << "Loading " << id << std::endl;
            auto ref = storage.create<T>(load_resource<T>(*this, id, args...));
            map.emplace(id, ref.any());
            return ref;
        } else {
            if (!it->second.is<T>())
                throw std::runtime_error("Attempt to access existing resource with different type");
            return *it->second.as<T>();
        }
    }

    /// Remove any unused resources.
    void clean() {
        for(auto it = map.begin(); it != map.end();) {
            if (it->second.ref_count() == 1) map.erase(it++);
            else ++it;
        }
    }

    /// Load a resource with an arbitrary function, for one-off resource types.
    /// Returns an existing instance if the id has been used before.
    /// res.get_with<SomeType>("my_resource", [](){ /* ... */ });
    template<class T, class Fn> Ref<T> get_with(std::string const& id, Fn fn) {
        auto it = map.find(id);
        if (it != map.end()) {
            if (!it->second.is<T>())
                throw std::runtime_error("Attempt to access existing resource with different type");
        }
        if (it == map.end()) {
            std::cout << "Loading " << id << std::endl;
            auto ref = storage.create<T>(fn());
            map.emplace(id, ref.any());
            return ref;
        } else {
            if (!it->second.is<T>())
                throw std::runtime_error("Attempt to access existing resource with different type");
            return *it->second.as<T>();
        }
    }
};
