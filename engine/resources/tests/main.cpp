#define CATCH_CONFIG_MAIN
#define BACKWARD_HAS_BFD 1

#include <catch.hpp>
#include <backward.hpp>
#include <resources/resources.hpp>

backward::SignalHandling sh;


overload(int) load_resource(Resources& res, std::string const& name, int value) {
    return value * 2;
}

TEST_CASE("resources") {
	Storage storage{};
	Resources resources{storage};
    auto ref = resources.get<int>("my_number", 21);
    REQUIRE(*ref == 42);
    REQUIRE(*resources.get<int>("my_number", 30) == 42);
}
