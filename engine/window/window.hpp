#pragma once

#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include <deque>
#include <array>
#include <optional>
#include <math/math.hpp>

enum class WindowEvKind {
    KeyPress,
    KeyRelease,
    MouseButtonPress,
    MouseButtonRelease,
    MouseMove,
    MouseWheelScroll,
    Resize,
    FramebufferResize,
    Closed,
    FocusGained,
    FocusLost,
    Moved
};

struct WindowEv {
    WindowEvKind kind;

    // Input
    struct Key{ int code, scancode, mods; };
    struct Mouse{ int button, mods; double x, y, x_diff, y_diff; };
    struct MouseWheel{ double x_off, y_off; };

    // Window
    struct WindowSize{ int width; int height; };
    struct FbSize{ int width; int height; };
    struct WindowPosition{ int x; int y; };

    union {
        Key key;
        Mouse mouse;
        MouseWheel mouse_wheel;
        WindowSize window_size;
        FbSize fb_size;
        WindowPosition window_position;
    };

    explicit WindowEv(WindowEvKind kind): kind(kind) {}
    WindowEv() {}
};

/// A wrapper around a GLFW window.
class Window {
public:
    explicit Window(const char *name = "window", int width = 1280, int height = 720);
    ~Window();

    Window(Window const&) = delete;
    Window& operator=(Window const&) = delete;
    Window& operator=(Window const&&) = delete;
    Window(Window&&);

    GLFWwindow* window = nullptr;
    std::deque<WindowEv> events{};
    std::array<bool, GLFW_MOUSE_BUTTON_LAST> mouse_pressed;
    std::array<bool, GLFW_KEY_LAST> key_pressed;
    Vector2d mouse;
    Vector2d mouse_view;
    Vector2i size;
    Vector2i view_size;
    Vector2i view_offset;
    double target_ratio;

    /// Poll for events
    void poll_events();

    /// Get the next event
    std::optional<WindowEv> next_event();

    /// Bind the window's framebuffer & set the viewport
    void bind() const;

    /// Swap the framebuffers
    void display() const;

    /// Check if the window is open
    bool should_close() const;

    /// Close the window
    void close();

    /// Set the number of buffer swaps to wait on when calling display.
    void set_vsync(int n);
};
