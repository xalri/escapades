#include "window.hpp"
#include <iostream>

void error_callback(int error, const char* description) {
    std::cout << description << std::endl;
}

void mouse_button_callback(GLFWwindow* w, int button, int action, int mods) {
    auto window = static_cast<Window*>(glfwGetWindowUserPointer(w));
    if(action == GLFW_PRESS) {
        window->mouse_pressed[button] = true;
        window->events.emplace_back(WindowEvKind::MouseButtonPress);
    } else {
        window->mouse_pressed[button] = false;
        window->events.emplace_back(WindowEvKind::MouseButtonRelease);
    }
    window->events.back().mouse.button = button;
    window->events.back().mouse.mods = mods;
    window->events.back().mouse.x = window->mouse.x;
    window->events.back().mouse.y = window->mouse.y;
}

void cursor_pos_callback(GLFWwindow* w, double x, double y) {
    auto window = static_cast<Window*>(glfwGetWindowUserPointer(w));
    auto x_diff = 0.0, y_diff = 0.0;
    if (window->mouse.x != -1) {
        x_diff = x - window->mouse.x;
        y_diff = y - window->mouse.y;
    }
    window->mouse.x = (float)x;
    window->mouse.y = (float)y;
    window->mouse_view.x = (float)x - window->view_offset.x;
    window->mouse_view.y = (float)y - window->view_offset.y;
    window->events.emplace_back(WindowEvKind::MouseMove);
    window->events.back().mouse.x = x;
    window->events.back().mouse.y = y;
    window->events.back().mouse.x_diff = x_diff;
    window->events.back().mouse.y_diff = y_diff;
}

void scroll_callback(GLFWwindow* w, double x, double y) {
    auto window = static_cast<Window*>(glfwGetWindowUserPointer(w));
    window->events.emplace_back(WindowEvKind::MouseWheelScroll);
    window->events.back().mouse_wheel.x_off = x;
    window->events.back().mouse_wheel.y_off = y;
}

void key_callback(GLFWwindow* w, int key, int scancode, int action, int mods) {
    auto window = static_cast<Window*>(glfwGetWindowUserPointer(w));
    if(action == GLFW_PRESS) {
        window->key_pressed[key] = true;
        window->events.emplace_back(WindowEvKind::KeyPress);
    } else if (action == GLFW_RELEASE) {
        window->key_pressed[key] = false;
        window->events.emplace_back(WindowEvKind::KeyRelease);
    } else {
        return;
    }
    window->events.back().key.code = key;
    window->events.back().key.scancode = scancode;
    window->events.back().key.mods = mods;
}


void window_pos_callback(GLFWwindow* w, int x, int y) {
    auto window = static_cast<Window*>(glfwGetWindowUserPointer(w));
    window->events.emplace_back(WindowEvKind::Moved);
    window->events.back().window_position.x = x;
    window->events.back().window_position.y = y;
}

void window_size_callback(GLFWwindow* _w, int w, int h) {
    auto window = static_cast<Window*>(glfwGetWindowUserPointer(_w));
    window->events.emplace_back(WindowEvKind::Resize);
    window->events.back().window_size.width = w;
    window->events.back().window_size.height = h;
    window->size.x = w;
    window->size.y = h;
}

void fb_size_callback(GLFWwindow* _w, int w, int h) {
    auto window = static_cast<Window*>(glfwGetWindowUserPointer(_w));
    window->events.emplace_back(WindowEvKind::FramebufferResize);
    window->events.back().fb_size.width = w;
    window->events.back().fb_size.height = h;

    auto ratio = (float) w / (float) h;
    auto scale_x = 1.0, scale_y = 1.0;
    auto offset_x = 0.0, offset_y = 0.0;

    if(ratio > window->target_ratio) {
        scale_x = window->target_ratio / ratio;
        offset_x = w * (1 - scale_x) / 2.f;
    } else {
        scale_y = ratio / window->target_ratio;
        offset_y = h * (1 - scale_y) / 2.f;
    }

    window->view_size.x = (int)(w * scale_x);
    window->view_size.y = (int)(h * scale_y);
    window->view_offset.x = (int) offset_x;
    window->view_offset.y = (int) offset_y;
}

void close_callback(GLFWwindow* w) {
    auto window = static_cast<Window*>(glfwGetWindowUserPointer(w));
    window->events.emplace_back(WindowEvKind::Closed);
}

void focus_callback(GLFWwindow* w, int focused) {
    auto window = static_cast<Window*>(glfwGetWindowUserPointer(w));
    if (focused == GL_TRUE) window->events.emplace_back(WindowEvKind::FocusGained);
    else window->events.emplace_back(WindowEvKind::FocusLost);
}


Window::Window(const char *name, int width, int height) {
    glfwSetErrorCallback(error_callback);
    std::cout << "Initialising glfw " << glfwGetVersionString() << std::endl;
    if (!glfwInit()) throw std::runtime_error("Failed to initialise GLFW");

    size.x = width;
    size.y = height;
    view_size.x = width;
    view_size.y = height;
    target_ratio = (double) width / (double) height;

    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    //glfwWindowHint(GLFW_RESIZABLE, false);
    window = glfwCreateWindow(width, height, name, nullptr, nullptr);

    glfwSetKeyCallback(window, key_callback);
    glfwSetScrollCallback(window, scroll_callback);
    glfwSetCursorPosCallback(window, cursor_pos_callback);
    glfwSetMouseButtonCallback(window, mouse_button_callback);

    glfwSetWindowPosCallback(window, window_pos_callback);
    glfwSetWindowSizeCallback(window, window_size_callback);
    glfwSetFramebufferSizeCallback(window, fb_size_callback);
    glfwSetWindowCloseCallback(window, close_callback);
    glfwSetWindowFocusCallback(window, focus_callback);

    if (!window) {
        glfwTerminate();
        throw std::runtime_error("Failed to create window");
    }

    glfwMakeContextCurrent(window);

    if (!gladLoadGL()) {
        throw std::runtime_error("gladLoadGL failed");
    }

    glfwSetWindowUserPointer(window, this);

    for(auto& b : mouse_pressed) b = false;
    for(auto& b : key_pressed) b = false;
}

Window::Window(Window&&) {
    glfwSetWindowUserPointer(window, this);
}

Window::~Window() {
    glfwDestroyWindow(window);
    glfwTerminate();
}

void Window::poll_events() {
    glfwPollEvents();
}

std::optional<WindowEv> Window::next_event() {
    if (events.empty()) {
        return {};
    } else {
        auto ev = events.front();
        events.pop_front();
        return {ev};
    }
}

bool Window::should_close() const {
    return glfwWindowShouldClose(window) == GL_TRUE;
}

void Window::bind() const {
    glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);
    glViewport(view_offset.x, view_offset.y, view_size.x, view_size.y);
}

void Window::close() {
    glfwSetWindowShouldClose(window, GLFW_TRUE);
}

void Window::display() const {
    glfwSwapBuffers(window);
}

void Window::set_vsync(int n) {
    glfwSwapInterval(n);
}
