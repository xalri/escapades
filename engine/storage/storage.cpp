#include "storage.hpp"

std::atomic<uint16_t> box_id_counter{1};
std::atomic<uint32_t> type_id_counter{0};

bool RefData::operator==(const RefData &rhs) const {
    return index == rhs.index &&
           epoch == rhs.epoch &&
           box_id == rhs.box_id;
}

bool RefData::operator!=(const RefData &rhs) const {
    return !(rhs == *this);
}

std::ostream &operator<<(std::ostream &os, const RefData &data) {
    os << "index: " << data.index << " epoch: " << data.epoch << " storage: " << data.box_id;
    return os;
}
