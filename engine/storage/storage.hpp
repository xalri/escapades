// This module implements a Component Graph System (CGS), an
// adaptation of an entity component system where we remove entities
// and instead allow components to reference other components.
// It's intent is to be semantically similar to an OOP style but with
// improved performance. Fixed-size components are stored contiguously
// and memory is reused, to speed up iteration and avoid allocation.
//
// A `Box` holds components of a single type. A `Storage` dynamically
// creates boxes as components of different types are added. Adding
// a component to a box returns a strong reference (Ref<T>) to the new
// component which acts as a smart pointer: the reference can be cloned
// and the component is only deleted when there are no more strong
// references to it.
//
//    auto storage = Storage();
//    auto ref = storage.create<int>(42);
//    std::cout << *ref << std::endl;
//
// Components are accessed using operator* (*ref) or by iterating over the
// box. The par_for_each method runs a given function in parallel for each component of a given type:
//
//    storage.par_for_each<int>([](auto& component) { ... }).
//
// Or you can iterate over components in a box with stl style iterators:
//
//    for (auto& component : storage.box<int>()) { ... }
//
// References can be downgraded to weak references which don't keep
// the component alive, to allow for reference cycles (e.g. components
// could keep a weak pointer to their parent). Attempting to access a
// component which has been deleted results in a runtime exception.
//
//    auto weak = ref.weak();
//    std::cout << storage[weak] << std::endl;
//
// It's good practice to declare which components a function accesses by
// taking a reference to the box, allowing the user of the function to
// reason about concurrent access to components:
//
//    void integrate(Box<Body> const& bodies, Box<Transform> &transforms) {
//      for (auto& body : bodies) {
//        transforms[body.transform].position += body.velocity;
//      }
//    }
//    ...
//    integrate(storage.box<Body>(), storage.box<Transform>());
//
// That said, the current implementation is not thread-safe, in particular
// Ref's destructor could cause concurrent access to it's reference count
// and the component itself, which somewhat decreases the usefulness of
// restricting access to being only via the storage.
//
// The idea of a component graph system comes from `froggy`:
// https://github.com/kvark/froggy/wiki/Component-Graph-System

#pragma once

#include <memory>
#include <map>
#include <any>
#include <utility>
#include <vector>
#include <deque>
#include <atomic>
#include <iostream>
#include <functional>
#include <typeindex>
#include <util/parallel.hpp>
#include <util/util.hpp>
#include <iterator_tpl.h>

extern std::atomic<uint16_t> box_id_counter;
constexpr uint16_t INVALID_BOX = 0;

// type_id<T>() gives a unique value for every type T. The value can change between runs
// of the program.
extern std::atomic_uint32_t type_id_counter;
constexpr uint32_t NULL_TYPE = 0;
template<class T> uint32_t type_id() {
    static uint32_t id = ++type_id_counter;
    return id;
}

/// Write debug output to stdout for tracing the lifetime of components.
constexpr bool TRACE = false;

/// Check that references are valid before dereferencing.
constexpr bool TEST_VALIDITY = true;

/// An exception thrown when attempting to get a component from the wrong storage
class wrong_box: public std::logic_error {
public:
    wrong_box(): logic_error("Box::operator[] on the wrong box!") {}
};

/// An exception thrown when attempting to get a component using a weak pointer which
/// has been deleted.
class dead_component: public std::logic_error {
public:
    dead_component(): logic_error("Box::operator[] on a component which has been deleted") {}
};

template<class T> class Box;
template<class T> class Ref;
template<class T> class Weak;

/// Uniquely identifies a component.
struct RefData {
    /// The index in the storage of the component.
    uint32_t index = 0;
    /// The epoch (number of times the component at this index has been overwritten)
    uint16_t epoch = 0;
    /// The ID of the storage.
    uint16_t box_id = 0;

    bool operator==(const RefData &rhs) const;
    bool operator!=(const RefData &rhs) const;

    friend std::ostream &operator<<(std::ostream &os, const RefData &data);
};

/// Metadata for a storage.
template<class T> struct BoxInner {
    /// Component data.
    std::vector<T> data{};
    /// The current epoch of each component.
    std::vector<uint16_t> epoch{};
    /// The number of strong references to each component.
    std::vector<uint32_t> refs{};
    /// A list of free slots in the storage.
    std::vector<uint32_t> free{};

    /// Get the epoch for a given component index.
    uint16_t get_epoch(uint32_t index) const {
        return (uint32_t) index < epoch.size() ? epoch[index] : (uint16_t) 0;
    }
};

/// A weak pointer to any component. Can only be used by casting to the component type.
class WeakAny {
protected:
    template<class T> friend class Weak;
    std::shared_ptr<void> meta {};
    uint32_t type = NULL_TYPE;
    RefData data {};

    template<class T> explicit WeakAny(std::shared_ptr<BoxInner<T>> meta, uint32_t type, RefData data):
            meta(std::static_pointer_cast<void>(meta)), type(type), data(data) {}

public:
    WeakAny() = default;

    /// Check if the component's type is T.
    template<class T> bool is() const { return type == type_id<T>(); }

    /// Downcast the pointer to a Weak<T>. Throws an exception if the type is incorrect.
    template<class T> std::optional<Weak<T>> as() const;

    /// Check if the pointer is valid (non-empty and not moved from).
    bool valid() const {
        return meta != nullptr && data.box_id != INVALID_BOX;
    }
};

/// A weak reference to a component
template<class T> class Weak {
protected:
    friend struct std::hash<Weak<T>>;
    friend class Ref<T>;
    friend class Box<T>;
    friend class WeakAny;
    template<class C> friend class Weak;

    // A pointer to the reference count & epoch data for the storage this component belongs to.
    std::shared_ptr<BoxInner<T>> meta = nullptr;

    Weak(RefData data, std::shared_ptr<BoxInner<T>> count);

public:
    // Contains the index, storage ID, and epoch to uniquely identify the component.
    RefData data{};

    // The default assignment & copy constructor are fine.
    Weak& operator=(const Weak&) = default;
    Weak& operator=(Weak&&) noexcept = default;
    Weak(Weak&&) noexcept = default;
    Weak(const Weak& other) = default;

    /// Create an empty ref. Use with caution.
    Weak() = default;

    /// Cast to a WeakAny.
    WeakAny any() const {
        return WeakAny(meta, type_id<T>(), data);
    }

    T& operator*() const {
        test_alive();
        return meta->data[data.index];
    }

    T* operator->() { return &**this; }
    T const* operator->() const { return &**this; }

    /// Check if the pointer is valid (non-empty and not moved from).
    bool valid() const {
        return meta != nullptr && data.box_id != INVALID_BOX;
    }

    /// Check if the component pointed to is still alive.
    bool is_alive() const {
        return valid() && meta->get_epoch(data.index) == data.epoch;
    }

    /// Throws an exception if validity checking is on and the pointer is invalid
    void test_alive() const {
        if (TEST_VALIDITY && !valid()) throw std::invalid_argument("Invalid ref");
        if (!is_alive()) throw dead_component();
    }

    /// Attempt to upgrade to a Ref<T>. Throws an exception if the component isn't alive.
    Ref<T> upgrade() const;

    bool operator==(const Weak &rhs) const { return data == rhs.data; }
    bool operator!=(const Weak &rhs) const { return !(rhs == *this); }

    friend std::ostream &operator<<(std::ostream &os, const Weak<T> &weak) {
        os << weak.data;
        return os;
    }
};

template<class T> Weak<T>::Weak(RefData data, std::shared_ptr<BoxInner<T>> count): meta(std::move(count)), data(data) {}

template<class T> std::optional<Weak<T>> WeakAny::as() const {
    if (type != type_id<T>()) return {};
    return { Weak<T>(data, std::static_pointer_cast<BoxInner<T>>(meta)) };
}

/// A reference to a component of any type. The component will be kept alive for as long
/// as at least one reference exists, then destroyed in the destructor of the last reference.
/// RefAny stores a std::function to destruct the item if it's the last reference dropped,
/// since it would need to know the type at compile time to call the destructor directly.
class RefAny {
protected:
    template<class T> friend class Ref;
    std::shared_ptr<void> meta {};
    uint32_t type = NULL_TYPE;
    RefData data {};
    std::function<void(RefData, std::shared_ptr<void>)> destructor;

    template<class T> explicit RefAny(std::shared_ptr<BoxInner<T>> meta, uint32_t type, RefData data):
            meta(std::static_pointer_cast<void>(meta)),
            type(type),
            data(data),
            destructor([](RefData data, std::shared_ptr<void> meta){
                drop(Ref<T>(data, std::static_pointer_cast<BoxInner<T>>(meta)));
            })
    {
        meta->refs[data.index] += 1;
    }

public:
    RefAny() = default;

    // TODO: implement & test copy assign & construct and move assign.
    RefAny& operator=(const RefAny&) = delete;
    RefAny& operator=(RefAny&&) noexcept = delete;
    RefAny(const RefAny& other) = delete;

    RefAny(RefAny&& rhs): meta(std::move(rhs.meta)), type(rhs.type), data(std::move(rhs.data)),
                          destructor(std::move(rhs.destructor)) {
        rhs.meta = nullptr;
        rhs.destructor = std::function<void(RefData, std::shared_ptr<void>)>();
        rhs.data = RefData();
    }

    ~RefAny() {
        if (meta != nullptr) destructor(data, meta);
    }

    /// Get the number of strong references to the component
    int ref_count() {
        auto meta = std::static_pointer_cast<BoxInner<uint32_t>>(this->meta);
        return meta->refs[data.index];
    }

    /// Check if the component's type is T.
    template<class T> bool is() const { return type == type_id<T>(); }

    /// Downcast the pointer to a Ref<T>. Throws an exception if the type is incorrect.
    template<class T> std::optional<Ref<T>> as() const;

    /// Check if the pointer is valid (non-empty and not moved from).
    bool valid() const {
        return meta != nullptr && data.box_id != INVALID_BOX;
    }
};

/// A reference to a component. The component is guaranteed to stay
/// around while there's at least on `Ref` pointing to it.
template<class T> class Ref {
protected:
    friend struct std::hash<Ref<T>>;
    friend class Box<T>;
    friend class Weak<T>;
    friend class RefAny;

    // A pointer to the reference count & epoch data for the storage this component belongs to.
    std::shared_ptr<BoxInner<T>> meta = nullptr;

    Ref(RefData data, std::shared_ptr<BoxInner<T>> meta): meta(std::move(meta)), data(data) {
        test_valid();
        if(TRACE) std::cout << "normal construct for " << data.index << std::endl;
        if(TRACE) std::cout << "count is " << this->meta->refs[this->data.index] << std::endl;
    }

public:
    // Contains the index, storage ID, and epoch to uniquely identify the component.
    RefData data{};

    /// Create an empty ref. Use with caution.
    Ref() = default;

    // Move assign, avoids modifying the reference count.
    Ref& operator=(Ref&& rhs) noexcept {
        if (this == &rhs) return *this;
        if(TRACE) std::cout << "move assign for " << rhs.data.index << std::endl;
        this->~Ref();
        meta = std::move(rhs.meta);
        data = std::move(rhs.data);
        // Invalidate the moved-from item such that attempting to use it throws an exception.
        // Also avoids decrementing the ref count when the moved-from value is out of scope.
        rhs.meta = nullptr;
        rhs.data.box_id = INVALID_BOX;
        if(TRACE) std::cout << "count is " << meta->refs[data.index] << std::endl;
        return *this;
    }

    // Move constructor.
    Ref(Ref&& r) noexcept: meta(r.meta), data(r.data) {
        if(TRACE) std::cout << "move construct for " << r.data.index << std::endl;
        r.meta = nullptr;
        r.data.box_id = INVALID_BOX;
        if(TRACE) std::cout << "count is " << meta->refs[data.index] << std::endl;
    }

    // Copy assign & constructor
    Ref& operator=(const Ref& rhs) {
        this->~Ref();
        data = rhs.data;
        meta = rhs.meta;
        meta->refs[data.index] += 1;
        return *this;
    }
    Ref(Ref const& rhs) {
        data = rhs.data;
        meta = rhs.meta;
        meta->refs[data.index] += 1;
    }

    // Destructor decrements the reference count and deletes the component if it's reached zero.
    ~Ref() {
        // Skip decrement if the ref's been moved.
        if (meta == nullptr || data.box_id == INVALID_BOX) return;

        if(TRACE) std::cout << "destruct for " << data.index << ", " << meta << std::endl;
        meta->refs[data.index] -= 1;
        if (meta->refs[data.index] == 0) {
            meta->epoch.reserve(data.index + 1);
            while (meta->epoch.size() < data.index + 1) {
                meta->epoch.push_back(0);
            }
            if(TRACE) std::cout << "dropping " << data << std::endl;
            drop(std::move(meta->data[data.index]));
            meta->epoch[data.index] += 1;
            meta->free.push_back(data.index);
        }
        if(TRACE) std::cout << "count is " << meta->refs[data.index] << std::endl;
        meta = nullptr;
    }

    /// Cast to a RefAny.
    RefAny any() const {
        return RefAny(meta, type_id<T>(), data);
    }

    /// Get the number of strong references to the component
    int ref_count() {
        return meta->refs[data.index];
    }

    /// Get a reference to the component
    T& operator*() {
        test_valid();
        return meta->data[data.index];
    }

    /// Get a const reference to the component
    T const& operator*() const {
        test_valid();
        return meta->data[data.index];
    }

    /// Access some member of the component
    T* operator->() { return &**this; }

    /// Access a const member of the component
    T const* operator->() const { return &**this; }

    /// Check if the reference is valid. Returns false if the reference is empty or has been moved from.
    bool valid() const {
        return  meta != nullptr && data.box_id != INVALID_BOX;
    }

    /// Throws an exception if validity checking is on and the pointer is invalid
    void test_valid() const {
        if (TEST_VALIDITY && !valid()) throw std::invalid_argument("Invalid ref");
     }

    /// Create a copy of the reference, pointing to the same component.
    Ref<T> clone() const {
        meta->refs[data.index] += 1;
        return Ref(data, meta);
    };

    /// Create a weak reference to the component.
    /// Weak references don't add to the reference count, so a component with weak references
    /// to it could be deleted.
    Weak<T> weak() const {
        return Weak<T>(data, meta);
    }

    bool operator==(const Ref &rhs) const { return data == rhs.data; }
    bool operator!=(const Ref &rhs) const { return !(rhs == *this); }
};

template<class T> inline Ref<T> Weak<T>::upgrade() const {
    test_alive();
    meta->refs[data.index] += 1;
    return Ref<T>(data, meta);
}

template<class T> std::optional<Ref<T>> RefAny::as() const {
    if (type != type_id<T>()) return {};
    auto meta = std::static_pointer_cast<BoxInner<T>>(this->meta);
    meta->refs[data.index] += 1;
    return { Ref<T>(data, meta) };
}

// Check if a type has a member named init_component
#include <util/member_check.h>
CREATE_MEMBER_CHECK(init_component);
template<> struct has_member_init_component<int> {
	static bool const value = false;
};

//template<typename U, typename = void> struct has_init_component: std::false_type {};
//template<typename U> struct has_init_component<U, typename std::enable_if<bool(sizeof(&U::init_component))>::type> : std::true_type {};

/// A collection of components of one type.
/// Adding a component returns a `Ref<T>`, which can be shared, when all copies
/// have been destroyed the component is removed.
/// Refs can be downgraded to Weak references, which don't keep the component alive.
/// Components are accessed with the subscript operator, which takes a Ref<T> or
/// Weak<T> created by this box.
/// ```
/// auto ints = Box<int>{};
/// auto some_ref = ints.create(42);
/// std::cout << ints[some_ref] << std::endl;
/// ```
template<class T> class Box {
public:
    /// The box's ID
    uint16_t id = box_id_counter++;

    /// Contains components & metadata.
    std::shared_ptr<BoxInner<T>> meta = std::make_shared<BoxInner<T>>();

    Box(Box const&) = delete;
    Box& operator=(Box const&) = delete;

    /// Create a new box.
    Box() = default;

    /// Reserve space for at least `n` components.
    void reserve(uint32_t n) {
        meta->data.reserve(n);
        meta->refs.reserve(n);
        meta->epoch.reserve(n);
    }

    /// Get a reference to a component given a RefData
    T& get(RefData const& ref) {
        if (TEST_VALIDITY && ref.box_id != id) throw wrong_box();
        return meta->data[ref.index];
    }

    /// Get a const reference to a component given a RefData
    T const& get(RefData const &ref) const {
        if (TEST_VALIDITY && ref.box_id != id) throw wrong_box();
        return meta->data[ref.index];
    }

    /// If type T has a member function init_component, call it with a weak
    /// reference to the component.
    template<class C = T>
    typename std::enable_if<has_member_init_component<C>::value, void>::type
    init_component(Weak<T> ref) {
        meta->data[ref.data.index].init_component(ref);
    }

    /// If T doesn't have init_component, do nothing.
    // TODO: this might fail in VS?
    template<class C = T>
    typename std::enable_if<!has_member_init_component<C>::value, void>::type
    init_component(Weak<T>) { }

    /// Add a component to the box. Returns a strong reference which can
    /// be shared, when all copies of the reference are gone the component is
    /// removed.
    Ref<T> create(T&& value) {
        if (meta->free.empty()) {
            auto i = (uint32_t) meta->data.size();
            meta->data.push_back(std::forward<T>(value));
            meta->refs.push_back(1);
            auto data = RefData{ i, 0, id };
            init_component(Weak<T>(data, meta));
            return Ref<T>(data, meta);
        } else {
            auto i = (uint32_t) meta->free.back();
            meta->free.pop_back();
            meta->data[i] = std::forward<T>(value);
            meta->refs[i] += 1;
            auto data = RefData{ i, meta->get_epoch(i), id };
            init_component(Weak<T>(data, meta));
            return Ref<T>(data, meta);
        }
    }

    // Overload for trivially copyable types
    Ref<T> create(T const& value) { return create(std::move(T(value))); }

    /// Construct a component in-place given arguments to it's constructor.
    /// Returns a strong reference to the new component.
    template<class... Args> Ref<T> emplace(Args&&... args) {
        // TODO: real emplace? only works if there's no free spaces...
        return create(T(std::forward<Args>(args)...));
    }

    /// Run a function for each component in parallel on the global thread pool.
    /// Waits for completion before returning.
    template<class Fn>
    void par_for_each(Fn fn) {
        par::workers.split((int) meta->data.size(), [this, fn](int i) {
            if (meta->refs[i] == 0) return;
            fn(meta->data[i]);
        });
    }

    /// Get the number of strong references to a component
    int ref_count(Ref<T> &ref) { return meta->refs[ref.data.index]; }

    /// Get the number of strong references to a component
    int ref_count(Weak<T> &ref) { return meta->refs[ref.data.index]; }

    /// Get a reference to a component given a Ref<T>.
    /// Fails if the given reference is invalid or is from another box.
    T& operator[](Ref<T> const& ref) {
        ref.test_valid();
        return get(ref.data);
    }

    /// Get a const reference to a component given a Ref<T>.
    /// Fails if the given reference is invalid or is from another box.
    T const& operator[](Ref<T> const& ref) const {
        ref.test_valid();
        return get(ref.data);
    }

    /// Get a reference to a component given a Weak<T>.
    /// Fails if the given reference is invalid or is from another box.
    T& operator[](Weak<T> const& ref) {
        ref.test_alive();
        return get(ref.data);
    }

    /// Get a const reference to a component given a Weak<T>.
    /// Fails if the given reference is invalid or is from another box.
    T const& operator[](Weak<T> const& ref) const {
        ref.test_alive();
        return get(ref.data);
    }

    /// Create a weak pointer to a component given an index.
    Weak<T> make_weak(uint32_t idx) const {
        return Weak<T>{ RefData{ idx, meta->get_epoch(idx), id }, meta };
    }

    /// An iterator over the components
    struct it_state {
        uint32_t pos;

        inline void next(const Box<T>* ref) {
            pos++;
            while (pos != ref->meta->data.size() && ref->meta->refs[pos] == 0) pos++;
        }
        inline void begin(const Box<T>* ref) {
			pos = 0;
			while (pos != ref->meta->data.size() && ref->meta->refs[pos] == 0) pos++;
		}
        inline void end(const Box<T>* ref) { pos = (uint32_t) ref->meta->data.size(); }
        inline bool cmp(const it_state& s) const { return pos != s.pos; }
        inline T& get(Box<T>* ref) {
			if (ref->meta->refs[pos] == 0) std::abort();
            return ref->meta->data[pos];
        }
        inline const T& get(const Box<T>* ref) {
            return ref->meta->data[pos];
        }
    };

    SETUP_ITERATORS(Box<T>, T&, it_state);

    struct Refs {
        Box<T> const& box;
        struct it_state {
            uint32_t pos;

            inline void next(const Refs* ref) {
                pos++;
                while (pos != ref->box.meta->data.size() && ref->box.meta->refs[pos] == 0) pos++;
            }
            inline void begin(const Refs* ref) { 
				pos = 0;
				while (pos != ref->box.meta->data.size() && ref->box.meta->refs[pos] == 0) pos++;
			}
            inline void end(const Refs* ref) { pos = (uint32_t) ref->box.meta->data.size(); }
            inline bool cmp(const it_state& s) const { return pos != s.pos; }
            inline Ref<T> get(const Refs* ref) {
                auto weak = ref->box.make_weak(pos);
                weak.test_alive();
                auto strong = weak.upgrade();
                strong.test_valid();
                return strong;
            }
        };

        SETUP_CONST_ITERATOR(Refs, Ref<T>, it_state);
    };

    Refs refs() const { return Refs{ *this }; }
};

/// A collection of boxes of any component type. Creates boxes as they're accessed.
class Storage {
private:
    std::vector<std::shared_ptr<void>> boxes{};

public:
    Storage() = default;

    // Movable & non-copyable.
    Storage& operator=(Storage&&) noexcept = default;
    Storage(Storage&&) noexcept = default;
    Storage& operator=(const Storage&) = delete;
    Storage(const Storage& other) = delete;

    /// Get a reference to the box for a given type of component
    template<class T> Box<T>& box() {
        auto id = type_id<T>();
        while (boxes.size() < id + 1) boxes.emplace_back();
        if (!boxes[id]) boxes[id] = std::make_shared<Box<T>>();

        return *std::static_pointer_cast<Box<T>>(boxes[id]);
    }

    /// Get a const reference to the box for a given type of component
    template<class T> Box<T> const& box() const {
        return const_cast<Storage*>(this)->box<T>();
    }

    /// Reserve space for at least `n` components.
    template<class T> void reserve(uint32_t n) {
        box<T>().reserve(n);
    }

    /// Add a component to the storage. Returns a strong reference which can
    /// be shared, when all copies of the reference are gone the component is
    /// removed.
    template<class T> Ref<T> create(T&& value) {
        return box<T>().create(std::forward<T>(value));
    }

    // Overload for trivially copyable types
    template<class T> Ref<T> create(T const& value) {
        return box<T>().create(value);
    }

    /// Construct a component in-place given arguments to it's constructor.
    /// Returns a strong reference to the new component.
    template<class T, class... Args> Ref<T> emplace(Args&&... args) {
        return box<T>().emplace(std::forward<Args>(args)...);
    }

    /// Run a function for each component in parallel on the global thread pool.
    /// Waits for completion before returning.
    template<class T, class Fn> void par_for_each(Fn fn) {
        box<T>().par_for_each(fn);
    }

    /// Get a reference to a component given a Ref<T>.
    /// Fails if the given reference is invalid or is from another storage storage.
    template<class T> T& operator[](Ref<T> const& ref) {
        return box<T>().operator[](ref);
    }

    /// Get a const reference to a component given a Ref<T>.
    /// Fails if the given reference is invalid or is from another storage storage.
    template<class T> T const& operator[](Ref<T> const& ref) const {
        return box<T>().operator[](ref);
    }

    /// Get a reference to a component given a Weak<T>.
    /// Fails if the given reference is invalid or is from another storage storage.
    template<class T> T& operator[](Weak<T> const& ref) {
        return box<T>().operator[](ref);
    }

    /// Get a const reference to a component given a Weak<T>.
    /// Fails if the given reference is invalid or is from another storage storage.
    template<class T> T const& operator[](Weak<T> const& ref) const {
        return box<T>().operator[](ref);
    }
};

// Implement hash for reference types.
namespace std {
    template<> struct hash<RefData> {
        std::size_t operator()(RefData const& k) const {
            using std::size_t;
            using std::hash;
            using std::string;

            return ((hash<uint32_t>()(k.index)
                  ^ (hash<uint16_t>()(k.epoch) << 1)) >> 1)
                  ^ (hash<uint16_t>()(k.box_id) << 1);
        }
    };

    template<class T> struct hash<Ref<T>> {
        std::size_t operator()(Ref<T> const& k) const {
            return hash<RefData>()(k.data);
        }
    };

    template<class T> struct hash<Weak<T>> {
        std::size_t operator()(Weak<T> const& k) const {
            return hash<RefData>()(k.data);
        }
    };
}
