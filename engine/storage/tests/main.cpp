#define CATCH_CONFIG_MAIN
#define BACKWARD_HAS_BFD 1

#include <catch.hpp>
#include <backward.hpp>
#include <storage/storage.hpp>
#include <util/util.hpp>

backward::SignalHandling sh;

TEST_CASE("component deletion") {
	Box<int> box{};
    auto ref = box.create(42);
    auto weak = ref.weak();

    REQUIRE(*ref == 42);
    REQUIRE(box[weak] == 42);

    // Check that the object is inaccessible after the only strong ref is dropped.
    drop(std::move(ref));
    REQUIRE(!weak.is_alive());
    REQUIRE_THROWS(box[weak]);
}

TEST_CASE("vector of refs") {
	Box<int> box{};

    auto refs = std::vector<Ref<int>>();
    refs.push_back(box.create(0));
    refs.push_back(box.create(1));
    refs.push_back(box.create(2));

    auto weak = refs[1].weak();
    refs.erase(refs.begin() + 1);

    REQUIRE_THROWS(box[weak]);

    {
        refs.push_back(box.create(1));
        auto weak = refs.back().weak();
        refs.erase(refs.end() - 1);
        REQUIRE_THROWS(box[weak]);
    }
}

TEST_CASE("iter") {
	Box<int> storage{};
    auto ref0 = storage.create(0);
    auto ref1 = storage.create(1);
    auto ref2 = storage.create(2);
    auto ref3 = storage.create(3);

    auto iter = storage.begin();

    REQUIRE(*(iter) == 0);

    REQUIRE(*(++iter) == 1);
    REQUIRE(*(++iter) == 2);
    REQUIRE(*(++iter) == 3);
    REQUIRE(++iter == storage.end());

    drop(std::move(ref2));
    iter = storage.begin();

    REQUIRE(*(iter) == 0);
    REQUIRE(*(++iter) == 1);
    REQUIRE(*(++iter) == 3);
    REQUIRE(++iter == storage.end());
}
