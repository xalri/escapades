#pragma once

#include <deque>
#include <vector>
#include <stdexcept>
#include "poly.hpp"

/// Calculate the minimum convex decomposition of the given simple poly in O(n^3) time.
/// Returns indices. Fails if the polygon is self-intersecting.
std::vector<std::vector<int>> convex_partition(Poly const& poly);

/// An exception thrown when convex_partition is called with a self-intersecting polygon.
class invalid_poly: public std::logic_error {
public:
    invalid_poly(): logic_error("Convex decomposition failed; invalid (self-intersecting?) polygon") {}
};

/// Dynamic programming state for a diagonal
struct DPState {
    bool visible;
    unsigned weight;
    std::deque<std::pair<int, int>> pairs;
};

/// Dynamic programming state for an entire poly
typedef std::vector<std::vector<DPState>> States;

bool is_reflex(Vector2f a, Vector2f b, Vector2f c);
void type_a(int i, int j, int k, Poly const& poly, States& state);
void type_b(int i, int j, int k, Poly const& poly, States& state);
void update_state(int a, int b, unsigned w, int i, int j, States& state);
