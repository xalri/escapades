#include <catch.hpp>
#include <math/math.hpp>

TEST_CASE("wrap") {
   REQUIRE(wrap(0.5, 0.0, 1.0) == 0.5);
   REQUIRE(wrap(-0.5, 0.0, 1.0) == 0.5);
   REQUIRE(wrap(-0.2, 0.0, 1.0) == 0.8);
   REQUIRE(wrap(1.5, 0.0, 1.0) == 0.5);
   REQUIRE(wrap(1.2, 0.0, 1.0) == Approx(0.2));
   REQUIRE(wrap(0.0, 0.0, 1.0) == 0.0);
   REQUIRE(wrap(-1.0, 0.0, 1.0) == 0.0);
   REQUIRE(wrap(1.0, 0.0, 1.0) == 0.0);
}

TEST_CASE("map") {
   REQUIRE(map(0.5f, range(0.0, 1.0), range(10.0, 20.0)) == 15.0);
   REQUIRE(map(2.0f, range(0.0, 1.0), range(10.0, 20.0)) == 20.0);
   REQUIRE(map(2.0f, range(0.0, 1.0), range(20.0, 10.0)) == 10.0);
   REQUIRE(map(0.7f, range(0.0, 1.0), range(10.0, 20.0)) == 17.0);
   REQUIRE(map(0.7f, range(0.0, 1.0), range(20.0, 10.0)) == 13.0);
   REQUIRE(map(7.0f, range(5.0, 10.0), range(20.0, 10.0)) == 16.0);
   REQUIRE(map(7.0f, range(5.0, 10.0), range(10.0, 20.0)) == 14.0);
}
