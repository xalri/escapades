#include <catch.hpp>
#include <math/poly.hpp>

TEST_CASE("area") {
    auto poly = Poly::square();
    REQUIRE(poly.area() == 20.0f * 20.0f);
}

TEST_CASE("convex decomposition") {
    auto poly = Poly(std::vector<Vector2f> {
            vec2(1.f, 1.f),
            vec2(2.f, 1.f),
            vec2(2.f, -1.f),
            vec2(1.f, -1.f),
            vec2(1.f, -2.f),
            vec2(-1.f, -2.f),
            vec2(-1.f, -1.f),
            vec2(-2.f, -1.f),
            vec2(-2.f, 1.f),
            vec2(-1.f, 1.f),
            vec2(-1.f, 2.f),
            vec2(1.f, 2.f),
    });
    // TODO: ...
}