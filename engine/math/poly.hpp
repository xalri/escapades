#pragma once

#include <iterator_tpl.h>
#include <vector>
#include <mutex>
#include <algorithm>
#include <memory>
#include <optional>
#include <ostream>
#include "math.hpp"

/// The scalar cross product of a polygon. Positive when the polygon is clockwise.
inline float cross(std::vector<Vector2f> const &points) {
    auto sum = 0.0f;
    auto p = points[points.size() - 1];
    for(auto& q : points) {
        sum += p.x * q.y - q.x * p.y;
        p = q;
    }
    return sum;
}

/// The scalar cross product of three points.
inline float cross(Vector2f a, Vector2f b, Vector2f c) {
    return (c.x * a.y - a.x * c.y) + (a.x * b.y - b.x * a.y) + (b.x * c.y - c.x * b.y);
}

/// Test if the segments a0->a1 and b0->b1 intersect.
/// Returns false if the segments are collinear, or touch at the end.
inline bool intersects(Vector2f a0, Vector2f a1, Vector2f b0, Vector2f b1) {
    auto o1 = cross(a0, a1, b0);
    auto o2 = cross(a0, a1, b1);
    auto o3 = cross(b0, b1, a0);
    auto o4 = cross(b0, b1, a1);

    return (o1 > 0.0) != (o2 > 0.0) && (o3 > 0.0) != (o4 > 0.0);
}

/// The intersection point of two segments.
/// Collinear segments are not considered intersecting.
inline std::optional<Vector2f> intersection_point(Vector2f a0, Vector2f a1, Vector2f b0, Vector2f b1) {
    if (!intersects(a0, a1, b0, b1)) return {};

    auto a_ = a1 - a0;
    auto b_ = b1 - b0;
    auto cross = a_.perp_dot(b_);
    if (cross == 0.0) return {};

    auto diff = a0 - b0;
    auto t = b_.perp_dot(diff) / cross;

    return { Vector2f(a0.x + (t * a_.x), a0.y + (t * a_.y)) };
}

/// The perpendicular distance from the line a->b to the point c
inline float perpendicular_distance(Vector2f a, Vector2f b, Vector2f p) {
    auto c = p.x * (b.y - a.y) - p.y * (b.x-a.x) + b.x * a.y - b.y * a.x;
    auto d = std::pow(b.y - a.y, 2) + std::pow(b.x - a.x, 2);
    return static_cast<float>(std::abs(c) / std::sqrt(d));
}

/// Test if point p is to the right of a -> b
inline bool right_of(Vector2f a, Vector2f b, Vector2f p) {
    return (b - a).perp_dot(p - a) < 0.0;
}

/// Test if a diagonal a1->b is locally inside a clockwise polygon
/// (a0, a1, a2, ... b)
inline bool locally_inside(Vector2f a0, Vector2f a1, Vector2f a2, Vector2f b) {
    if (cross(a0, a1, a2) > 0.0) return right_of(a0, a1, b) || right_of(a1, a2, b);
    else return right_of(a0, a1, b) && right_of(a1, a2, b);
}

/// Find the closest point on a segment to a point.
inline Vector2f project_on_segment(Vector2f p, Vector2f a, Vector2f b) {
    if (a == b) return a;
    auto ab = b - a;
    auto t = (p - a).dot(ab) / ab.dot(ab);
    if (t <= 0.0f) return a;
    if (t >= 1.0f) return b;
    return a + ab * t;
}

struct Segments;
struct Iter;

/// A simple (non-intersecting) polygon, optimised for collision tests.
class Poly {
private:
    std::vector<Vector2f> points{};
    // Cached results of the more computationally intensive methods
    mutable std::vector<std::shared_ptr<Poly>> _convex{};
    mutable std::optional<float> _area{};
    mutable std::optional<float> _base_inertia{};
    mutable std::optional<float> _radius{};
    mutable std::optional<Bounds<float>> _aabb{};
    mutable std::optional<Vector2f> _centroid{};
    mutable std::mutex cache_lock{};

    void calculate_convex() const;

    /// Remove cached values for convex decomposition etc.
    void reset_cache() {
        auto lock = std::unique_lock<std::mutex>(cache_lock);
        _convex.clear();
        _radius = std::nullopt;
        _centroid = std::nullopt;
        _area = std::nullopt;
        _aabb = std::nullopt;
        _base_inertia = std::nullopt;
    }

protected:
    friend class Body;
    friend struct Segments;
    Poly() = default;

public:
    /// Creates a Poly from a vector of points
    explicit Poly(std::vector <Vector2f> p) {
        auto n = p.size();
        if (n < 3) { throw std::invalid_argument("Poly requires at least three points."); }

        if (cross(p) > 0.0) std::reverse(p.begin(), p.end());
        points = std::move(p);

        simplify(0.01f);
        //if (intersects_self()) throw std::invalid_argument("Self-intersecting poly");
    }

    friend std::ostream &operator<<(std::ostream &os, const Poly &poly);

    // Movable & non-copyable.
    Poly(const Poly&) = delete;
    Poly& operator=(const Poly&) = delete;

    Poly(Poly&& rhs) noexcept {
        auto rhs_lock = std::unique_lock<std::mutex>(rhs.cache_lock);

        points = std::move(rhs.points);
        _convex = std::move(rhs._convex);
        _area = std::move(rhs._area);
        _base_inertia = std::move(rhs._base_inertia);
        _radius = std::move(rhs._radius);
        _aabb = std::move(rhs._aabb);
        _centroid = std::move(rhs._centroid);
    }

    Poly& operator=(Poly&& rhs) noexcept {
        if (this != &rhs) {
            auto rhs_lock = std::unique_lock<std::mutex>(rhs.cache_lock, std::defer_lock);
            auto lhs_lock = std::unique_lock<std::mutex>(cache_lock, std::defer_lock);
            // Lock at the same time to prevent deadlock if two threads assign a=b and b=a.
            std::lock(rhs_lock, lhs_lock);

            points = std::move(rhs.points);
            _convex = std::move(rhs._convex);
            _area = std::move(rhs._area);
            _base_inertia = std::move(rhs._base_inertia);
            _radius = std::move(rhs._radius);
            _aabb = std::move(rhs._aabb);
            _centroid = std::move(rhs._centroid);
        }
        return *this;
    }

    // Const & mutable subscript operators. Wraps negatives & large values for convenience.
    Vector2f& operator[](int index) { return points[wrap_max(index, size())]; }
    Vector2f const& operator[](int index) const { return points[wrap_max(index, size())]; }

    /// Clone the Poly
    Poly clone() const {
        auto lock = std::unique_lock<std::mutex>(cache_lock);
        Poly poly;
        poly.points = points;
        poly._convex = _convex;
        poly._area = _area;
        poly._base_inertia = _base_inertia;
        poly._radius = _radius;
        poly._aabb = _aabb;
        poly._centroid = _centroid;
        return poly;
    }

    /// Get the point at a given index.
    Vector2f get(int index) const { return points[wrap_max(index, size())]; }

    /// Create a rectangle with the given dimensions.
    static Poly rect(float width, float height, bool center = true) {
        if (center) {
            auto w = width / 2.0f;
            auto h = height / 2.0f;
            auto vec = std::vector<Vector2f>{ vec2(w, h), vec2(w, -h), vec2(-w, -h), vec2(-w, h) };
            return Poly(vec);
        } else {
            auto w = width;
            auto h = height;
            auto vec = std::vector<Vector2f>{ vec2<float>(w, h), vec2<float>(w, 0), vec2<float>(0, 0), vec2<float>(0, h) };
            return Poly(vec);
        }
    }

    /// Create a 20 x 20 square
    static Poly square() {
        return rect(20.0f, 20.0f);
    }

    /// Approximate an ellipse with the given vertex count & dimensions
    static Poly ellipse(unsigned v, float width, float height) {
        if (v < 3) throw std::invalid_argument("Poly::blob requires at least three points");
        auto points = std::vector<Vector2f>();
        for(unsigned i = 0; i < v; i++) {
            auto angle = (M_PI / (float)v) + (2.0 * M_PI * (float)i / (float)v);
            points.push_back(vec2((float) std::cos(angle) * width / 2.f, (float) std::sin(angle) * height / 2.f));
        }
        return Poly(points);
    }

    /// Test if the poly is self-intersecting..
    bool intersects_self() const {
        for (auto i = 0; i < size(); i++) {
            auto dist_sq = (get(i) - get(i+1)).length_sq();
            for (auto j = 0; j < size(); j++) {
                if (j == i || wrap_max(j + 1, size()) == i) { continue; }

                if (auto _p = intersection_point(get(i), get(i + 1), get(j), get(j + 1))) {
                    auto p = *_p;
                    if ((p - get(i)).length_sq() < dist_sq - 0.00001) return true;
                }
            }
        }
        return false;
    }

    /// Calculate the intersection of this poly with a _convex_ clipping poly
    std::optional<Poly> clip(Poly const& clip) const {
        std::vector<Vector2f> in{};
        std::vector<Vector2f> out = points;
        for (auto i = 0; i < clip.size(); i++) {
            in = out;
            out.clear();
            auto s = in.back();
            for (auto p : in) {
                if (cross(clip[i], clip[i+1], p) < 0.f) {
                    if (auto _p = intersection_point(clip[i], clip[i+1], s, p))
                        if (out.back() != *_p) out.push_back(*_p);
                    if (out.back() != p) out.push_back(p);
                } else {
                    if (auto _p = intersection_point(clip[i], clip[i+1], s, p))
                        if (out.back() != *_p) out.push_back(*_p);
                }
                s = p;
            }
        }
        if (out.size() > 2) return {Poly(out)};
        return {};
    }

    /// Apply a transformation matrix to every point in the poly.
    void transform(Matrix3f matrix) {
        for(auto& point : *this) point = matrix * point;
        if (cross(points) > 0.0) std::reverse(points.begin(), points.end());
        reset_cache();
    }

    /// Expand along the vertex normals
    void expand(float distance) {
        auto normals = std::vector<Vector2f>();
        for(auto i = 0; i < size(); i++) {
            normals.push_back(-((get(i-1) - get(i)) + (get(i+1) - get(i))).normalize());
        }
        for(auto i = 0; i < size(); i++) {
            points[i] += normals[i] * distance;
        }
        reset_cache();
    }

    /// Recursively simplify the polyline a->b using Douglas-Peucker.
    /// Fails if `e` is negative or if b > a.
    void douglas_peucker(int a, int b, float e) {
        if (e < 0.0) { throw std::invalid_argument("douglas_peucker called with negative epsilon"); }
        if (a > b) { throw std::invalid_argument("douglas_peucker invalid range"); }

        if (b - a < 2) { return; }

        // Find the point furthest from the line a -> b
        auto distance = 0.0;
        auto index = a + 1;

        for (auto i = a+1; i < b; i++) {
            auto d = perpendicular_distance(get(a), get(b), get(i));
            if (d > distance) { index = i; distance = d; }
        }

        if (distance < e) {
            // If all points are all close to the line a -> b we remove them.
            // TODO: is this correct?
            points.erase(points.begin() + a + 1, points.begin() + b);
            reset_cache();
        } else {
            // Recursively simplify the segments to the left and right of the point furthest
            // from the line. The right half is simplified first to preserve indices.
            douglas_peucker(index, b, e);
            douglas_peucker(a, index, e);
        }
    }

    /// Simplify the polygon by removing points which are close to the line connecting their
    /// adjacent points. Fails if epsilon is negative.
    void simplify(float e) {
        auto len = size();
        douglas_peucker(0, len - 1, e);
        // Remove the first point if it's collinear with it's adjacent points
        if (size() > 2 && perpendicular_distance(get(-1), get(1), get(0)) < e) {
            points.erase(points.begin());
            reset_cache();
        }
        // Remove the last point if it's collinear with it's adjacent points
        if (size() > 2 && perpendicular_distance(get(-2), get(0), get(-1)) < e) {
            points.erase(points.end() - 1);
            reset_cache();
        }
    }

    /// Test if the polygon is convex
    bool is_convex() const {
        if (size() == 3) { return true; }
        for (auto i = 0; i < size(); i++) {
            if (cross(get(i), get(i + 1), get(i + 2)) > 0.0) return false;
        }
        return true;
    }

    /// Get an iterator over the segments of the poly
    Segments segments() const;

    /// Calculate the relative rotational inertia.
    float inertia() const;

    /// Calculate the area of the polygon
    float area() const;

    /// The number of vertices in the poly.
    int size() const { return static_cast<int>(points.size()); }

    /// Get the axis-aligned bounding box
    Bounds<float> const& aabb() const {
        if (!_aabb) {
            auto a = (*this)[0];
            auto aabb = Bounds<float>(range(a.x, a.x), range(a.y, a.y));
            for (auto p : (*this)) {
                if (p.x < aabb.x.min) aabb.x.min = p.x;
                if (p.x > aabb.x.max) aabb.x.max = p.x;
                if (p.y < aabb.y.min) aabb.y.min = p.y;
                if (p.y > aabb.y.max) aabb.y.max = p.y;
            }
            auto lock = std::unique_lock<std::mutex>(cache_lock);
            _aabb = { aabb };
        }
        return *_aabb;
    }

    /// Project the polygon onto an axis.
    Range<float> project(Vector2f axis) const {
        auto min = points[0].dot(axis);
        auto max = min;
        for(auto i = 1; i < size(); i++) {
            auto a = points[i].dot(axis);
            if (a < min) min = a;
            else if (a > max) max = a;
        }
        return Range<float>{min, max, false};
    }

    /// Get the cached minimal convex decomposition.
    std::vector<std::shared_ptr<Poly>> const& convex() const {
        if (_convex.empty()) calculate_convex();
        return _convex;
    }

    /// The average of the polygon's points.
    Vector2f centroid() const {
        if (!_centroid) {
            auto sum = vec2(0.f, 0.f);
            for (auto p : (*this)) sum += p;
            auto lock = std::unique_lock<std::mutex>(cache_lock);
            _centroid = {sum / (float) size()};
        }
        return *_centroid;
    }

    /// The poly's bounding box
    Bounds<float> bounds(Vector2f pos, float angle, float scale) const {
        if (float_eq(angle, 0.f)) {
            return aabb() * scale + pos;
        } else {
            auto c = pos + rotate(angle) * centroid() * scale;
            auto r = radius() * scale;
            return Bounds<float>{ range(c.x - r, c.x + r), range(c.y - r, c.y + r) };
        }
    }

    /// The poly's swept bounding box for one frame at the given position & velocity.
    Bounds<float> swept_bounds(Vector2f pos, float angle, Vector2f vel, float scale) const {
        auto b = bounds(pos, angle, scale);
        if (vel.x < 0) b.x.min += vel.x;
        else if (vel.x > 0) b.x.max += vel.x;
        if (vel.y < 0) b.y.min += vel.y;
        else if (vel.y > 0) b.y.max += vel.y;
        return b;
    }

    /// Calculate the radius of the polygon
    float radius() const {
        if (!_radius) {
            auto radius = 0.0f;
            auto center = centroid();
            for (auto s : (*this)) {
                auto len = (center - s).length();
                if (len > radius) radius = len;
            }
            auto lock = std::unique_lock<std::mutex>(cache_lock);
            _radius = { radius };
        }
        return *_radius;
    }

    struct it_state {
        int pos;

        inline void next(const Poly* ref) { pos++; }
        inline void begin(const Poly* ref) { pos = 0; }
        inline void end(const Poly* ref) { pos = ref->size(); }
        inline bool cmp(const it_state& s) const { return pos != s.pos; }
        inline Vector2f& get(Poly* ref) const {
            return ref->points[pos];
        }
        inline Vector2f const& get(const Poly* ref) const {
            return ref->points[pos];
        }
    };
    SETUP_ITERATORS(Poly, Vector2f&, it_state);
};

/// A segment of a polygon
struct Segment { Vector2f a, b; };

/// An iterator over the segments of a poly
struct Segments {
    Poly const& p;
    explicit Segments(Poly const& p): p(p) {}
    struct it_state {
        int pos;

        inline void next(const Segments* ref) { pos++; }
        inline void begin(const Segments* ref) { pos = 0; }
        inline void end(const Segments* ref) { pos = ref->p.size(); }
        inline bool cmp(const it_state& s) const { return pos != s.pos; }
        inline const Segment get(const Segments* ref) { return Segment{ ref->p.points[pos], ref->p[pos + 1] }; }
    };
    SETUP_CONST_ITERATOR(Segments, Segment, it_state);
};

inline Segments Poly::segments() const { return Segments{ *this }; }

inline float Poly::inertia() const {
    if (!_base_inertia) {
        auto numerator = 0.0f;
        auto denominator = 0.0f;
        for (auto s : segments()) {
            auto u = s.a; auto v =s.b;
            auto a = std::abs(u.perp_dot(v));
            auto b = u.x * (u.x + v.x) + u.y * (u.y + v.y) + v.x * v.x + v.y * v.y;
            numerator += a * b;
            denominator += a;
        }
        auto i = 4.0f * numerator / (6.0f * denominator);
        auto lock = std::unique_lock<std::mutex>(cache_lock);
        _base_inertia = { i };
    }
    return *_base_inertia;
}

inline float Poly::area() const {
    if (!_area) {
        auto sum = 0.0f;
        for (auto s : segments()) sum += std::abs(s.a.perp_dot(s.b));
        auto lock = std::unique_lock<std::mutex>(cache_lock);
        _area = { sum / 2.0f };
    }
    return *_area;
}

#include "decomp.hpp"

inline void Poly::calculate_convex() const {
    auto& c = _convex;
    if (is_convex()) {
        auto clone = std::make_shared<Poly>(this->clone());
        auto lock = std::unique_lock<std::mutex>(cache_lock);
        c.clear();
        c.push_back(clone);
    } else {
        auto lock = std::unique_lock<std::mutex>(cache_lock);
        c.clear();

        auto idx = convex_partition(*this);
        c.reserve(idx.size());

        for (auto& idx : idx) {
            Poly poly;
            poly.points = std::vector<Vector2f>();
            for(auto i : idx) poly.points.push_back((*this).points[i]);

            poly.simplify(0.0001f); // remove collinear points

            c.push_back(std::make_shared<Poly>(std::move(poly)));
        }
    }
}

inline std::ostream &operator<<(std::ostream &os, const Poly &poly) {
    os << "[";
    for(auto& p : poly) os << p << ",";
    os << "]";
    return os;
}
