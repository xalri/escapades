// A rewrite of polypartition's minimum convex partitioning algorithm.
// The original code is available at https://github.com/ivanfratric/polypartition

// Copyright (C) 2011 by Ivan Fratric
// Modifications by Lewis Hallam, 2018
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

#include "poly.hpp"
#include "decomp.hpp"
#include <vector>
#include <algorithm>

std::vector <std::vector<int>> convex_partition(const Poly &poly) {
	auto n = poly.size();
	auto state = std::vector<std::vector<DPState>>(n, std::vector<DPState>(n, DPState()));

	// Find the set of reflex vertices
	auto is_convex = std::vector<bool>(n);
	for(auto i = 0; i < n; i++) is_convex[i] = !is_reflex(poly[i-1], poly[i], poly[i+1]);

	// Set initial weights and check visibility for each diagonal
	for(auto i = 0; i < n; i++) {
		for(auto j = i + 1; j < n; j++) {
			state[i][j].visible = true;
			state[i][j].weight = (j == i + 1) ? 0 : 2147483647;

			// Skip visibility checks for adjacent vertices
			if (j == i + 1) continue;

			// A local visibility check for each vertex
			if (!locally_inside(poly[i-1], poly[i], poly[i+1], poly[j])) {
				state[i][j].visible = false;
				continue;
			}
			if (!locally_inside(poly[j-1], poly[j], poly[j+1], poly[i])) {
				state[i][j].visible = false;
				continue;
			}

			// If they're locally visible, we do an intersection test against each
			// segment in the polygon.
			auto dist_sq = (poly[i] - poly[j]).length_sq();
			for (auto k = 0; k < n; k++) {
				if (k == i || wrap_max(k + 1, n) == i) { continue; }

				if (auto _p = intersection_point(poly[i], poly[j], poly[k], poly[k + 1])) {
					auto p = *_p;
					// TODO: is this distance test necessary?
					if ((p - poly[i]).length_sq() < dist_sq - 0.00001) {
						state[i][j].visible = false;
						break;
					}
				}
			}
		}
	}

	for (auto i = 0; i < n - 2; i++) {
		auto j = i + 2;
		if (state[i][j].visible) {
			state[i][j].weight = 0;
			state[i][j].pairs.emplace_back(i+1, i+1);
		}
	}

	state[0][(n-1)].visible = true;
	is_convex[0] = false;

	for (auto gap = 3; gap < n; gap++) {
		for (auto i = 0; i < (n - gap); i++) {
			if (is_convex[i]) continue;
			auto k = i + gap;
			if (state[i][k].visible) {
				if (!is_convex[k]) {
					for (auto j = (i+1); j < k; j++) type_a(i, j, k, poly, state);
				} else {
					for (auto j = (i+1); j < (k-1); j++) {
						if (is_convex[j]) continue;
						type_a(i, j, k, poly, state);
					}
					type_a(i, k-1, k, poly, state);
				}
			}
		}
		for (auto k = gap; k < n; k++) {
			if (is_convex[k]) continue;
			auto i = k - gap;
			if (is_convex[i] && state[i][k].visible) {
				type_b(i, i+1, k, poly, state);
				for (auto j = (i+2); j < k; j++) {
					if (is_convex[j]) continue;
					type_b(i, j, k, poly, state);
				}
			}
		}
	}

	auto diagonals = std::deque<std::pair<int, int>>();
	diagonals.emplace_front(0, n-1);

	while (!diagonals.empty()) {
		auto diagonal = diagonals.front();
		diagonals.pop_front();

		if (diagonal.second - diagonal.first <= 1) { continue; }
		auto &pairs = state[diagonal.first][diagonal.second].pairs;

		if (pairs.empty()) throw invalid_poly();

		if (!is_convex[diagonal.first]) {
			auto last = pairs.back();
			auto j = last.second;
			diagonals.emplace_front(j, diagonal.second);

			if (j - diagonal.first > 1) {
				if (last.first != last.second) {
					auto &pairs2 = state[diagonal.first][j].pairs;
					while (true) {
						if (pairs2.empty()) throw invalid_poly();
						auto last2 = pairs2.back();
						if (last.first != last2.first) { pairs2.pop_back(); }
						else { break; }
					}
				}
				diagonals.emplace_front(diagonal.first, j);
			}
		} else {
			auto first = pairs.front();
			auto j = first.first;
			diagonals.emplace_front(diagonal.first, j);

			if (diagonal.second - j > 1) {
				if (first.first != first.second) {
					auto &pairs2 = state[j][diagonal.second].pairs;
					while (true) {
						if (pairs2.empty()) throw invalid_poly();
						auto first2 = pairs2[0];
						if (first.second != first2.second) { pairs2.pop_front(); }
						else { break; }
					}
				}
				diagonals.emplace_front(j, diagonal.second);
			}
		}
	}

	auto parts = std::vector<std::vector<int>>();
	auto diagonals2 = std::deque<std::pair<int, int>>();

	diagonals.emplace_front(0, n - 1);

	while (!diagonals.empty()) {
		auto diagonal = diagonals.front();
		diagonals.pop_front();

		if (diagonal.second - diagonal.first <= 1) { continue; }

		auto indices = std::vector<int>();
		diagonals2.clear();

		indices.push_back(diagonal.first);
		indices.push_back(diagonal.second);
		diagonals2.push_front(diagonal);

		while (!diagonals2.empty()) {
			auto diagonal = diagonals2.front();
			diagonals2.pop_front();

			if (diagonal.second - diagonal.first <= 1) { continue; }
			auto ijreal = true;
			auto jkreal = true;

			auto& pairs = state[diagonal.first][diagonal.second].pairs;

			int j;

			if (!is_convex[diagonal.first]) {
				auto last = pairs.back();
				if (last.first != last.second) { ijreal = false; }
				j = last.second;
			} else {
				auto first = pairs[0];
				if (first.first != first.second) { jkreal = false; }
				j = first.first;
			};

			if (ijreal) {
				diagonals.emplace_back(diagonal.first, j);
			} else {
				diagonals2.emplace_back(diagonal.first, j);
			}

			if (jkreal) {
				diagonals.emplace_back(j, diagonal.second);
			} else {
				diagonals2.emplace_back(j, diagonal.second);
			}

			indices.push_back(j);
		}

		std::sort(indices.begin(), indices.end());
		parts.push_back(indices);
	}

	return parts;
}

bool is_reflex(Vector2f a, Vector2f b, Vector2f c) {
	return cross(a, b, c) > 0.0;
}

void type_a(int i, int j, int k, Poly const &poly, States &state) {
	if (!state[i][j].visible)  return;

	auto top = j;
	auto w = state[i][j].weight;

	if (k - j > 1) {
		if (!state[j][k].visible)  return;
		w += state[j][k].weight + 1;
	}

	if (j - i > 1) {
		auto& pairs = state[i][j].pairs;

		auto last = std::optional<int>();
		for(auto p = pairs.rbegin(); p != pairs.rend(); ++p) {
			if (!is_reflex(poly[p->second], poly[j], poly[k]))  last = std::optional<int>(p->first);
			else  break;
		}

		if (last) {
			if (is_reflex(poly[k], poly[i], poly[*last]))  w += 1;
			else  top = *last;
		} else {
			w += 1;
		}
	}
	update_state(i, k, w, top, j, state);
}

void type_b(int i, int j, int k, Poly const &poly, States &state) {
	if (!state[j][k].visible) return;

	auto top = j;
	auto w = state[j][k].weight;

	if (j - i > 1) {
		if (!state[i][j].visible) return;
		w += state[i][j].weight + 1;
	}
	if (k - j > 1) {
		auto& pairs = state[j][k].pairs;

		if (!pairs.empty() && !is_reflex(poly[i], poly[j], poly[pairs[0].first])) {
			auto last = pairs[0].second;

			for (auto pair : pairs) {
				if (!is_reflex(poly[i], poly[j], poly[pair.first])) { last = pair.second; }
				else { break; }
			}
			if (is_reflex(poly[last], poly[k], poly[i])) { w += 1; }
			else { top = last; }
		} else {
			w += 1;
		}
	}
	update_state(i, k, w, j, top, state);
}

void update_state(int a, int b, unsigned w, int i, int j, States &state) {
	auto w2 = state[a][b].weight;
	if (w > w2) return;

	auto &pairs = state[a][b].pairs;

	if (w < w2) {
		pairs.clear();
		pairs.emplace_back(i, j);
		state[a][b].weight = w;
	} else {
		if (!pairs.empty() && i <= pairs[0].first) return;
		while (!pairs.empty() && pairs[0].second >= j) pairs.pop_front();
		pairs.emplace_front(i, j);
	}
}
