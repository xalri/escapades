#pragma once

#include <cmath>
#include <iostream>
#include <iterator_tpl.h>

// ================================================================= //
//                        General Functions
// ================================================================= //

#ifndef M_PI
#define M_PI 3.1415926535897932384626433832795028
#endif

/// Clamp a value to a given range
template<class T> constexpr T clamp(const T& v, const T& min, const T& max) {
    return v < min ? min : v > max ? max : v;
}

/// Floating point modulus
template<class T> constexpr T fmod(T a, T b) {
    return ((a) - ((int)((a)/(b))) * (b));
}

/// Wrap a value between 0 and max
template<class T> constexpr T wrap_max(const T& v, const T& max) {
    return fmod(max + fmod(v, max), max);
}

constexpr int wrap_max(const int v, const int max) { return (max + (v % max)) % max; }
constexpr unsigned wrap_max(const unsigned v, const unsigned max) { return (max + (v % max)) % max; }

/// Wrap a value in the range [min, max)
template<class T> constexpr T wrap(const T& v, const T& min, const T& max) {
    return min + wrap_max(v - min, max - min);
}

/// Convert an angle in degrees to radians
template<class T> constexpr T deg2rad(const T& v) {
    return v * (T) M_PI / 180;
}

/// Convert an angle in radians to degrees
template<class T> constexpr T rad2deg(const T& v) {
    return v * 180 / (T) M_PI;
}


// ================================================================= //
//                            Vectors
// ================================================================= //

/// Relative floating point comparison
template<typename T> constexpr bool float_eq(T f1, T f2) {
    auto max = std::fmax(10, std::fmax(fabs(f1), fabs(f2)));
    auto e = std::numeric_limits<T>::epsilon();
    return std::fabs(f1 - f2) <= e * max;
}

constexpr bool float_eq(int f1, int f2) {
    return f1 == f2;
}

/// A 2D Vector
template<class S> struct Vector2 {
    S x, y;

    constexpr explicit Vector2(S x = 0, S y = 0): x(x), y(y) {}

    /// Cast the vector to a different vector type.
    template<class T> constexpr Vector2<T> cast() const {
        return Vector2<T>{ static_cast<T>(x), static_cast<T>(y) };
    }

    /// The squared magnitude of the vector
    constexpr S length_sq() const {
        return x * x + y * y;
    }

    /// The magnitude of the vector
    constexpr S length() const {
        return std::sqrt(length_sq());
    }

    /// Get the angle of the vector from the positive x axis in degrees.
    constexpr float angle() const {
        return rad2deg(std::atan2(y, x));
    }

    /// Get a unit vector pointing in the same direction
    constexpr Vector2<S> normalize() const {
        auto len = length();
        if (len == 0) return Vector2<S>();
        else return Vector2<S>{x / len, y / len};
    }

    /// Get the tangent to this vector
    constexpr Vector2<S> perpendicular() const {
        return Vector2<S>{ -y, x };
    }

    constexpr Vector2<S> inv_y() const {
        return Vector2<S>{ x, -y };
    }

    /// The dot product of two vectors.
    constexpr S dot(const Vector2<S>& rhs) const {
        return (x * rhs.x) + (y * rhs.y);
    }

    /// The cross product (perpendicular dot product) of two vectors.
    constexpr S perp_dot(const Vector2<S> &rhs) const {
        return (x * rhs.y) - (y * rhs.x);
    }

    constexpr Vector2<S> operator*(S v) const {
        return Vector2<S>{x * v, y * v};
    }

    constexpr Vector2<S>& operator*=(S v) {
        x *= v; y *= v;
        return *this;
    }

    constexpr Vector2<S> operator/(S v) const {
        return Vector2<S>{x / v, y / v};
    }

    constexpr Vector2<S>& operator/=(S v) {
        x /= v; y /= v;
        return *this;
    }

    constexpr Vector2<S> operator-() const {
        return Vector2<S>{ -x, -y };
    }

    constexpr Vector2<S> operator+(const Vector2<S>& rhs) const {
        return Vector2<S>{x + rhs.x, y + rhs.y};
    }

    constexpr Vector2<S> operator*(const Vector2<S>& rhs) const {
        return Vector2<S>{x * rhs.x, y * rhs.y};
    }

    constexpr Vector2<S> operator/(const Vector2<S>& rhs) const {
        return Vector2<S>{x / rhs.x, y / rhs.y};
    }

    constexpr Vector2<S>& operator+=(const Vector2<S>& rhs) {
        x += rhs.x; y += rhs.y;
        return *this;
    }

    constexpr Vector2<S> operator-(const Vector2<S>& rhs) const {
        return Vector2<S>{x - rhs.x, y - rhs.y};
    }

    constexpr Vector2<S>& operator-=(const Vector2<S>& rhs) {
        x -= rhs.x;
        y -= rhs.y;
        return *this;
    }

    constexpr bool operator==(const Vector2<S>& rhs) const {
        return float_eq(x, rhs.x) && float_eq(y, rhs.y);
    }

    constexpr bool operator!=(const Vector2 &rhs) const {
        return !(rhs == *this);
    }
};

using Vector2f = Vector2<float>;
using Vector2d = Vector2<double>;
using Vector2i = Vector2<int>;

/// A 4D Vector
template<class S> struct Vector4 {
    union{ S x; S r; };
    union{ S y; S g; };
    union{ S z; S b; };
    union{ S w; S a; };
    Vector4(S x = 0, S y = 0, S z = 0, S w = 0): x(x), y(y), z(z), w(w) {}

    constexpr Vector2f xy() { return Vector2<S>(x, y); }
};

/// A 4D float vector.
using Vector4f = Vector4<float>;

/// Shorthand constructor for a Vector2
template<class T> constexpr Vector2<T> vec2(T x, T y) {
    return Vector2<T>{ x, y };
}

/// Shorthand constructor for a Vector4
template<class T> constexpr Vector4<T> vec4(T x = 0, T y = 0, T z = 0, T w = 0) {
    return Vector4<T>{ x, y, z, w };
}

template <class T> std::ostream& operator<<(std::ostream& os, const Vector2<T>& v)  {
    os << "(" << v.x << "," << v.y << ")";
    return os;
}

// ================================================================= //
//                            Matrix
// ================================================================= //

/// A 3x3 column major matrix
template<class T> struct Matrix3 {
    T m00 = 1, m01 = 0, m02 = 0;
    T m10 = 0, m11 = 1, m12 = 0;
    T m20 = 0, m21 = 0, m22 = 1;

    /// Calculate the determinant
    constexpr T determinant() const {
        return m00 * (m11 * m22 - m12 * m21) + m10 * (m21 * m02 - m01 * m22) + m20 * (m01 * m12 - m11 * m02);
    }

    /// Transpose the matrix
    constexpr Matrix3<T> transpose() const {
        return Matrix3{ m00, m10, m20, m01, m11, m21, m02, m12, m22 };
    }

    /// Get the matrix's inverse
    constexpr Matrix3<T> invert() const {
        if (determinant() == 0.0) throw std::logic_error("Matrix::invert on non-invertible matrix");
        T x = 1 / determinant();
        return Matrix3{
                x * (m11 * m22 - m12 * m21), x * (m21 * m02 - m01 * m22), x * (m01 * m12 - m11 * m02),
                x * (m20 * m12 - m10 * m22), x * (m00 * m22 - m20 * m02), x * (m10 * m02 - m00 * m12),
                x * (m10 * m21 - m20 * m11), x * (m20 * m01 - m00 * m21), x * (m00 * m11 - m10 * m01)
        };
    }

    constexpr Matrix3<T> operator*(const Matrix3<T>& a) const {
        auto b = *this;
        return Matrix3 {
                a.m00 * b.m00 + a.m01 * b.m10 + a.m02 * b.m20, a.m00 * b.m01 + a.m01 * b.m11 + a.m02 * b.m21, a.m00 * b.m02 + a.m01 * b.m12 + a.m02 * b.m22,
                a.m10 * b.m00 + a.m11 * b.m10 + a.m12 * b.m20, a.m10 * b.m01 + a.m11 * b.m11 + a.m12 * b.m21, a.m10 * b.m02 + a.m11 * b.m12 + a.m12 * b.m22,
                a.m20 * b.m00 + a.m21 * b.m10 + a.m22 * b.m20, a.m20 * b.m01 + a.m21 * b.m11 + a.m22 * b.m21, a.m20 * b.m02 + a.m21 * b.m12 + a.m22 * b.m22,
        };
    }

    constexpr Matrix3<T>&operator*=(const Matrix3<T>& rhs) {
        *this = *this * rhs;
        return *this;
    }

    constexpr Vector2<T> operator*(const Vector2<T>& v) const {
        return Vector2<T> {
                m00 * v.x + m10 * v.y + m20,
                m01 * v.x + m11 * v.y + m21,
        };
    }
};

typedef Matrix3<float> Matrix3f;

template <class T> std::ostream& operator<<(std::ostream& os, const Matrix3<T>& m)  {
    os << "|" << m.m00 << ", " << m.m01 << ", " << m.m02 << "|" << std::endl;
    os << "|" << m.m10 << ", " << m.m11 << ", " << m.m12 << "|" << std::endl;
    os << "|" << m.m20 << ", " << m.m21 << ", " << m.m22 << "|";
    return os;
}

// ================================================================= //
//                        Ranges & Bounds
// ================================================================= //

/// A range of values
template<class T> struct Range {
    T min, max;

    constexpr Range(T a = 0, T b = 0, bool sort = true) {
        if (!sort || a < b) { max = b; min = a; }
        else { max = a; min = b; }
    }

    /// Test if a value is contained in the range
    constexpr bool contains(const T& v) const {
        return v >= min && v <= max;
    }

    /// Test if this range contains another range
    constexpr bool contains(const Range<T>& r) const {
        return min <= r.min && max >= r.max;
    }

    /// Test if this range intersects another
    constexpr bool intersects(const Range<T>& other) const {
        return !(min > other.max || max < other.min);
    }

    /// Get the union of two ranges
    constexpr Range<T> combine(const Range<T>& other) const {
        return Range(std::min(min, other.min), std::max(max, other.max));
    }

    /// Expand the range by the given distance in both directions
    constexpr Range<T> expand(T distance) const {
        return Range(min - distance, max + distance);
    }

    /// Get the width of the range
    constexpr T width() const {
        return (max - min);
    }

    /// Get the point in the center of the range
    constexpr T center() const {
        return min + (width() / 2);
    }

    constexpr T clamp(T v) {
        return ::clamp(v, min, max);
    }

    // Relational operators
    constexpr bool operator==(const Range<T>& other) const {
        return min == other.min && max == other.max;
    }

    constexpr bool operator<(const Range &rhs) const { return min < rhs.min; }
    constexpr bool operator>(const Range &rhs) const { return rhs < *this; }
    constexpr bool operator<=(const Range &rhs) const { return !(rhs < *this); }
    constexpr bool operator>=(const Range &rhs) const { return !(*this < rhs); }

    constexpr Range<T> operator+(T distance) const {
        return Range(min + distance, max + distance);
    }

    constexpr Range<T> operator*(T scale) const {
        return Range(min * scale, max * scale);
    }

    constexpr Range<T> operator-(T distance) const {
        return Range(min - distance, max - distance);
    }

    friend std::ostream &operator<<(std::ostream &os, const Range &range1) {
        os << "[" << range1.min << " to " << range1.max << "]";
        return os;
    }

    /// An iterator over the values in the range, mostly useful for integer ranges,
    /// the minimum and maximum values are included.
    struct it_state {
        T value;

        void next(const Range<T>* ref) { value += 1; }
        void begin(const Range<T>* ref) { value = ref->min; }
        void end(const Range<T>* ref) { value = ref->max; }
        bool cmp(const it_state& s) const { return value <= s.value; }
        const T get(const Range<T>* ref) const { return value; }
    };
    SETUP_CONST_ITERATOR(Range<T>, T, it_state);
};

/// Shorthand constructor for a Range
constexpr const Range<float> range(float min, float max) {
    return Range<float>(min, max, false);
}

/// Map v from one range to another with linear interpolation
template<class T> constexpr T map(const T& v, const Range<T>& from, const Range<T>& to) {
    auto value = clamp(v, from.min, from.max);
    auto k = (value - from.min) / (from.max - from.min);
    return to.min + ((to.max - to.min) * k);
}

/// A 2D axis-aligned bounding box
template<class T> struct Bounds {
    Range<T> x, y;

    constexpr Bounds(Range<T> x = Range<T>(), Range<T> y = Range<T>()): x(x), y(y) {}

    /// Test if a point is contained in the bounds
    constexpr bool contains(const Vector2<T>& v) const {
        return x.contains(v.x) && y.contains(v.y);
    }

    /// Test if this AABB contains another
    constexpr bool contains(const Bounds<T>& other) const {
        return x.contains(other.x) && y.contains(other.y);
    }

    /// Test if this AABB intersects another
    constexpr bool intersects(const Bounds<T>& other) const {
        return !(x.min > other.x.max ||
                 x.max < other.x.min ||
                 y.min > other.y.max ||
                 y.max < other.y.min);
    }

    /// Get the union of two bounds
    constexpr Bounds<T> combine(const Bounds<T>& other) const {
        return Bounds(x.combine(other.x), y.combine(other.y));
    }

    /// Expand the bounds by the given distance in each direction
    constexpr Bounds<T> expand(T distance) const {
        return Bounds(x.expand(distance), y.expand(distance));
    }

    /// Get the perimeter of the bounds
    constexpr float perimeter() const {
        return 2.f * (x.width() + y.width());
    }

    /// Get the point in the center of the bounds
    constexpr Vector2<T> center() const {
        return vec2(x.center(), y.center());
    }

    constexpr Vector2<T> min() const { return vec2(x.min, y.min); }
    constexpr Vector2<T> max() const { return vec2(x.max, y.max); }
    constexpr Vector2<T> size() const { return vec2(x.width(), y.width()); }

    /// Get a point's position relative to the bounds
    constexpr Vector2<T> to_relative(Vector2<T> point) const {
        return vec2<T>(point.x - x.min, point.y - y.min);
    }

    /// Get the absolute position of a relative position
    constexpr Vector2<T> to_absolute(Vector2<T> point) const {
        return vec2<T>(point.x + x.min, point.y + y.min);
    }

    constexpr Vector2<T> lerp(Vector2<T> point) const {
        return point * size() + min();
    }

    /// Move the bounds
    constexpr Bounds<T> operator+(Vector2<T> distance) const {
        return Bounds(x + distance.x, y + distance.y);
    }
    constexpr Bounds<T> operator-(Vector2<T> distance) const {
        return Bounds(x - distance.x, y - distance.y);
    }
    constexpr Bounds<T> operator*(T scale) const {
        return Bounds(x * scale, y * scale);
    }

    constexpr bool operator==(const Bounds<T>& other) const {
        return x == other.x && y == other.y;
    }

    friend std::ostream &operator<<(std::ostream &os, const Bounds &bounds) {
        os << "x: " << bounds.x << " y: " << bounds.y;
        return os;
    }

    Vector2<T> clamp(Vector2<T> v) {
        return vec2(x.clamp(v.x), y.clamp(v.y));
    }
};


// ================================================================= //
//                         Transformations
// ================================================================= //

/// The identity matrix.
constexpr Matrix3f id() {
    return Matrix3f();
}

/// Create a translation matrix.
constexpr Matrix3f translate(float x, float y) {
    return Matrix3f{ 1.0, 0.0, 0.0,   0.0, 1.0, 0.0,   x, y, 1.0 };
}

constexpr Matrix3f translate(Vector2f t) {
    return Matrix3f{ 1.0, 0.0, 0.0,   0.0, 1.0, 0.0,   t.x, t.y, 1.0 };
}

/// Create a transformation matrix to flip along the x axes.
constexpr Matrix3f flip_x() {
    return Matrix3f { -1.f, 0.0, 0.0,   0.0, 1.0, 0.0,   0.0, 0.0, 1.0 };
}

/// Create a transformation matrix to flip along the y axes.
constexpr Matrix3f flip_y() {
    return Matrix3f { 1.f, 0.0, 0.0,   0.0, -1.f, 0.0,   0.0, 0.0, 1.0 };
}

/// Create a transformation matrix which scales.
constexpr Matrix3f scale(float scale) {
    return Matrix3f { scale, 0.0, 0.0,   0.0, scale, 0.0,   0.0, 0.0, 1.0 };
}

/// Create a transformation matrix which scales.
constexpr Matrix3f scale(float x_scale, float y_scale) {
    return Matrix3f { x_scale, 0.0, 0.0,   0.0, y_scale, 0.0,   0.0, 0.0, 1.0 };
}
constexpr Matrix3f scale(Vector2f scale) {
    return Matrix3f { scale.x, 0.0, 0.0,   0.0, scale.y, 0.0,   0.0, 0.0, 1.0 };
}

/// Create a transformation matrix which rotates by the given angle in degrees.
inline Matrix3f rotate(float angle) {
    float s = std::sin(deg2rad(angle));
    float c = std::cos(deg2rad(angle));
    return Matrix3f { c, s, 0, -s, c, 0, 0, 0, 1 };
}

/// Create a transformation matrix which rotates about a given point
inline Matrix3f rotate_about(float angle, Vector2f pos) {
    return translate(pos) * rotate(angle) * translate(-pos);
}

/// Create a view matrix
constexpr Matrix3f view(Bounds<float> bounds) {
    return flip_y()
           * translate(-1.f, -1.f)
           * scale(2.f / bounds.x.width(), 2.f / bounds.y.width())
           * translate(-bounds.x.min, -bounds.y.min);
}

/// A position, orientation, and scale in world-space.
struct Transform {
    Vector2f position = vec2(0.0f, 0.0f);
    float orientation = 0.0f;
    float scale = 1.0f;

    /// Get the interpolated transformation at some fractional time in ticks with
    /// the given velocity & angular velocity.
    constexpr Transform interpolate(float t, Vector2f velocity, float angular_velocity) const {
        return Transform { position + velocity * t, orientation + angular_velocity * t};
    }

    /// Combine two transformations. Scale is multiplied.
    constexpr Transform operator+(Transform const& rhs) const {
        // TODO: adjust position by scale & orientation
        return Transform { position + rhs.position, orientation + rhs.orientation, scale * rhs.scale };
    }

    /// Get the transformation matrix which applies this transform to a point.
    inline Matrix3f matrix() const {
        return ::translate(position) * ::rotate(orientation) * ::scale(scale);
    }
};
