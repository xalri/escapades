<?xml version="1.0" encoding="UTF-8"?>
<tileset name="sprites" tilewidth="1754" tileheight="1354" tilecount="6" columns="0">
 <grid orientation="orthogonal" width="1" height="1"/>
 <tile id="1">
  <image width="993" height="962" source="dragon.png"/>
  <objectgroup draworder="index">
   <object id="5" x="425" y="554" width="378" height="373">
    <ellipse/>
   </object>
  </objectgroup>
 </tile>
 <tile id="2">
  <properties>
   <property name="smooth" type="bool" value="true"/>
  </properties>
  <image width="129" height="129" source="raider.png"/>
  <objectgroup draworder="index">
   <object id="5" x="34.9404" y="14.8308" width="81.1925" height="78.6788">
    <ellipse/>
   </object>
  </objectgroup>
 </tile>
 <tile id="3">
  <image width="1754" height="1354" source="breath.png"/>
  <objectgroup draworder="index">
   <object id="1" x="877" y="642">
    <polygon points="0,0 818,-296 842,-246 866,-142 872,-46 872,40 864,130 850,210 826,288 786,378"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="4">
  <image width="200" height="50" source="arrow.png"/>
  <objectgroup draworder="index">
   <object id="2" x="195.541" y="28.6624">
    <polygon points="4.4586,0 -32.1656,-17.1975 -190.446,-21.0191 -190.127,14.3312 -30.8917,10.1911"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="5">
  <image width="1500" height="400" source="victory.png"/>
 </tile>
 <tile id="6">
  <image width="1500" height="400" source="failure.png"/>
 </tile>
</tileset>
