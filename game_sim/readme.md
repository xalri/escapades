Coursework for CSC3222 Gaming Simulations.

The engine used for this submission is shared with my submission for CSC3224 Games Development.
The collision detection and response algorithms in `engine/physics` and the pathfinding
algorithms in `engine/navigation` are to be marked for CSC3222, the rest of the engine
is to be marked for CSC3224.

The broad-phase is based on Box2D's dynamic bounding volume tree, the relevant
files contain the author's copyright notice and licence and are not intended
to be taken as my own work.
