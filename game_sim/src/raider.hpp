#pragma once

#include <random>
#include <backward.hpp>
#include <util/cwd.hpp>
#include <util/util.hpp>
#include <math/math.hpp>
#include <math/poly.hpp>
#include <storage/storage.hpp>
#include <physics/body.hpp>
#include <physics/collision.hpp>
#include <stage/object.hpp>
#include "const.hpp"
#include "dragon.hpp"
#include "nav.hpp"

const int ARROW_LIFETIME = (int) std::round(seconds_to_ticks(2.f));
const float ARROW_SPEED = feet_to_pixels(15.f) / seconds_to_ticks(1.f);

/// An arrow fired by a raider, deals damage to the dragon.
struct Arrow: Object {
    bool alive = true;
    int timer = ARROW_LIFETIME;
    int hit_wall = false;
    float damage = 10.f;

    explicit Arrow(Resources& res): Object(res, cwd() + "/resources/proto/arrow.json") {
        body->collision_mask = WALL | DRAGON;
        body->collision_id = ARROW;
    }

    // Called by Box<T> when we're added.
    void init_component(Weak<Arrow> self) {
        body->parent = self.any();
    }

    void update() {
        if (--timer < 1) { alive = false; }
        if (!alive) return;

        // Fade before de-spawning
        if (timer < 30) sprite->tint.a *= 1.0 - 0.02;

        if (hit_wall) {
            // After hitting a wall, arrows fade and slow
            body->velocity *= 1.0 - 0.01;
            body->t->scale -= 0.01;
        } else {
            // While in flight we simulate the arrow's 'height' with subtle changes to
            // the scale and speed.
            if (timer > ARROW_LIFETIME / 2) {
                body->t->scale += 0.008;
                body->velocity /= 1.0 - 0.007;
            } else {
                body->t->scale -= 0.008;
                body->velocity *= 1.0 - 0.007;
            }
        }
    }

    void resolve_collisions(BroadPhase &broad_phase, Storage &s) {
        if (!alive) return;
        //if (!body.valid()) std::abort();
        for (auto &c: broad_phase.collisions(body)) {

            if (c.body->collision_id == WALL) {
                auto resolution = c.contact.resolve(*body, *c.body);
                resolution.first.apply(*body);
                resolution.second.apply(*c.body);

                hit_wall = true;
                timer = std::min(timer, 100);
            }

            if (auto ref = c.body->parent.as<Dragon>()) {
                auto& dragon = **ref;
                dragon.take_damage(damage);
                alive = false;
                return;
            }
        }
    }
};

struct Raider : Object {
    struct ControlState {
        Vector2f point_to= vec2(1.f, 0.f);
        Vector2f move_to = vec2(0.f, 0.f);
        Vector2f target = vec2(0.f, 0.f);
    };

    ControlState control;
    float base_speed = feet_to_pixels(5.f) / seconds_to_ticks(1.f);
    float max_health = 200.0f;
    float base_damage = 10.f;
    float arrow_damage = 10.f;
    float health = max_health;
    bool alive = true;
    bool on_fire = false;
    bool in_rubble = false;
    bool at_entrance = false;
    bool at_pool = false;
    bool at_hoard = false;
    int shoot_cooldown = (int)seconds_to_ticks(6.0f);
    int cooldown_timer = 0;

    // Called by Box<T> when we're added.
    void init_component(Weak<Raider> self) {
        body->parent = self.any();
    }

    explicit Raider(Resources &res): Object(res, cwd() + "/resources/proto/raider.json") {
        body->collision_mask = WALL | BOULDER | RAIDER | ZONE | DRAGON | FIRE;
        body->collision_id = RAIDER;
        body->inv_inertia = 0.001;
        body->inv_mass = 0.025;
    }

    void update() {
        if (!alive) return;

        if (on_fire) {
            sprite->tint = vec4<float>(1.2, 0.7, 0.7, 1);
            health -= 50.0 / seconds_to_ticks(1.0);
            on_fire = false;
            if (health <= 0) alive = false;
        } else {
            sprite->tint = vec4<float>(1, 1, 1, 1);
        }

        float max_speed = base_speed;
        if (in_rubble) {
            max_speed /= 2;
            in_rubble = false;
        }

        if (cooldown_timer > 0) cooldown_timer--;

        if (control.point_to != vec2(0.f, 0.f)) body->t->orientation = control.point_to.angle();
        body->velocity = control.move_to.normalize() * max_speed;
        sprite->depth = 0.2f - body->t->position.y / 100000.f;
    }

    /// Resolve collisions.
    void resolve_collisions(BroadPhase &broad_phase, Storage &s) {
        if (!alive) return;
        at_entrance = false;
        at_pool = false;
        at_hoard = false;

        for (auto &c: broad_phase.collisions(body.weak())) {
            // Solid stuff
            auto resolve = (c.body->collision_id & (WALL | BOULDER | RAIDER | DRAGON)) != 0;
            if (resolve) {
                auto resolution = c.contact.resolve(*body, *c.body);
                resolution.first.apply(*body);
                resolution.second.apply(*c.body);
            }

            // Map zones
            if (auto map_obj = c.body->parent.as<MapObject>()) {
                // zones only take effect if we're > 10px into them.
                if (c.contact.intersection.t < -10) {
                    if ((**map_obj).type == "rubble") in_rubble = true;
                    if ((**map_obj).type == "entrance") at_entrance = true;
                    if ((**map_obj).type == "pool") at_pool = true;
                    if ((**map_obj).type == "hoard") at_hoard = true;
                }
            }

            // Fire
            if (c.body->collision_id & FIRE) on_fire = true;
        }
    }

    /// Attempt to fire an arrow, does nothing if the cooldown is active.
    std::optional<Ref<Arrow>> try_shoot(Resources& res) {
        if (cooldown_timer > 0) return {};
        cooldown_timer = shoot_cooldown;

        auto arrow = Arrow(res);
        arrow.transform->position = transform->position;
        arrow.transform->orientation = transform->orientation;
        arrow.transform->scale = 0.8;
        arrow.damage = arrow_damage;
        arrow.body->velocity = control.point_to.normalize() * ARROW_SPEED;

        return { res.storage.create(std::move(arrow)) };
    }
};

/// Follower AI
struct Follower {
    enum class FollowerState { Swarm, Escape, Hoard, Pool };

    Ref<Raider> raider;
    Weak<Transform> leader;
    Weak<Transform> dragon;
    std::shared_ptr<Navigation> nav;
    // A simple push-down automata for behaviour
    std::deque<FollowerState> state;

    /// Create a new follower. Takes references to the positions of the leader and the dragon.
    explicit Follower(Resources &res, Weak<Transform>&& leader, Weak<Transform>&& dragon):
            raider(res.storage.emplace<Raider>(res)),
            leader(leader),
            dragon(dragon)
    {
        state.push_back(FollowerState::Swarm);

        raider->max_health = 150;
        raider->health = std::uniform_int_distribution<int>(100, 150)(rng());
    }

    /// Update the follower
    void update() {
        if (state.empty()) return;
        if (state.back() == FollowerState::Swarm) swarm();
        else if (state.back() == FollowerState::Escape) escape();
        else if (state.back() == FollowerState::Hoard) to_hoard();
        else if (state.back() == FollowerState::Pool) to_pool();

		if (dragon.is_alive()) {
			// Point toward the dragon
			auto point_to = dragon->position - raider->transform->position;
			raider->control.point_to = point_to.normalize();
		}
    }

    void escape() {
        if (raider->at_entrance) raider->alive = false;
    }

    void to_hoard() {
        raider->body->inv_mass = 0.5;
		if (raider->at_hoard) {
            raider->arrow_damage = raider->base_damage * 2.f;
            state.pop_back();
            return;
        }
        raider->control.move_to = nav->hoard_dir.query(raider->transform->position).xy().inv_y();
    }

    void to_pool() {
        raider->body->inv_mass = 0.05;

        if (raider->at_pool) {
            raider->health = clamp(raider->health + 50.f, 0.f, raider->max_health);
            state.pop_back();
            return;
        }
        raider->control.move_to = nav->pool_dir.query(raider->transform->position).xy().inv_y();
    }

    /// Swarm around the leader, facing the dragon.
    void swarm() {
        // Make swarming followers light so the leader can push through them
        raider->body->inv_mass = 2.0;

        // If the leader is dead, try to escape.
        if (!leader.is_alive()) {
            state.back() = FollowerState::Escape;
            // Move in a random direction
            std::uniform_real_distribution<float> dist(-1, 1);
            raider->control.move_to = vec2(dist(rng()), dist(rng())).normalize();
            return;
        }

        // If the dragon is dead we can stop swarming.
        if (!dragon.is_alive()) {
            state.pop_back();
            return;
        }

        // Move to a position vaguely behind the leader
        auto behind_ldr = leader->position + rotate(leader->orientation) * vec2(-100.f, 0.f);
        auto to_leader = nav->target_dir.query(raider->transform->position).xy().inv_y() * 2.f
                + (behind_ldr - raider->transform->position).normalize();
        to_leader = to_leader.normalize();
        auto move_to = raider->control.move_to * SWARM_MOMENTUM + to_leader * SWARM_FOLLOW;
        raider->control.move_to = move_to.normalize();
    }
};
