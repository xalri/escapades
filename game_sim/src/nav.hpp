#pragma once

#include <resources/resources.hpp>
#include <field/field.hpp>
#include <field/navigation.hpp>

struct Navigation {
    field::Field<float, 1> cost_map; // The cost of moving across each tile
    field::PathDistanceField target_dist; // Global distance to the dragon's target
    field::Field<float, 2> target_dir; // Direction to move to get to the dragon's target

	field::PathDistanceField hoard_dist;
    field::Field<float, 2> hoard_dir;

	field::Field<float, 1> pool_cost_map;
	field::PathDistanceField pool_dist;
	field::Field<float, 2> pool_dir;

    Navigation(Resources& res, Bounds<float> bounds, float coarseness):
            cost_map(res, bounds, coarseness),
            target_dist(res, bounds, coarseness),
            target_dir(res, bounds, coarseness / 2.f),
            hoard_dist(res, bounds, coarseness),
            hoard_dir(res, bounds, coarseness / 2.f),
			pool_cost_map(res, bounds, coarseness),
            pool_dist(res, bounds, coarseness),
            pool_dir(res, bounds, coarseness / 2.f)
			{}
};
