#define BACKWARD_HAS_BFD 1
#include <backward.hpp>
#include <util/time.hpp>
#include <window/window.hpp>
#include "const.hpp"
#include "scene.hpp"

// Catches signals (segfaults etc) and prints a stack trace.
backward::SignalHandling sh;

int main() {
    auto window = Window("Dragon's Grave", 1280, 720);
    //window.set_vsync(1);

	Scene scene{};
    auto timer = StepTimer(seconds_to_ns(ticks_to_seconds(1) / SPEED_SCALE));

    // A single-threaded fixed time-step game loop.
    while (!window.should_close()) {
        window.poll_events();
        for(auto &ev : window.events) {
            if (ev.kind == WindowEvKind::KeyPress && ev.key.code == GLFW_KEY_X) window.close();
            scene.on_window_ev(ev);
        }
        window.events.clear();

        timer.tick([&](){ scene.update(window); });

        window.bind();
        gl::DepthMask(true);
        gl::Clear().Color().Depth();
        scene.draw();
        window.display();
    }
}
