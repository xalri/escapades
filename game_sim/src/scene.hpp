#pragma once

#include <backward.hpp>
#include <util/cwd.hpp>
#include <util/time.hpp>
#include <util/parallel.hpp>
#include <util/util.hpp>
#include <math/math.hpp>
#include <math/poly.hpp>
#include <field/field.hpp>
#include <field/navigation.hpp>
#include <field/draw.hpp>
#include <field/region.hpp>
#include <field/sobel.hpp>
#include <field/blur.hpp>
#include <storage/storage.hpp>
#include <physics/body.hpp>
#include <physics/collision.hpp>
#include <window/window.hpp>
#include <render/debug.hpp>
#include <render/sprite.hpp>
#include <render/queue.hpp>
#include <resources/resources.hpp>
#include <stage/stage.hpp>
#include <stage/tmx.hpp>
#include "const.hpp"
#include "nav.hpp"
#include "dragon.hpp"
#include "raider.hpp"

struct Scene {
    Bounds<float> view_bounds = Bounds<float>(range(0.f, 1280.f), range(0.0, 720.f));
    Bounds<float> world_bounds = Bounds<float>(range(0.f, 128.f * 16.f), range(0.0, 128.f * 10.f));

    Storage s{}; // Contiguous component & resource storage.
    Resources res{s}; // Loads & caches images, maps, object prototypes, etc.
    BroadPhase broad_phase{}; // Collision broad-phase.
    DrawQueue draw_queue{}; // A queue of draw calls ordered by depth.
    DebugDraw debug{res}; // Draws body geometry & debug text
    std::shared_ptr<Navigation> nav = std::make_shared<Navigation>(res, world_bounds, 8); // Navigation state
    bool exit = false;
    bool won = false;
    bool dead = false;
	bool start = true;
	long tick = 0;

    // Things which make up the game world:
    Ref<Stage> map{}; // Contains map geometry & the background.
    Ref<Dragon> dragon{};
    Ref<Raider> leader{};
    std::vector<Ref<Follower>> followers{};
    std::vector<Ref<Arrow>> arrows{};

    Object victory{res, cwd() + "/resources/proto/victory.json"};
    Object failure{res, cwd() + "/resources/proto/failure.json"};

    int overlay = 0; // debug overlay state

    Scene() {
        // Load the map from a tmx file.
        // Contains the background image along with geometry for the cave walls, boulders, and zones.
        map = res.get<Stage>(cwd() + "/resources/maps/dragons_grave.tmx");

		// Victory/failure messages are objects, for reasons.
		// Make them invisible and move off-screen.
		auto off_screen = vec2(-2000.f, -2000.f);
		victory.sprite->visible = false;
		failure.sprite->visible = false;
		victory.transform->position = off_screen;
		failure.transform->position = off_screen;

        // Build the navigation cost map
        nav->cost_map.clear(1.f);
        for(auto& obj : map->objects) {
            if (obj->type == "wall" || obj->type == "boulder")
                field::draw_body(res, debug.polys, nav->cost_map, *obj->body, vec4(100.f), 23.f);
            if (obj->type == "rubble")
                field::draw_body(res, debug.polys, nav->cost_map, *obj->body, vec4(2.5f));
            if (obj->type == "hoard") field::draw_body(res, debug.polys, nav->hoard_dist.distance, *obj->body, vec4(0.f), -10.0);
        }

		// Setup the navigation field for the hoard (the cost map is static, so we only need to calculate this once.)
        nav->hoard_dist.update(nav->cost_map, 50);
        field::sobel_filter(res, nav->hoard_dist.distance, nav->hoard_dir);
    }

    void spawn() {
        // Do nothing if we already have a leader.
        if(!won && leader.valid()) return;

        won = false;
        dead = false;
		start = false;
		arrows.clear();

        // Create the leader
        leader = s.emplace<Raider>(res);
        leader->transform->position = vec2(1170.f, 1140.f);
        leader->control.point_to = vec2(-0.2f, -1.f);

        // Create the dragon
        dragon = s.emplace<Dragon>(res);
        dragon->transform->position = vec2(1400.f, 600.f);

        // Spawn followers
        //followers.clear();
        followers.resize(0);
        for(auto i = 0; i < 20; i++) {
            followers.push_back(s.emplace<Follower>(res, leader->transform.weak(), dragon->transform.weak()));
            followers.back()->raider->transform->position = vec2(1100.f + (i % 7) * 30.f, 1200.f + (i / 7) * 30.f);
            followers.back()->nav = nav;
        }
    }

    void on_window_ev(WindowEv &ev) {
        if (ev.kind == WindowEvKind::KeyPress) {
            if (ev.key.code == GLFW_KEY_K) spawn();
            else if (ev.key.code == GLFW_KEY_J) {
                if (start || won || dead) return;
                auto idx = std::uniform_int_distribution<int>(0, followers.size() - 1)(rng());
                followers[idx]->state.push_back(Follower::FollowerState::Hoard);
            }
            else if (ev.key.code == GLFW_KEY_L) {
                if (start || won || dead) return;
                Ref<Follower> ref{};
                auto min = 100000.f;
                for (auto &f : followers) {
                    if (f->state.back() != Follower::FollowerState::Pool && f->raider->health < min) {
                        min = f->raider->health;
                        ref = f.clone();
                    }
                }
                if (ref.valid()) ref->state.push_back(Follower::FollowerState::Pool);
            }
            else if (ev.key.code == GLFW_KEY_X) exit = true;
            else if (ev.key.code == GLFW_KEY_0) overlay = 0;
            else if (ev.key.code == GLFW_KEY_1) overlay = 1;
            else if (ev.key.code == GLFW_KEY_2) overlay = 2;
            else if (ev.key.code == GLFW_KEY_3) overlay = 3;
            else if (ev.key.code == GLFW_KEY_4) overlay = 4;
            else if (ev.key.code == GLFW_KEY_5) overlay = 5;
            else if (ev.key.code == GLFW_KEY_6) overlay = 6;
        }
    }

    void update(Window &window) {
        tick += 1;
        if(exit) {
            window.close();
            return;
        }

        if (won || dead) return;

        if (dragon.valid() && tick % 4 == 2) {
            nav->pool_cost_map.clear(1.f);
            nav->pool_dist.clear();

            gl::BlendEquation(gl::BlendEquation::kMax);

            for(auto& obj : map->objects) {
                gl::Enable(gl::kBlend);
                if (obj->type == "wall" || obj->type == "boulder")
                    field::draw_body(res, debug.polys, nav->pool_cost_map, *obj->body, vec4(100.f), 23.f);
                if (obj->type == "rubble")
                    field::draw_body(res, debug.polys, nav->pool_cost_map, *obj->body, vec4(2.5f));
                if (obj->type == "pool") {
                    gl::Disable(gl::kBlend);
                    field::draw_body(res, debug.polys, nav->pool_dist.distance, *obj->body, vec4(0.f), 10.f);
                }
            }

            field::draw_body_density(res, debug.polys, nav->pool_cost_map, *dragon->neighbourhood, 1.0f, 50.f);
            //field::draw_body(res, debug.polys, nav->pool_cost_map, *dragon->neighbourhood, vec4(10.f), 20.f);

            nav->pool_dist.update(nav->pool_cost_map, 30);
            field::sobel_filter(res, nav->pool_dist.distance, nav->pool_dir);
        }

        // Dragon navigation:
        if(dragon.valid() && (leader.valid() || !followers.empty())) {

            if (tick % 4 == 1) {
                // Find the dragon's target
                Ref<Body> target;
                if (leader.valid()) {
                    target = leader->body.clone();
                } else {
                    auto min = 1000000000000.f;
                    // Find the closest follower
                    for (auto &ref : followers) {
                        auto dist_sq = (ref->raider->transform->position - dragon->transform->position).length_sq();
                        if (dist_sq < min) {
                            target = ref->raider->body.clone();
                            min = dist_sq;
                        }
                    }
                }

                // Draw the target to initialize the `target_dist` global distance field (the target has a global
                // distance of 0 to the target)
                nav->target_dist.clear();
                field::draw_body(res, debug.polys, nav->target_dist.distance, *target, vec4(10.f), 16.0);

                // Update the global distance field based on the cost map, uses a modification of the fast marching
                // method (https://en.wikipedia.org/wiki/Fast_marching_method) for the GPU.
                // The method is iterative, here we run 20 iterations.
                // The result is the time taken to reach the target from every point in the map.
                nav->target_dist.update(nav->cost_map, 20);

                // Approximate the gradient of the global distance field with a sobel filter -- gives the direction
                // to move from every point to reach the target.
                field::sobel_filter(res, nav->target_dist.distance, nav->target_dir);
            }

            // Update the dragon (ideally we would wait as long as possible to do this, the above calls are asynchronous,
            // querying blocks on the GPU finishing)
            auto dir = nav->target_dir.query(dragon->body->t->position).xy().normalize();
            dir.y = -dir.y; // y is inverted in fields (they're textures, so the origin is the top left)
            dragon->move_to = dragon->transform->position + dir;
        }

        // Player input
        if (leader.valid()) {
            if (window.mouse_pressed[GLFW_MOUSE_BUTTON_LEFT]) {
                leader->control.point_to = (vec2(window.mouse.x, window.mouse.y)
                                            - vec2(window.size.x / 2.0, window.size.y / 2.0)).normalize().cast<float>();
                leader->control.move_to = leader->control.point_to;
            } else {
                leader->control.move_to = vec2(0.f, 0.f);
            }
        }
        if (window.key_pressed[GLFW_KEY_SPACE]) {
            for (auto& ref : followers) {
                if (auto a = ref->raider->try_shoot(res)) arrows.push_back(a->clone());
            }
        }

        // Update stuff
        for(auto& dragon : s.box<Dragon>()) dragon.update();
        for(auto& follower : s.box<Follower>()) follower.update();
        for(auto& raider : s.box<Raider>()) raider.update();
        for(auto& arrow : s.box<Arrow>()) arrow.update();
        for(auto& body : s.box<Body>()) body.integrate();

        // Iterative collision solving.
        for (auto i = 0; i < 1; i++) {
            broad_phase.find_collisions(s.box<Body>());

            for (auto &raider : s.box<Raider>()) raider.resolve_collisions(broad_phase, s);
            for (auto &arrow : s.box<Arrow>()) arrow.resolve_collisions(broad_phase, s);
            for (auto &dragon : s.box<Dragon>()) dragon.resolve_collisions(broad_phase, s);
        }

        // Remove dead stuff
        swap_filter(followers, [&](auto& ref) { return ref->raider->alive; } );
        swap_filter(arrows, [&](auto& arrow) { return arrow->alive; } );
        if (leader.valid() && !leader->alive) leader = {};
        if (dragon.valid() && !dragon->alive) dragon = {};

		if (!start && !leader.valid() && followers.empty()) dead = true;
		else if (!start && !dragon.valid()) won = true;
    }

    void draw() {
        auto view_bounds = this->view_bounds;

		if (won || dead) {
			view_bounds = view_bounds * 1.5f + (vec2(-2000.f, -2000.f) - (view_bounds).center() * 1.5f);
			victory.sprite->visible = won;
			failure.sprite->visible = dead;
		} else {
			if(leader.valid()) view_bounds = view_bounds * 1.2 + leader->transform->position - (view_bounds * 1.2).center();
			else view_bounds = view_bounds * 1.72;
		}

        auto view = ::view(view_bounds);

        // Draw all sprites.
        for (auto& sprite : s.box<Sprite>())
            draw_queue.add(sprite.depth, [&](){ sprite.draw(view); });

        // Draw the geometry of the bodies, for debugging.
        for (auto body : s.box<Body>().refs())
            draw_queue.add(0.0, [&, body](){ debug.draw_body(view, body); });

		gl::BlendEquation(gl::BlendEquation::kFuncAdd);
		gl::Enable(gl::kBlend);
        draw_queue.run();

        // Draw the overlay
        switch (overlay) {
            case 1: field::draw(res, nav->cost_map, view, range(0.f, 4.f), false); break;
            case 2: field::draw(res, nav->target_dist.distance, view, range(0.f, 150.f), true); break;
            case 3: field::draw(res, nav->target_dir, view, range(-1.f, 1.f), false); break;
            case 4: field::draw(res, nav->hoard_dist.distance, view, range(0.f, 250.f), true); break;
            case 5: field::draw(res, nav->pool_cost_map, view, range(0.f, 150.f), true); break;
            case 6: field::draw(res, nav->pool_dist.distance, view, range(0.f, 150.f), true); break;
            default: break;
        }
    }
};
