#pragma once

#include <render/sprite.hpp>
#include <window/window.hpp>
#include <physics/collision.hpp>
#include <stage/object.hpp>
#include <backward.hpp>
#include "const.hpp"

struct Dragon: Object {
    Object breath;
    float base_speed = feet_to_pixels(3.f) / seconds_to_ticks(1.f);
    float max_speed = base_speed;
    bool breath_active = false;
    bool breath_in = false;
    bool alive = true;
    float max_health = 1000.0;
    float health = max_health;
    Ref<Body> neighbourhood;
    std::optional<Vector2f> target{};
    std::optional<Vector2f> move_to{};

	Dragon() = delete;

    explicit Dragon(Resources& res):
            Object(res, cwd() + "/resources/proto/dragon.json"),
            breath(res, cwd() + "/resources/proto/dragon_breath.json")
    {
        body->collision_id = DRAGON;
        body->collision_mask = WALL | BOULDER;
        body->inv_inertia = 0.0;

        breath.body->collision_id = FIRE;
        breath.sprite->tint.a = 0.7;
        breath.transform->scale = 0.0001;

        auto range = feet_to_pixels(20.0);
        neighbourhood = res.storage.create(Body::immovable(
                res.storage.create(Poly::ellipse(20, range * 2, range * 2)),
                res.storage.emplace<Material>(),
                body->t.clone()));
        neighbourhood->is_static = false;
        neighbourhood->collision_mask = RAIDER;
    }

    // Called by Box<T> when we're added.
    void init_component(Weak<Dragon> self) {
        body->parent = self.any();
    }

    void update() {
        if (!alive) return;
		if (!transform.valid()) std::abort();

        auto& breath_t = *breath.transform;
        body->velocity *= 0.4;
        sprite->depth = 0.2f - transform->position.y / 100000.f;

        if(target.has_value() || breath_active) {
            if(!target.has_value()) breath_in = true;
            breath_t.scale += breath_in ? -0.01 : 0.01;
            if(breath_t.scale < 0.0001 || breath_t.scale > 1.6) {
                if(breath_in && !target.has_value()) breath_active = false;
                breath_in = !breath_in;
            }
        }

        breath_t.position = transform->position - vec2(25.f, 48.f);

        if (target.has_value()) {
            breath_t.orientation = (*target - breath_t.position).angle();
        }

        body->velocity = (move_to.has_value()) ?
            body->velocity = (*move_to - transform->position).normalize() * max_speed :
            body->velocity = vec2(0.f, 0.f);
    }

    void take_damage(float damage) {
        health -= damage;
        if (health <= 0.0) {
            alive = false;
            health = 0;
        }
    }

    void resolve_collisions(BroadPhase &broad_phase, Storage& s) {
        if (!alive) return;

        for (auto &c: broad_phase.collisions(body)) {
            auto resolution = c.contact.resolve(*body, *c.body);
            resolution.first.apply(s[body]);
            resolution.second.apply(s[c.body]);
        }

        target = {};
        auto max = 0.0;
        for (auto &c: broad_phase.collisions(neighbourhood)) {
            auto distance = -c.contact.intersection.t;
            if(distance > max) {
                max = distance;
                target = {c.contact.contact_b};
                breath_active = true;
            }
        }
    }
};