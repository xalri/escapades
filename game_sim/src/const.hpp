#pragma once
#include <cstdint>

constexpr float TICKS_PER_SECOND = 60.0;
constexpr float SPEED_SCALE = 1.0;

/// Convert a value in feet to pixels (the unit we use for world-space)
constexpr float feet_to_pixels(float feet) noexcept { return feet * 32.f / 5.f; }

/// Convert some number of seconds to frames.
constexpr float seconds_to_ticks(float s) noexcept { return s * TICKS_PER_SECOND; }

/// Convert a number of frames to seconds.
constexpr float ticks_to_seconds(float s) noexcept { return s / TICKS_PER_SECOND; }

// Collision IDs, used for masking collisions in the broad-phase
const uint32_t WALL = 0b00000001;
const uint32_t BOULDER = 0b00000010;
const uint32_t ZONE = 0b00000100;
const uint32_t RAIDER = 0b00001000;
const uint32_t DRAGON = 0b00010000;
const uint32_t FIRE = 0b00100000;
const uint32_t ARROW = 0b01000000;

// Influence ratios for swarming behaviour.
const uint32_t SWARM_MOMENTUM = 20;
const uint32_t SWARM_FOLLOW = 5;
