To build and run the game on linux:

```
> git clone --recursive https://gitlab.com/xalri/escapades.git
> cd escapades
> mkdir build; cd build
> cmake ..
> make game
> (cd ../game; ../build/game/game)
```

A C++ game framework, written as coursework for game development & game simulations
modules.

Interesting features include:
 - A [component graph system](https://github.com/kvark/froggy/wiki/Component-Graph-System)
   for managing entities.
 - Custom impulse based collision system
 - GPU flow-field pathfinding

## Notes on things which don't exist yet

 - Finish data formats for entity definitions, animations, etc..
 - Integrating a UI library for in-game menus, maybe nuklear?
 - side-on platformer lighting? ambient given by 'distance' into ground, but propagating normals?
 - some sort of normal map of the surfaces of the map's geometry. maybe also for sprites,
   just draw the entire scene to multiple targets (diffuse + normal)
 - then use magic (e.g. jump-flooding the eikonal eq) to calculate AO, can be
   a one-time calculation.
 - render lighting to a framebuffer, then compose with diffuse
 - blending normals for multiple lights:
   - http://blog.selfshadow.com/publications/blending-in-detail/
   - avoids a render pass per light, but the approximation might not be very good..
   - so, have a colour & normal layer for lighting, and a diffuse & normal layer for scene
      - per layer, or global?...
      - maybe per layer? idk, performance etc, maybe try..
   - blend with a full screen quad.
  - anyway, lighting with normals pretty much requires modeling sprites & tiles in 3D
  - (hand drawn normal maps would be slow..)
  - Sprites & tile maps:
  - model & animate in 3D, render to spritesheet with normal maps?
  - no textures in 3D model, just colour
