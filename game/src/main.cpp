#define BACKWARD_HAS_BFD 1
#include <backward.hpp>
#include <util/time.hpp>
#include <window/window.hpp>
#include "game.hpp"

// Catches signals (segfaults etc) and prints a stack trace.
backward::SignalHandling sh;

int main() {
    auto window = Window{ "Game", 1280, 720 };
    auto game = Game{};
    auto timer = StepTimer{};

    //window.set_vsync(0);

    // A single-threaded fixed time-step game loop.
    while (!window.should_close()) {
        // Forward window events
        window.poll_events();
        while(auto ev = window.next_event()) {
            game.evs->emplace_back(EvKind::Window);
            game.evs->back().window_ev = *ev;
        }

        // Update the game each tick
        // (may do nothing -- the timer is independent from the draw loop)
        timer.tick([&](){ game.update(window); });
        //game.update(window);

        // Draw stuff.
        window.bind();
        game.draw();
        window.display();
    }
}

