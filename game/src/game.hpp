#pragma once

#include <storage/storage.hpp>
#include <util/cwd.hpp>
#include <window/window.hpp>
#include <resources/resources.hpp>
#include <stage/stage.hpp>
#include <stage/tmx.hpp>
#include <render/gl.hpp>
#include <render/shader.hpp>
#include "ev.hpp"
#include "level.hpp"
#include "ui.hpp"

struct Game {
    /// An event queue, contains window events (input, etc) and game events.
    Evs evs = std::make_shared<std::deque<Ev>>();

    /// Contiguous component storage for active entities
    Storage s{};
    /// Component storage for prototypes
    Storage proto_s{};
    /// Resource manager
    Resources res{ s };
    /// Current level prototype
    Stage map_proto = load_map(res, cwd() + "/resources/maps/tutorial.tmx", proto_s);
    //Stage map_proto = load_map(res, cwd() + "/resources/maps/level01.tmx", proto_s);
    /// World state for the current level
    Level level{ s, res, map_proto.instance(s), evs };
    /// UI state
    UI ui{};

    bool dead = false;
    bool level_complete = false;
    bool paused = false;
    int pause_counter = 0;
    std::string next_map_path{};
    Ref<gl::Program> clear_program;
    gl::VertexArray vao{}; // Empty VAO for screen-clear shader
    Vector4f screen_tint = BACKGROUND_COL; // An overlay drawn between the game and UI

    Game() {
        clear_program = res.get<gl::Program>("game/clear_program", fill_screen_src, 0.f);
        screen_tint.a = 0.f;
    }

    /// Update the state of the game.
    void update(Window& window) {
        // Handle events
        while(!evs->empty()) {
            on_ev(window, evs->front());
            evs->pop_front();
        }

        // Update the level
        if (!paused) level.update(window);

        if (pause_counter > 0 && --pause_counter == 0) paused = true;
        if (dead) {
            screen_tint.a = (float)(120 - pause_counter) / 120.f;
            ui.respawn->sprite->tint.a = (float)(120 - pause_counter) / 30.f;
        }
        if (level_complete) {
            screen_tint.a = clamp((float)(120 - pause_counter) / 30.f, 0.f, 0.5f);
            ui.level_complete->sprite->tint.a = (float)(120 - pause_counter) / 30.f;
        }
    }

    /// Switch to the map specified by `this->next_map_path`.
    void next_map() {
        map_proto = load_map(res, cwd() + next_map_path, proto_s);
        level_complete = false;
        paused = false;
        pause_counter = 0;
        screen_tint.a = 0.f;
        ui.level_complete = {};
        reset_map();
    }

    /// Reset the world-state based on the current map prototype.
    /// Doesn't effect checkpoints, etc.
    void reset_map() {
        level.reset(map_proto.instance(s));
    }

    /// Handle an event
    void on_ev(Window& window, Ev& ev) {
        if (ev.kind == EvKind::Window) {
            if (ev.window_ev.kind == WindowEvKind::KeyPress) {
                if (ev.window_ev.key.code == GLFW_KEY_SPACE) {
                    // If the level complete prompt is showing, move to the next map.
                    if (level_complete) next_map();
                } else if (ev.window_ev.key.code == GLFW_KEY_R) {
                    evs->emplace_back(EvKind::Respawn);
                }
            }
        } else if (ev.kind == EvKind::Respawn) {
            // Clear any UI prompts and un-pause
            // (can restart at any time, in game or during a prompt)
            dead = false;
            paused = false;
            pause_counter = 0;
            screen_tint.a = 0.f;
            level_complete = false;
            ui.respawn = {};
            ui.level_complete = {};
            // Reset the level
            reset_map();
        } else if (ev.kind == EvKind::Died) {
            if (!level_complete && !dead) {
                // Show the respawn prompt
                dead = true;
                pause_counter = 120;
                ui.respawn = ui.s.emplace<Object>(ui.s, *ui.p_respawn);
            }
        } else if (ev.kind == EvKind::LevelComplete) {
            if (!level_complete) {
                // Show the level complete prompt
                level.ball->alive = false;
                level.prev_checkpoint = {};
                level_complete = true;
                pause_counter = 120;
                next_map_path = ev.next_level;
                ui.level_complete = ui.s.emplace<Object>(ui.s, *ui.p_level_complete);
            }
        }

        // Forward events to the level & UI
        level.on_ev(window, ev);
        ui.on_ev(window, ev);
    }

    /// Draw stuff
    void draw() {
        gl::ClearColor(BACKGROUND_COL.r, BACKGROUND_COL.g, BACKGROUND_COL.b, BACKGROUND_COL.a);
        gl::DepthMask(true);
        gl::Clear().Color().Depth();

        // Draw the level
        if (screen_tint.a < 0.99f) {
            auto bg = level.stage.bg_colour;
            fill_screen(vec4(bg.r / 255.f, bg.g / 255.f, bg.b / 255.f, bg.a / 255.f));
            level.draw();
        }

        // Draw the UI
        if (screen_tint.a > 0.f) fill_screen(screen_tint);
        ui.draw();
    }

    /// Draw an overlay colour
    void fill_screen(Vector4f col) {
        gl::DepthMask(false);
        gl::Enable(gl::kBlend);
        gl::Use(*clear_program);
        gl::Uniform<Vector4f>(*clear_program, "col").set(col);
        gl::Bind(vao);
        gl::DrawArrays(gl::PrimitiveType::kTriangles, 0, 3);
        gl::Unbind(vao);
        gl::DepthMask(true);
    }
};