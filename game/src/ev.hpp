#pragma once

#include <memory>
#include <deque>
#include <variant>
#include <window/window.hpp>
#include <storage/storage.hpp>
#include <stage/stage.hpp>

/// An event kind
enum class EvKind {
    Window,
    Respawn,
    Checkpoint,
    LevelComplete,
    Died,
};

/// A game event.
struct Ev {
    EvKind kind;
    union {
        std::string next_level;
        WindowEv window_ev;
        Ref<MapObject> checkpoint;
    };

    /// Create a new event with a given kind. The relevant union field _must_ be set before use.
    explicit Ev(EvKind kind) {
        // Initialise to 0 -- equiv to moved-from state for e.g. refs & strings,
        // so assigning to them will hopefully skip their destructors..
        memset(this, 0, sizeof(Ev));
        this->kind = kind;
    }

    /// Clears the non-trivial data in the union based on the tag.
    /// TODO: std::variant exists but gave strange compiler errors, should try again some time..
    ~Ev() {
        if (kind == EvKind::LevelComplete) (&next_level)->~basic_string();
        else if (kind == EvKind::Checkpoint) (&checkpoint)->~Ref();
    }
};

/// A shared deque of events
using Evs = std::shared_ptr<std::deque<Ev>>;
