#pragma once

#include <storage/storage.hpp>
#include <resources/resources.hpp>
#include <util/cwd.hpp>

const Vector4f BACKGROUND_COL = vec4(0.1f, 0.1f, 0.1f, 1.0f);

struct UI {
    Bounds<float> bounds = Bounds<float>(range(-1.f, 1.f), range(-1.f, 1.f));
    Matrix3f view = ::view(bounds);
    Storage s{};
    Resources res{ s };
    DrawQueue draw_queue{};

    Ref<Prototype> p_respawn{};
    Ref<Object> respawn{};
    Ref<Prototype> p_level_complete{};
    Ref<Object> level_complete{};

    UI():
        p_respawn(res.get<Prototype>(cwd() + "/resources/proto/ui/respawn.json")),
        respawn{},
        p_level_complete(res.get<Prototype>(cwd() + "/resources/proto/ui/level_complete.json")),
        level_complete{}
    {}

    void on_ev(Window& window, Ev& ev) {
        // ...
    }

    void draw() {
        for (auto sprite : s.box<Sprite>().refs()) {
            //sprite->update_anim(16);
            draw_queue.add(sprite->depth, [&, sprite](){ sprite->draw(view); });
        }

        draw_queue.run();
    }
};
