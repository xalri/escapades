#pragma once

#include <storage/storage.hpp>
#include <util/cwd.hpp>
#include <math/math.hpp>
#include <physics/collision.hpp>
#include <window/window.hpp>
#include <resources/resources.hpp>
#include <stage/object.hpp>

/// The player controlled ball
class Ball: public Object {
public:
    Evs evs;
    bool alive = true;

    explicit Ball(Resources& res, Evs evs): Object(res, cwd() + "/resources/proto/ball.json"), evs(evs) {
        body->collision_mask |= 0b11;
        body->collision_id |= 0b10;
    }

    /// Resolve collisions.
    void resolve_collisions(BroadPhase &broad_phase, Storage &s) {
        for (auto &c: broad_phase.collisions(body.weak())) {
            // Solid stuff
            if (c.body->collision_id == 1) {
                auto resolution = c.contact.resolve(*body, *c.body);
                resolution.first.apply(*body);
                resolution.second.apply(*c.body);
            }

            // Zones
            if (alive && c.body->collision_id == 2) {
                if (auto obj = c.body->parent.as<MapObject>()) {
                    if ((*obj)->type == "kill") {
                        evs->emplace_back(EvKind::Died);
                        alive = false;
                        return;
                    } else if ((*obj)->type == "checkpoint") {
                        evs->emplace_back(EvKind::Checkpoint);
                        evs->back().checkpoint = obj->upgrade();
                    } else if ((*obj)->type == "end") {
                        evs->emplace_back(EvKind::LevelComplete);
                        evs->back().next_level = (*obj)->properties.get("next_map")->string_value;
                    }
                }
            }
        }
    }

    void input(Window const& window) {
        if (!alive) return;
        if (window.key_pressed[GLFW_KEY_A]) {
            body->velocity.x -= 0.05f;
            body->angular_vel -= 0.2f;
        }
        if (window.key_pressed[GLFW_KEY_D]) {
            body->velocity.x += 0.05f;
            body->angular_vel += 0.2f;
        }
    }

    void update() {
        if (body->velocity.length_sq() > 80.f) body->velocity /= 1 + ((body->velocity.length_sq() - 80.f) / 200.f);
        if (!alive) {
            body->velocity = vec2(0.f, 0.f);
            if (sprite->tint.a > 0.0) sprite->tint.a -= 0.02;
        }
    }
};

