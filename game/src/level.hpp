#pragma once

#include <storage/storage.hpp>
#include <math/math.hpp>
#include <math/poly.hpp>
#include <physics/body.hpp>
#include <physics/collision.hpp>
#include <window/window.hpp>
#include <render/debug.hpp>
#include <render/queue.hpp>
#include <resources/resources.hpp>
#include <stage/stage.hpp>
#include <stage/tmx.hpp>
#include <stage/object.hpp>
#include "ev.hpp"
#include "grenade.hpp"
#include "ball.hpp"

struct Level {
    Storage& s;
    Resources& res;
    Evs evs; // Global event queue

    // Things which make up the world:
    Stage stage; // Map objects & geometry
    Ref<Ball> ball{};
    std::vector<Ref<Grenade>> grenades{};

    // Other world-related state:
    Vector2f spawn_point{};
    Ref<MapObject> prev_checkpoint{};
    BroadPhase broad_phase{};
    int cooldown = 0;

    // Rendering stuff
    DrawQueue draw_queue{};
    DebugDraw debug{ res };
    Bounds<float> view_bounds = Bounds<float>(range(0.f, 1280.f / 1.4f), range(0.f, 720.f / 1.4f));
    Bounds<float> world_bounds = view_bounds;
    Matrix3f view = ::view(world_bounds);
    bool debug_on = true;

    Level(Storage& s, Resources& res, Stage stage, Evs evs) : s(s), res(res), evs(evs) {
        reset(std::move(stage));
    }

    void reset(Stage _stage) {
        stage = std::move(_stage);
        // Find a spawn point
        if (!prev_checkpoint.valid()) {
            for (auto &obj : stage.objects) {
                if (obj->type == "spawn") spawn_point = obj->body->t->position;
            }
        }
        prev_checkpoint = Ref<MapObject>{};
        cooldown = 0;
        grenades.clear();
        ball = s.emplace<Ball>(res, evs);
        ball->body->t->position = spawn_point;
    }

    void on_ev(Window& w, Ev &ev) {
        if (ev.kind == EvKind::Window) {
            on_window_ev(w, ev.window_ev);
        } else if (ev.kind == EvKind::Checkpoint) {
            if (prev_checkpoint.valid()) prev_checkpoint->sprite->set_frame(0);
            prev_checkpoint = ev.checkpoint;
            spawn_point = ev.checkpoint->body->t->position;
            ev.checkpoint->sprite->set_frame(1);
        }
    }

    void on_window_ev(Window &window, WindowEv &ev) {
        if (ev.kind == WindowEvKind::MouseButtonPress) {
            if (cooldown == 0 && ball->alive && ev.mouse.button == GLFW_MOUSE_BUTTON_LEFT) {
                cooldown = 20;
                grenades.push_back(s.emplace<Grenade>(res));

                auto pos = (vec2(window.mouse.x, window.mouse.y) - vec2(window.size.x / 2.0, window.size.y / 2.0)).normalize().cast<float>();

                grenades.back()->transform->position = ball->transform->position;
                grenades.back()->body->velocity = (pos.cast<float>()) * 6.f + ball->body->velocity * 1.5f;
            }
        } else if (ev.kind == WindowEvKind::KeyPress) {
            if (ev.key.code == GLFW_KEY_B) {
                debug_on = !debug_on;
            }
        }
    }

    void update(Window& window) {
        if (cooldown > 0) cooldown--;

        // Update entities
        ball->input(window);
        ball->update();
        for(auto& grenade : s.box<Grenade>()) grenade.update(s, res);

        // Apply drag & integrate dynamic bodies
        for (auto &body : s.box<Body>()) {
            if (body.is_static) continue;
            if (body.inv_mass != 0.f) body.velocity.y += 0.15f;
            body.integrate();
            body.angular_vel *= 0.98f;
            body.velocity *= 0.990f;
        }

        // Iterative collision solving.
        for (auto i = 0; i < 6; i++) {
            broad_phase.find_collisions(s.box<Body>());

            // Some types of collisions are specific to certain entities, so delegate to them:
            ball->resolve_collisions(broad_phase, s);
            for(auto& grenade : s.box<Grenade>()) grenade.resolve_collisions(broad_phase, s, res);

            // Then handle general collisions
            for (auto& c : broad_phase.collision_iter(s.box<Body>())) {
                auto body = c.first;
                for (auto& col : c.second) {
                    if (body->collision_id == 1) {
                        if (col.body->collision_id == 1) {
                            auto res = col.contact.resolve(*body, *col.body);
                            res.first.apply(*body);
                            res.second.apply(*col.body);
                        }
                    }

                    if (col.body->collision_id == 2) { zone_collision(body, col); }
                }
            }
        }

        swap_filter(grenades, [&](auto& g) { return g->alive; } );
        swap_filter(stage.objects, [&](auto& o) { return o->alive; } );
    }

    /// Handle a body interacting with a zone (water etc)
    void zone_collision(Weak<Body> body, Collision col) {
        if (auto obj = col.body->parent.as<MapObject>()) {
            if ((*obj)->type == "kill_obj") {
                if (auto a = body->parent.as<MapObject>()) (*a)->alive = false;
            } else if ((*obj)->type == "force") {
                // For water (and other force zones), we simulate buoyancy by applying the force
                // at the centroid of the intersection between the zone and the body.

                // Require an existing non-negligible overlap (ignores future collisions, since there's no overlap)
                if (col.contact.intersection.t > -1.f) return;
                // Don't apply to shockwaves..
                if (auto a = body->parent.as<Grenade>()) if ((*a)->exploded) return;

                // Get the force from the zone's properties
                auto force = vec2((*obj)->properties.get("x")->float_value, (*obj)->properties.get("y")->float_value);

                // Transform the hulls to world space for clipping
                // (TODO: have Poly::clip take a transformation matrix)
                auto poly_a = body->hull->clone(); poly_a.transform(body->t->matrix());
                auto poly_b = col.body->hull->clone(); poly_b.transform(col.body->t->matrix());

                if (auto clip = poly_a.clip(poly_b)) {
                    // Apply a force proportional to the displaced area at the centroid of the intersection
                    auto centre = clip->centroid();
                    (*body).push(centre, force * (clip->area() / 1000.f));

                    // Apply some extra drag
                    (*body).velocity *= 0.995;
                    //(*body).angular_vel *= 0.98;

                    if (debug_on) {
                        // Draw the intersection area
                        auto debug_body = s.create(Body::immovable(s.create(clip->clone()), s.emplace<Material>(), s.emplace<Transform>()));
                        draw_queue.add(0.01, [&, debug_body]() { debug.draw_body(view, debug_body, vec4(1.f, 1.f, 1.f, 1.f)); });
                    }
                }
            }
        }
    }

    /// Draw the level's sprites & geometry
    void draw() {
        // Calculate the view matrix
        world_bounds = view_bounds + ball->transform->position - view_bounds.center();
        view = ::view(world_bounds);

        // TODO: really need a BVT for sprites etc.
        // Populate the render queue
        for (auto sprite : s.box<Sprite>().refs()) {
            sprite->update_anim(16);
            draw_queue.add(sprite->depth, [&, sprite](){ sprite->draw(view); });
        }
        for (auto& poly : *stage.filled_polys) {
            draw_queue.add(poly.depth, [&]() { poly.draw(view); });
        }
        if (debug_on) {
            for (auto body : s.box<Body>().refs()) {
                if (body->bounds.intersects(view_bounds))
                    draw_queue.add(0.02, [&, body]() { debug.draw_body(view, body); });
            }
        }

        draw_queue.run();
    }
};

