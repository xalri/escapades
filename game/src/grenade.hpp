#pragma once

#include <storage/storage.hpp>
#include <util/cwd.hpp>
#include <math/math.hpp>
#include <physics/body.hpp>
#include <physics/collision.hpp>
#include <resources/resources.hpp>
#include <stage/object.hpp>

class Grenade: public Object {
public:
    bool alive = true;
    bool exploded = false;
    Ref<Object> shockwave;
    int counter = 0;

    explicit Grenade(Resources& res): Object(res, cwd() + "/resources/proto/grenade.json") {
        body->collision_mask |= 0b1;
    }

    void init_component(Weak<Grenade> self) {
        body->parent = self.any();
    }

    void update(Storage &s, Resources& res) {
        if (!alive) return;
        counter++;
        if (exploded) {
            if (!shockwave.valid()) return;
            if (counter > 7) alive = false;
            shockwave->transform->scale += 0.07f;
            shockwave->transform->scale *= 0.9f;
            shockwave->sprite->tint.a -= 0.08f;
        } else {
            if (counter > 180) explode(s, res);
        }
    }

    void resolve_collisions(BroadPhase &broad_phase, Storage &s, Resources& res) {
        if (!alive) return;
        if (!exploded) {
            if (!body.valid()) return;
            for (auto &c: broad_phase.collisions(body.weak())) {
                explode(s, res);
                break;
            }
        } else {
            if (!shockwave.valid()) return;
            for (auto &c: broad_phase.collisions(shockwave->body.weak())) {
                (*c.body).push(transform->position, - c.contact.intersection.normal * 200.f);
            }
        }
    }

    void explode(Storage &s, Resources& res) {
        shockwave = s.emplace<Object>(res, cwd() + "/resources/proto/shock_wave.json");
        shockwave->transform->position = transform->position;
        shockwave->transform->scale = 0.001f;
        shockwave->sprite->tint.a = 0.8f;
        shockwave->body->collision_mask |= 0b11;
        shockwave->body->parent = body->parent;
        sprite = Ref<Sprite>();
        body = Ref<Body>();
        exploded = true;
        counter = 0;
    }
};

