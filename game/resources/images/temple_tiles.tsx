<?xml version="1.0" encoding="UTF-8"?>
<tileset name="temple_tiles" tilewidth="32" tileheight="32" tilecount="221" columns="17">
 <image source="temple_tiles.png" width="544" height="416"/>
 <tile id="76">
  <properties>
   <property name="collision_id" type="int" value="1"/>
  </properties>
  <objectgroup draworder="index">
   <object id="1" x="10" y="0">
    <polygon points="0,0 0,4 3,8 6,11 6,16 8,18 13,20 22,20 22,0"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="77">
  <properties>
   <property name="collision_id" type="int" value="1"/>
  </properties>
  <animation>
   <frame tileid="80" duration="10000"/>
   <frame tileid="78" duration="8000"/>
   <frame tileid="77" duration="4000"/>
   <frame tileid="79" duration="6000"/>
  </animation>
 </tile>
 <tile id="78">
  <properties>
   <property name="collision_id" type="int" value="1"/>
  </properties>
  <animation>
   <frame tileid="78" duration="3000"/>
   <frame tileid="80" duration="6500"/>
   <frame tileid="79" duration="8000"/>
   <frame tileid="77" duration="1200"/>
  </animation>
 </tile>
 <tile id="79">
  <properties>
   <property name="collision_id" type="int" value="1"/>
  </properties>
  <animation>
   <frame tileid="79" duration="8000"/>
   <frame tileid="80" duration="1500"/>
   <frame tileid="78" duration="3000"/>
   <frame tileid="77" duration="9000"/>
  </animation>
 </tile>
 <tile id="80">
  <properties>
   <property name="collision_id" type="int" value="1"/>
  </properties>
  <animation>
   <frame tileid="80" duration="4000"/>
   <frame tileid="78" duration="8000"/>
   <frame tileid="77" duration="6000"/>
   <frame tileid="79" duration="3000"/>
  </animation>
 </tile>
 <tile id="81">
  <properties>
   <property name="collision_id" type="int" value="1"/>
  </properties>
  <objectgroup draworder="index">
   <object id="1" x="0" y="20">
    <polygon points="0,0 7,0 12,-2 14,-4 14,-9 17,-12 22,-16 22,-20 0,-20"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="82">
  <properties>
   <property name="collision_id" type="int" value="1"/>
  </properties>
 </tile>
 <tile id="83">
  <properties>
   <property name="collision_id" type="int" value="1"/>
  </properties>
 </tile>
 <tile id="94">
  <properties>
   <property name="collision_id" type="int" value="1"/>
  </properties>
 </tile>
 <tile id="95">
  <properties>
   <property name="collision_id" type="int" value="1"/>
  </properties>
 </tile>
 <tile id="96">
  <properties>
   <property name="collision_id" type="int" value="1"/>
  </properties>
 </tile>
 <tile id="97">
  <properties>
   <property name="collision_id" type="int" value="1"/>
  </properties>
 </tile>
 <tile id="102">
  <properties>
   <property name="collision_id" type="int" value="1"/>
  </properties>
  <objectgroup draworder="index"/>
 </tile>
 <tile id="103">
  <properties>
   <property name="collision_id" type="int" value="1"/>
  </properties>
  <objectgroup draworder="index"/>
 </tile>
 <tile id="104">
  <properties>
   <property name="collision_id" type="int" value="1"/>
  </properties>
  <objectgroup draworder="index"/>
 </tile>
 <tile id="105">
  <properties>
   <property name="collision_id" type="int" value="1"/>
  </properties>
  <objectgroup draworder="index"/>
 </tile>
 <tile id="106">
  <properties>
   <property name="collision_id" type="int" value="1"/>
  </properties>
  <objectgroup draworder="index"/>
 </tile>
 <tile id="107">
  <properties>
   <property name="collision_id" type="int" value="1"/>
  </properties>
  <objectgroup draworder="index"/>
 </tile>
 <tile id="110">
  <properties>
   <property name="collision_id" type="int" value="1"/>
  </properties>
  <objectgroup draworder="index">
   <object id="1" x="10" y="0">
    <polygon points="0,0 0,4 3,8 6,11 6,16 8,18 13,20 22,20 22,0"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="111">
  <properties>
   <property name="collision_id" type="int" value="1"/>
  </properties>
  <objectgroup draworder="index">
   <object id="3" x="0" y="0">
    <polygon points="0,0 32,0 32,21 0,21"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="112">
  <properties>
   <property name="collision_id" type="int" value="1"/>
  </properties>
  <objectgroup draworder="index">
   <object id="1" x="0" y="0">
    <polygon points="0,0 32,0 32,21 0,21"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="113">
  <properties>
   <property name="collision_id" type="int" value="1"/>
  </properties>
  <objectgroup draworder="index">
   <object id="1" x="0" y="0">
    <polygon points="0,0 32,0 32,21 0,21"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="114">
  <properties>
   <property name="collision_id" type="int" value="1"/>
  </properties>
  <objectgroup draworder="index">
   <object id="1" x="0" y="0">
    <polygon points="0,0 32,0 32,21 0,21"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="115">
  <properties>
   <property name="collision_id" type="int" value="1"/>
  </properties>
  <objectgroup draworder="index">
   <object id="1" x="0" y="20">
    <polygon points="0,0 7,0 12,-2 14,-4 14,-9 17,-12 22,-16 22,-20 0,-20"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="119">
  <properties>
   <property name="collision_id" type="int" value="1"/>
  </properties>
 </tile>
 <tile id="120">
  <properties>
   <property name="collision_id" type="int" value="1"/>
  </properties>
 </tile>
 <tile id="121">
  <properties>
   <property name="collision_id" type="int" value="1"/>
  </properties>
 </tile>
 <tile id="122">
  <properties>
   <property name="collision_id" type="int" value="1"/>
  </properties>
 </tile>
 <tile id="123">
  <properties>
   <property name="collision_id" type="int" value="1"/>
  </properties>
 </tile>
 <tile id="124">
  <properties>
   <property name="collision_id" type="int" value="1"/>
  </properties>
 </tile>
 <tile id="127">
  <properties>
   <property name="collision_id" type="int" value="1"/>
  </properties>
  <objectgroup draworder="index">
   <object id="1" x="10" y="0">
    <polygon points="-1,0 -1,4 3,8 6,11 6,16 8,18 13,20 22,20 22,0"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="128">
  <properties>
   <property name="collision_id" type="int" value="1"/>
  </properties>
  <objectgroup draworder="index">
   <object id="2" x="0" y="0">
    <polygon points="0,0 32,0 32,21 0,21"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="129">
  <properties>
   <property name="collision_id" type="int" value="1"/>
  </properties>
  <objectgroup draworder="index">
   <object id="1" x="0" y="0">
    <polygon points="0,0 32,0 32,21 0,21"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="130">
  <properties>
   <property name="collision_id" type="int" value="1"/>
  </properties>
  <objectgroup draworder="index">
   <object id="1" x="0" y="0">
    <polygon points="0,0 32,0 32,21 0,21"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="131">
  <properties>
   <property name="collision_id" type="int" value="1"/>
  </properties>
  <objectgroup draworder="index">
   <object id="1" x="0" y="0">
    <polygon points="0,0 32,0 32,21 0,21"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="132">
  <properties>
   <property name="collision_id" type="int" value="1"/>
  </properties>
  <objectgroup draworder="index">
   <object id="1" x="0" y="20">
    <polygon points="0,0 7,0 12,-2 14,-4 14,-9 17,-12 21,-16 21,-20 0,-20"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="136">
  <properties>
   <property name="collision_id" type="int" value="1"/>
  </properties>
 </tile>
 <tile id="137">
  <properties>
   <property name="collision_id" type="int" value="1"/>
  </properties>
 </tile>
 <tile id="138">
  <properties>
   <property name="collision_id" type="int" value="1"/>
  </properties>
 </tile>
 <tile id="139">
  <properties>
   <property name="collision_id" type="int" value="1"/>
  </properties>
 </tile>
 <tile id="140">
  <properties>
   <property name="collision_id" type="int" value="1"/>
  </properties>
 </tile>
 <tile id="141">
  <properties>
   <property name="collision_id" type="int" value="1"/>
  </properties>
 </tile>
 <tile id="153">
  <properties>
   <property name="collision_id" type="int" value="1"/>
  </properties>
 </tile>
 <tile id="154">
  <properties>
   <property name="collision_id" type="int" value="1"/>
  </properties>
 </tile>
 <tile id="155">
  <properties>
   <property name="collision_id" type="int" value="1"/>
  </properties>
 </tile>
 <tile id="156">
  <properties>
   <property name="collision_id" type="int" value="1"/>
  </properties>
 </tile>
 <tile id="157">
  <properties>
   <property name="collision_id" type="int" value="1"/>
  </properties>
 </tile>
 <tile id="158">
  <properties>
   <property name="collision_id" type="int" value="1"/>
  </properties>
 </tile>
 <tile id="170">
  <properties>
   <property name="collision_id" type="int" value="1"/>
  </properties>
 </tile>
 <tile id="171">
  <properties>
   <property name="collision_id" type="int" value="1"/>
  </properties>
 </tile>
 <tile id="172">
  <properties>
   <property name="collision_id" type="int" value="1"/>
  </properties>
 </tile>
 <tile id="173">
  <properties>
   <property name="collision_id" type="int" value="1"/>
  </properties>
 </tile>
 <tile id="174">
  <properties>
   <property name="collision_id" type="int" value="1"/>
  </properties>
 </tile>
 <tile id="175">
  <properties>
   <property name="collision_id" type="int" value="1"/>
  </properties>
 </tile>
 <tile id="187">
  <properties>
   <property name="collision_id" type="int" value="1"/>
  </properties>
 </tile>
 <tile id="188">
  <properties>
   <property name="collision_id" type="int" value="1"/>
  </properties>
 </tile>
 <tile id="189">
  <properties>
   <property name="collision_id" type="int" value="1"/>
  </properties>
 </tile>
 <tile id="190">
  <properties>
   <property name="collision_id" type="int" value="1"/>
  </properties>
 </tile>
 <tile id="191">
  <properties>
   <property name="collision_id" type="int" value="1"/>
  </properties>
 </tile>
 <tile id="192">
  <properties>
   <property name="collision_id" type="int" value="1"/>
  </properties>
 </tile>
</tileset>
