<?xml version="1.0" encoding="UTF-8"?>
<tileset name="base" tilewidth="1404" tileheight="588" tilecount="44" columns="0">
 <grid orientation="orthogonal" width="1" height="1"/>
 <tile id="0">
  <image width="483" height="488" source="misc/tree_02.png"/>
 </tile>
 <tile id="1">
  <image width="331" height="333" source="misc/tree_01.png"/>
 </tile>
 <tile id="2">
  <image width="32" height="18" source="misc/rock_small_02.png"/>
  <objectgroup draworder="index">
   <object id="1" x="0" y="6">
    <polygon points="0,0 1,-2 5,-6 13,-6 18,-5 26,-4 32,0 31,4 25,10 21,12 3,12 0,8"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="3">
  <image width="20" height="13" source="misc/rock_small_01.png"/>
 </tile>
 <tile id="4">
  <image width="126" height="303" source="misc/rock_large_01.png"/>
  <objectgroup draworder="index">
   <object id="1" x="5" y="300">
    <polygon points="0,0 117,1 113,-72 119,-258 119,-289 77,-300 -1,-282 -5,-246 -1,-129"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="5">
  <image width="116" height="78" source="misc/rock_02.png"/>
  <objectgroup draworder="index">
   <object id="1" x="0" y="68">
    <polygon points="0,0 9,-21 21,-41 27,-44 28,-55 32,-63 47,-69 68,-60 77,-57 79,-41 83,-33 92,-22 116,-13 114,-6 114,-3 111,12 86,12 31,7"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="6">
  <image width="108" height="73" source="misc/rock_01.png"/>
  <objectgroup draworder="index">
   <object id="1" x="0" y="64">
    <polygon points="0,0 5,-15 22,-29 38,-40 46,-47 54,-59 62,-64 77,-63 84,-55 86,-48 91,-39 95,-26 104,-18 106,-10 109,-2 103,1 85,5 67,6 58,9 38,6 19,5 9,7"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="7">
  <image width="326" height="46" source="misc/fence_01.png"/>
 </tile>
 <tile id="8">
  <properties>
   <property name="elasticity" type="float" value="0.2"/>
   <property name="kinetic_friction" type="float" value="0.6"/>
   <property name="static_friction" type="float" value="0.8"/>
  </properties>
  <image width="397" height="67" source="ground/grass_03.png"/>
  <objectgroup draworder="index">
   <object id="1" x="2" y="14">
    <polygon points="0,0 6,-8 22,-13 58,-14 162,-12 229,-9 285,-7 342,-5 367,-3 388,9 397,19 395,27 380,33 356,47 336,52 277,52 217,51 169,49 119,51 62,47 28,45 11,41 12,17"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="9">
  <properties>
   <property name="elasticity" type="float" value="0.2"/>
   <property name="kinetic_friction" type="float" value="0.6"/>
   <property name="static_friction" type="float" value="0.8"/>
  </properties>
  <image width="223" height="61" source="ground/grass_02.png"/>
  <objectgroup draworder="index">
   <object id="1" x="3" y="9">
    <polygon points="0,0 11,-9 175,-9 190,-7 205,1 218,17 220,22 212,29 187,44 164,50 130,53 100,51 74,45 42,47 17,41 9,30 1,14"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="10">
  <properties>
   <property name="elasticity" type="float" value="0.2"/>
   <property name="kinetic_friction" type="float" value="0.6"/>
   <property name="static_friction" type="float" value="0.8"/>
  </properties>
  <image width="1404" height="134" source="ground/grass_01.png"/>
  <objectgroup draworder="index">
   <object id="1" x="8" y="15">
    <properties>
     <property name="elasticity" type="float" value="0.2"/>
    </properties>
    <polygon points="0,0 127,-8 404,-14 1264,-15 1392,-7 1396,20 1395,69 1271,113 156,114 6,56"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="17">
  <image width="202" height="33" source="interactable/springboard_01.png"/>
  <objectgroup draworder="index">
   <object id="2" x="0" y="33">
    <properties>
     <property name="elasticity" type="float" value="2.4"/>
    </properties>
    <polygon points="9,-7 194,-7 202,-27 183,-30 143,-33 64,-32 18,-29 0,-27"/>
   </object>
   <object id="3" x="0" y="7">
    <polygon points="0,0 0,26 202,26 202,-1 193,15 12,15"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="18">
  <image width="346" height="163" source="interactable/ramp_01.png"/>
  <objectgroup draworder="index">
   <object id="1" x="346" y="163">
    <polygon points="62,-1 0,-10 -16,-11 -41,-12 -76,-15 -101,-18 -122,-22 -143,-27 -159,-32 -187,-42 -209,-52 -236,-67 -257,-83 -272,-96 -297,-117 -320,-142 -338,-163 -346,-156 -346,-3 39,20"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="19">
  <properties>
   <property name="elasticity" type="float" value="0.2"/>
   <property name="kinetic_friction" type="float" value="0.6"/>
   <property name="static_friction" type="float" value="0.8"/>
  </properties>
  <image width="385" height="250" source="ground/grass_curve_01.png"/>
  <objectgroup draworder="index">
   <object id="1" x="385" y="99">
    <polygon points="0,0 -10,-38 -25,-86 -50,-94 -90,-92 -144,-75 -189,-57 -222,-37 -272,-6 -309,17 -340,40 -371,76 -386,106 -383,143 -343,150 -288,152 -249,149 -160,71 -89,34 -20,30"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="20">
  <image width="207" height="158" source="ground/bedrock_02.png"/>
 </tile>
 <tile id="21">
  <image width="235" height="112" source="ground/bedrock_01.png"/>
 </tile>
 <tile id="22">
  <image width="204" height="151" source="misc/sign_01.png"/>
 </tile>
 <tile id="23">
  <image width="213" height="106" source="ground/bedrock_04.png"/>
 </tile>
 <tile id="24">
  <image width="174" height="143" source="ground/bedrock_03.png"/>
 </tile>
 <tile id="25">
  <image width="276" height="135" source="misc/long_grass_01.png"/>
 </tile>
 <tile id="26">
  <image width="53" height="64" source="misc/flowers_02.png"/>
 </tile>
 <tile id="27">
  <image width="67" height="59" source="misc/flowers_01.png"/>
 </tile>
 <tile id="28">
  <properties>
   <property name="elasticity" type="float" value="0.2"/>
   <property name="kinetic_friction" type="float" value="0.6"/>
   <property name="static_friction" type="float" value="0.8"/>
  </properties>
  <image width="937" height="128" source="ground/grass_04.png"/>
  <objectgroup draworder="index">
   <object id="1" x="8" y="15">
    <polygon points="0,0 9,-8 48,-14 256,-14 536,-13 713,-12 887,-11 921,-4 929,34 927,72 861,112 781,111 519,110 370,109 252,112 89,107 19,68 4,36"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="29">
  <properties>
   <property name="kinetic_friction" type="float" value="0.6"/>
   <property name="static_friction" type="float" value="0.9"/>
  </properties>
  <image width="126" height="126" source="interactable/crate_01.png"/>
  <objectgroup draworder="index">
   <object id="1" x="1" y="1" width="125" height="125"/>
  </objectgroup>
 </tile>
 <tile id="30">
  <properties>
   <property name="elasticity" type="float" value="0.2"/>
   <property name="kinetic_friction" type="float" value="0.6"/>
   <property name="static_friction" type="float" value="0.8"/>
  </properties>
  <image width="933" height="588" source="ground/grass_curve_02.png"/>
  <objectgroup draworder="index">
   <object id="1" x="0" y="535">
    <polygon points="0,0 1,-8 6,-15 12,-21 16,-24 43,-28 73,-32 174,-54 279,-83 395,-134 469,-171 569,-231 641,-281 722,-351 791,-427 841,-486 872,-527 889,-537 911,-528 929,-518 933,-499 927,-472 887,-368 828,-260 764,-202 696,-169 593,-41 504,-18 346,-8 219,31 89,54 20,50 5,24"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="31">
  <properties>
   <property name="elasticity" type="float" value="0.2"/>
   <property name="kinetic_friction" type="float" value="0.6"/>
   <property name="static_friction" type="float" value="0.8"/>
  </properties>
  <image width="264" height="87" source="ground/grass_05.png"/>
  <objectgroup draworder="index">
   <object id="1" x="2" y="20">
    <polygon points="0,0 25,-12 62,-18 119,-18 208,-18 250,-18 262,-10 258,29 246,59 92,69 56,64 29,42 8,30"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="32">
  <image width="291" height="246" source="misc/sign_02.png"/>
 </tile>
 <tile id="33">
  <image width="91" height="190" source="interactable/checkpoint_white.png"/>
  <objectgroup draworder="index">
   <object id="1" x="-1" y="1" width="91" height="189"/>
  </objectgroup>
  <animation>
   <frame tileid="33" duration="0"/>
   <frame tileid="34" duration="0"/>
  </animation>
 </tile>
 <tile id="34">
  <image width="91" height="190" source="interactable/checkpoint_blue.png"/>
 </tile>
 <tile id="35">
  <image width="145" height="228" source="interactable/end_flag.png"/>
  <objectgroup draworder="index">
   <object id="1" x="2" y="1" width="142" height="229"/>
  </objectgroup>
 </tile>
 <tile id="36">
  <image width="102" height="80" source="misc/mushroom_01_blue.png"/>
 </tile>
 <tile id="37">
  <image width="102" height="80" source="misc/mushroom_01_red.png"/>
 </tile>
 <tile id="38">
  <image width="102" height="80" source="misc/mushroom_01_yellow.png"/>
 </tile>
 <tile id="39">
  <image width="85" height="102" source="misc/mushroom_02_blue.png"/>
 </tile>
 <tile id="40">
  <image width="85" height="102" source="misc/mushroom_02_red.png"/>
 </tile>
 <tile id="41">
  <image width="85" height="102" source="misc/mushroom_02_yellow.png"/>
 </tile>
 <tile id="42">
  <image width="102" height="80" source="misc/mushroom_03_blue.png"/>
 </tile>
 <tile id="43">
  <image width="105" height="92" source="misc/mushroom_03_red.png"/>
 </tile>
 <tile id="44">
  <image width="105" height="92" source="misc/mushroom_03_yellow.png"/>
 </tile>
 <tile id="45">
  <image width="45" height="281" source="interactable/log.png"/>
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="45" height="280"/>
  </objectgroup>
 </tile>
 <tile id="46">
  <image width="53" height="274" source="interactable/heavy_door.png"/>
  <objectgroup draworder="index">
   <object id="1" x="1" y="1" width="50" height="272"/>
  </objectgroup>
 </tile>
 <tile id="47">
  <properties>
   <property name="elasticity" type="float" value="0.2"/>
   <property name="kinetic_friction" type="float" value="0.6"/>
   <property name="static_friction" type="float" value="0.8"/>
  </properties>
  <image width="933" height="588" source="ground/grass_curve_04.png"/>
  <objectgroup draworder="index">
   <object id="1" x="17" y="585">
    <polygon points="0,0 -16,-40 -11,-68 5,-78 91,-90 184,-110 258,-132 328,-159 390,-189 415,-203 410,-239 406,-258 419,-290 434,-300 448,-314 472,-330 527,-352 550,-356 585,-352 620,-348 642,-350 664,-362 690,-384 729,-426 765,-466 796,-500 820,-532 843,-559 852,-575 868,-584 902,-575 918,-560 882,-452 830,-339 724,-145 540,-82 297,-41 175,-6 74,5"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="48">
  <properties>
   <property name="elasticity" type="float" value="0.2"/>
   <property name="kinetic_friction" type="float" value="0.6"/>
   <property name="static_friction" type="float" value="0.8"/>
  </properties>
  <image width="704" height="126" source="ground/grass_06.png"/>
  <objectgroup draworder="index">
   <object id="1" x="-1" y="10">
    <polygon points="0,0 40,-3 164,-9 306,-6 526,-9 676,-10 704,0 707,54 697,86 626,117 347,117 84,114 16,88"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="49">
  <image width="215" height="52" source="misc/short_grass_01.png"/>
 </tile>
</tileset>
