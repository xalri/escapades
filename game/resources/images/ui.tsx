<?xml version="1.0" encoding="UTF-8"?>
<tileset name="ui" tilewidth="1234" tileheight="389" tilecount="2" columns="0">
 <grid orientation="orthogonal" width="1" height="1"/>
 <tile id="0">
  <image width="1234" height="97" source="ui/respawn.png"/>
 </tile>
 <tile id="1">
  <image width="1200" height="389" source="ui/level_complete.png"/>
 </tile>
</tileset>
