<?xml version="1.0" encoding="UTF-8"?>
<tileset name="bg" tilewidth="1657" tileheight="558" tilecount="10" columns="0">
 <grid orientation="orthogonal" width="1" height="1"/>
 <tile id="0">
  <image width="264" height="69" source="bg/cloud_06.png"/>
 </tile>
 <tile id="1">
  <image width="281" height="58" source="bg/cloud_05.png"/>
 </tile>
 <tile id="2">
  <image width="297" height="59" source="bg/cloud_04.png"/>
 </tile>
 <tile id="3">
  <image width="312" height="133" source="bg/cloud_03.png"/>
 </tile>
 <tile id="4">
  <image width="669" height="85" source="bg/cloud_02.png"/>
 </tile>
 <tile id="5">
  <image width="657" height="211" source="bg/cloud_01.png"/>
 </tile>
 <tile id="6">
  <image width="1657" height="511" source="bg/hills_03.png"/>
 </tile>
 <tile id="7">
  <image width="1657" height="558" source="bg/hills_02.png"/>
 </tile>
 <tile id="8">
  <image width="1657" height="496" source="bg/hills_01.png"/>
 </tile>
 <tile id="9">
  <image width="442" height="444" source="bg/cave_bg_01.png"/>
 </tile>
</tileset>
