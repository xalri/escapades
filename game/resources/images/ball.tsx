<?xml version="1.0" encoding="UTF-8"?>
<tileset name="ball" tilewidth="20" tileheight="20" tilecount="5" columns="5">
 <image source="ball.png" width="100" height="20"/>
 <tile id="0">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="20" height="20">
    <ellipse/>
   </object>
  </objectgroup>
  <animation>
   <frame tileid="0" duration="500"/>
   <frame tileid="1" duration="500"/>
   <frame tileid="2" duration="500"/>
   <frame tileid="3" duration="500"/>
   <frame tileid="4" duration="500"/>
   <frame tileid="3" duration="500"/>
   <frame tileid="2" duration="500"/>
   <frame tileid="1" duration="500"/>
  </animation>
 </tile>
</tileset>
